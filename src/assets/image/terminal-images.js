import new_terminal_logo from './new_terminal_logo.svg';
import new_search from './new_search2.svg';
import new_users2 from './new_users2.svg';
import new_trade2 from './new_trade2.svg';
import new_coin2 from './new_coin2.svg';
import new_ham from './new_ham.svg';
import gxToken from './terminals_1.svg';
import apple from './Icon - Apple.svg';
import andorid from './Icon - Android.svg';
import email1 from './email.svg';
import sms from './sms.svg';
import vault_c from './vault_c.svg';
import scan from './scan.svg';
import logo from './gxtoken.svg';
import new_login from './new_login.svg';
import new_refresh from './new_refresh.svg';
import new_mobile from './new_mobile.svg';
import new_admin from './new_admin.svg';
import new_vaults from './new_vaults.svg';
import new_theater from './new_theater.svg';
import new_username from './new_username.svg';
import profile from './profile.svg';
import sd_exchange from './sd-ex-fee.svg';
import sd_exchange_full_logo from './ex-logo.svg';
import marketcharts from './gx-maket-charts.svg';

import new_apps from './new_apps.svg';
import BTC from './coins/btc.svg';
import BCH from './coins/bch.svg';
import ETH from './coins/eth.svg';
import LTC from './coins/litecoin.svg';
import Monero from './coins/monero.svg';
import USDT from './coins/usdt.svg';
import XRP from './coins/xrp.svg';

import Binance from './exchanges/binance.svg';
import Bitfinex from './exchanges/bitfinex.svg';
import Coinbase from './exchanges/coinbase.svg';
import Crex24 from './exchanges/crex24.svg';
import Four from './exchanges/four.svg';
import new_email from './new_email.svg';
import back from './back-left.svg';
import gx from './gx.svg';
export {
  gx,
  marketcharts,
  sd_exchange,
  sd_exchange_full_logo,
  profile,
  back,
  BTC,
  BCH,
  ETH,
  LTC,
  Bitfinex,
  Coinbase,
  Four,
  Crex24,
  Binance,
  Monero,
  USDT,
  XRP,
  vault_c,
  new_email,
  new_apps,
  new_username,
  new_refresh,
  new_login,
  new_mobile,
  new_admin,
  new_vaults,
  new_theater,
  scan,
  new_terminal_logo,
  new_search,
  new_users2,
  new_trade2,
  new_coin2,
  new_ham,
  gxToken,
  apple,
  andorid,
  email1,
  sms,
  logo,
};
