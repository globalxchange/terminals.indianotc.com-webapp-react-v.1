import React, { createContext, useState, useEffect, useRef } from 'react';
import Axios from 'axios';
import uuidv4 from 'uuid/v4';
import moment from 'moment-timezone';
import btc from '../assets/image/coins/btc.svg';
import { notification, message } from 'antd';
import Gx_commsService from '../utils/services/gx-comms.service';
import GX_apis from '../utils/services/gx-terminal-apis.service';
import { withRouter } from 'react-router-dom';
import Gx_terminal_user_api from '../utils/services/gx-terminal-user.service';
import Gx_vault_apis from '../utils/services/gx-vaults.service';
import { gxToken } from '../assets/image/terminal-images';
import {
  OrderBookeventEmitter,
  orderbookWSHelpers,
} from '../utils/orderbook-ws';

export const OptionsContext = createContext();

const OptionsContextProvider = ({ children }) => {
  const getAppEmail = () => {
    return localStorage.getItem('tokenAppLoginAccount');
  };
  const getToken = () => {
    return localStorage.getItem('tokenAppToken');
  };
  const [username, setUsername] = useState('Loading');
  //--------------------------------------------new--------------------------------------------//
  const [selectedCoin, setSelectedCoin] = useState('');
  const [selectedBasePairs, setSelectedBasePairs] = useState('');
  const [selectedTerminal, setSelectedTerminal] = useState(null);
  const [terminalLiteList_pairs, setterminalLiteList_pairs] = useState([]);
  const [email, setEmailId] = useState(getAppEmail());
  const [bankerCount, setBankerCount] = useState([]);

  const [token, setToken] = useState(getToken());
  const [coinModal, setcoinModal] = useState(false);
  const [tradeCoin, setTradeCoin] = useState('');
  const [acceptedBasePairs, setacceptedBasePairs] = useState([]);
  const [terminals, setTerminals] = useState([]);
  const [userRegisteredTerminals, setUserRegisteredTerminals] = useState(null);
  const [terminalCoins, setTerminalCoins] = useState([]);
  const [showVaults, setshowVaults] = useState(false);
  const [vaultSets, setVaultSets] = useState([]);
  const [returnToVault, setReturnToVault] = useState('');
  const [tempLoginDetails, setTempLoginDetails] = useState(null);
  const [balanceTrigger, setBalanceTrigger] = useState(false);
  const [showAdminPage, setshowAdminPage] = useState(false);
  const [userViewClicked, setuserViewClicked] = useState(false);
  const [showTheatre, setshowTheatre] = useState(false);
  const [socketPriceData, setSocketPriceData] = useState([]);
  const [randomValue, setRandomValue] = useState(Math.random() * 1000);
  const [socketOrderBook, setsocketOrderBook] = useState(''); // based on right side click coin
  const [prevTradeCoin, setPreviousTradeCoin] = useState('');
  const [selectedTab, setSelectedTab] = useState('Buy Orders');

  // const [chartType, setChartType] = useState('Area');
  //-------------------------------------------------------------------------------------------//

  const [requestType, setRequestType] = useState('Deposit');
  const [depositAsset, setdepositAsset] = useState(0);
  const [coin, setCoin] = useState('BTC');
  const [loginModal, setloginModal] = useState(false);
  const [buysellorder, setbuysellorder] = useState('');
  const [orderStatus, setorderStatus] = useState('');
  const [ordermessage, setordermessage] = useState('');
  const [showCandleChart, setshowCandleChart] = useState(false);
  const [windowTerminalId, setwindowTerminalId] = useState('');
  const [appColor, setappColor] = useState('');
  const [terminalID, setterminalID] = useState('');
  const [appImage, setappImage] = useState('');
  const [iconColoured, seticonColoured] = useState('');
  const [appfullLogoColoured, setappfullLogoColoured] = useState('');
  const [appEmail, setappEmail] = useState('');
  const [appfullLogoWhite, setappfullLogoWhite] = useState('');
  const [appName, setappName] = useState('TERMINAL');
  const [socketLastTrade, setsocketLastTrade] = useState('');
  const [dynamicApi, setdynamicApi] = useState('');
  const [transCoin, setTransCoin] = useState('USD');
  const [isTrading, setIsTrading] = useState(false);
  const [profilePic, setProfilePic] = useState('');
  const [balanceChange, setBalanceChange] = useState(false);
  const [amount, setAmount] = useState(1);
  const [seconds, setSeconds] = useState(30);
  const [countDown, setCountDown] = useState('00:00:00');
  const [endDate, setEndDate] = useState();
  const [tradeList, setTradeList] = useState([]);
  const [chartSymbol, setChartSymbol] = useState('BTCUSD');
  const [isLimit, setIsLimit] = useState(false);
  const [limitVal, setLimitVal] = useState(0);
  const [startDate, setStartDate] = useState();
  const [cryptoTimeRate, setCryptoTimeRate] = useState();
  const [strikeRate, setStrikeRate] = useState();

  const [switchs, setSwitchs] = useState(false);

  const [coinDetail, setCoinDetail] = useState({
    name: 'Bitcoin',
    symbol: 'BTC',
    icon: btc,
  });
  const [cryptoPrice, setCryptoPrice] = useState();
  const [chartType, setChartType] = useState('area');
  const [tickerTime, setTickerTime] = useState('1s');
  const usdAmountFormatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
  const btcAmountFormatter = new Intl.NumberFormat('en-US', {
    minimumFractionDigits: 5,
    maximumFractionDigits: 5,
  });
  const [name, setName] = useState('');
  const getCoinsx = () => {
    return {
      t: JSON.parse(localStorage?.getItem('tradeCoin')),
      p: prevTradeCoin,
    };
  };

  useEffect(() => {
    localStorage.setItem('tradeCoin', JSON.stringify(tradeCoin));
  }, [tradeCoin]);

  useEffect(() => {
    if (window.innerWidth < 600) {
      return;
    }
    if (tradeCoin === '' || tradeCoin === undefined) {
      return;
    } else if (!switchs) {
      OrderBookeventEmitter.on(`orderbook-connected`, () => {
        let coins = getCoinsx();
        // console.log(coins);
        orderbookWSHelpers.joinBasePair(coins.t, coins.p, setPreviousTradeCoin);
      });

      OrderBookeventEmitter.on('orderbook-message', (event) => {
        const response = JSON.parse(event);
        if (response.orderbook) {
          console.log('recieving orderbook');

          let buy = [];
          let sell = [];

          if (tradeCoin.is_terminal_lite) {
            response.orderbook.bids?.forEach((ele) => {
              let n1 = {
                amount: '',
                price: '',
                totalAsk: '',
                totalBid: '',
                percentage: 0,
              };

              n1.price = parseFloat(ele[0]);
              n1.amount = parseFloat(ele[1]);
              n1.totalBid = parseFloat(ele[1]) * parseFloat(ele[0]);
              buy.push(n1);
            });
            response.orderbook.asks?.forEach((ele) => {
              let n1 = {
                amount: '',
                price: '',
                totalAsk: '',
                totalBid: '',
                percentage: 0,
              };
              n1.price = parseFloat(ele[0]);
              n1.amount = parseFloat(ele[1]);
              n1.totalAsk = parseFloat(ele[1]) * parseFloat(ele[0]);
              sell.push(n1);
            });
          } else {
            buy = response.orderbook.buy;
            sell = response.orderbook.sell;
          }

          setsocketOrderBook({ buy: buy, sell: sell });
        }
        if (response.price) {
          let data = [...socketPriceData];
          console.log('recieving price');
          data.push({ timestamp: Date.now(), value: response.price });
          setSocketPriceData(data);
        }
        if (response.forex) {
          // console.log(response.forex);
          setsocketOrderBook({
            buy: response.forex.bid,
            sell: response.forex.ask,
          });
          let data = [...socketPriceData];
          data.push({ timestamp: Date.now(), value: response.forex.bid });
          setSocketPriceData(data);
        }
        // setOrders(response.data);
      });
      setSwitchs(true);
    }
  }, [tradeCoin]);

  useEffect(() => {
    if (window.innerWidth < 600) {
      return;
    }
    // Unregistered under of a particular terminal problem

    const getData = async () => {
      if (
        localStorage.getItem('tokenAppLoginAccount') &&
        terminalID !== '' &&
        localStorage.getItem('tokenAppToken')
      ) {
        try {
          const headers = {
            token: localStorage.getItem('tokenAppToken'),
            email: localStorage.getItem('tokenAppLoginAccount'),
          };

          if (userRegisteredTerminals) {
            checkUserTerminals(userRegisteredTerminals, headers, terminalID);
          } else {
            console.log('into the get data');

            let res = await Gx_terminal_user_api.getUserRegisteredTerminals(
              headers,
            );
            setUserRegisteredTerminals(res);
            checkUserTerminals(res, headers, terminalID);
          }
        } catch (e) {
          console.log(e?.message);
        }
      }
    };
    getData();
  }, [terminalID]);

  const checkUserTerminals = async (item, headers, terminalID) => {
    if (window.innerWidth < 600) {
      return;
    }
    // console.log(item);
    try {
      let n1 = item?.find((ele) => {
        return ele.ter_terminal_id === terminalID;
      });
      // console.log(n1);
      if (!n1) {
        console.log(n1);
        await Gx_terminal_user_api.addUserToTerminal(headers, terminalID);
      }
      let registeredTerminals =
        await Gx_terminal_user_api.getUserRegisteredTerminals(headers);
      setUserRegisteredTerminals(registeredTerminals);
    } catch (error) {
      if (
        error?.message === 'User is not registered for terminals' ||
        error?.message === 'Session Expired'
      ) {
        notification?.error(error?.message);
        localStorage?.clear();
        setloginModal(true);
      } else {
        console.log(error);
        notification.error({ message: error?.message });
      }
    }
  };

  useEffect(() => {
    getCoins();
  }, []);

  //--------------------------------------------------------------socket------------------------------------------------------//

  // ws.onclose = () => {
  //   ws = new WebSocket('wss://charts.terminals.dev');
  //   restartSocket();
  // };

  //--------------------------------------------------------------socket------------------------------------------------------//

  // useEffect(async () => {
  //   if (window.innerWidth < 600) {
  //     return;
  //   }
  //   //
  //   if (tradeCoin && terminalID) {
  //     if (!tradeCoin?.is_terminal_lite) {
  //       // const socket = io(
  //       //   `https://terminalsapp.apimachine.com/orders?terminalId=${terminalID}`,
  //       // );
  //       // socket.on('connect', () => {
  //       //   console.log('socketoioioi', socket.id);
  //       // });
  //       // socket.on('disconnect', () => {
  //       //   console.log('socketoioioi', socket.id);
  //       // });
  //       // socket.on('orderbook', (data) => {
  //       //   console.log('socketoioioi', data);
  //       //   setsocketOrderBook(data);
  //       // });
  //       // socket.on('lastTradePrice', (data) => {
  //       //   console.log('socketoioioi', data);
  //       //   setsocketLastTrade(data);
  //       // });
  //       // socket.emit('joinTerminalBasePair', `${terminalID}-${tradeCoin}`);
  //       // socket.on('user.order.traded', (data) => {
  //       //   console.log('socketoioioi', data);
  //       //   message.success(data);
  //       // });
  //       // socket.on('error', (err) => {
  //       //   console.error('socketoioioi', err);
  //       // });
  //     } else if (tradeCoin?.is_terminal_lite) {
  //       restartSocket();
  //     }
  //   }
  // }, [tradeCoin, terminalID, email]);

  useEffect(() => {
    const appToken = getToken();
    setToken(appToken);
  }, [email]);

  useEffect(() => {
    async function getdata() {
      if (window.innerWidth < 600) {
        return;
      }
      try {
        let res = await Gx_commsService.getCoinPrices(``);
        setCryptoPrice(res.data);
      } catch (e) {
        message.error(e?.message);
      }
    }
    getdata();

    return () => {};
  }, []);

  useEffect(() => {
    if (ordermessage) {
      notification['info']({
        message: `${tradeCoin}   (${orderStatus})`,
        description: ordermessage,
        duration: 3,
      });
    }
  }, [orderStatus, ordermessage]);

  useEffect(() => {
    if (window.innerWidth < 600) {
      return;
    }
    if (email !== '' && email !== 'Loading') {
      let res = Gx_commsService.get_affiliate_logs(email);
      const data = res[0];
      if (data) {
        setUsername(data.username);
        setName(data.name);
      }
    }
  }, [email]);

  useEffect(() => {
    setTimeout(setBalanceChange(!balanceChange), 0);
  }, [isTrading]);

  function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }

  const handleLeaveBasePair = async () => {
    // io(`https://charts.apimachine.com/basepairs`);
    //  await socket.emit('leaveBasePair', `${tradeCoin}`);
  };

  const getCoins = async () => {
    if (window.innerWidth < 600) {
      return;
    }
    try {
      let res = await Gx_vault_apis.getCoinsData();
      setTerminalCoins([...res]);
    } catch (e) {
      notification.error(e?.message);
    }
  };

  const logout = () => {
    localStorage?.clear();
    window.location.reload();
  };

  const getCoinPairData = async (id, setLoading) => {
    if (window.innerWidth < 600) {
      return;
    }
    try {
      setterminalID(id);

      if (!selectedTerminal) {
        let res = await GX_apis.getAllTerminals();
        let data = res.find((item) => item?.terminal?.id === id);
        setTerminals(res);
        setSelectedTerminal(data);
        setappColor(
          data?.terminal?.color_code ? data?.terminal?.color_code : '#000000',
        );
        setappImage(data?.terminal?.icon_white);
        seticonColoured(data?.terminal?.full_logo_coloured);
        setappfullLogoColoured(
          data?.terminal?.full_logo_coloured
            ? data?.terminal?.full_logo_coloured
            : gxToken,
        );
        setappEmail(data?.terminal?.ter_user?.email);
        setappName(data?.terminal?.name);
        setappfullLogoWhite(data?.terminal?.full_logo_white);
      } else {
        setappColor(
          selectedTerminal?.terminal?.color_code
            ? selectedTerminal?.terminal?.color_code
            : '#000000',
        );
        setappImage(selectedTerminal?.terminal?.icon_white);
        seticonColoured(selectedTerminal?.terminal?.full_logo_coloured);
        setappfullLogoColoured(
          selectedTerminal?.terminal?.full_logo_coloured
            ? selectedTerminal?.terminal?.full_logo_coloured
            : gxToken,
        );
        setappEmail(selectedTerminal?.terminal?.ter_user?.email);
        setappName(selectedTerminal?.terminal?.name);
        setappfullLogoWhite(selectedTerminal?.terminal?.full_logo_white);
      }

      //new side coins api
      let res = await GX_apis.getCoinPairDataById(id);
      //--------------setting data for coinsider------------//
      setacceptedBasePairs(res.pairs);
      setSelectedCoin(res.pairs[0]);
      setterminalLiteList_pairs(res.pairs[0].pairs);
      setTradeCoin(res.pairs[0].pairs[0]);
      setLoading(false);

      //-----------------------------------------------------//
    } catch (error) {
      {
        console.log(error);
        notification.error({ message: error?.message });
      }
    }
  };

  const balanceToggle = () => {
    setTimeout(setBalanceChange(!balanceChange), 3000);
  };

  const getUTCDateTime = () => {
    let endDateTime = moment.utc().add(seconds, 'second');
    setEndDate(endDateTime.valueOf());
    return endDateTime.format('YYYY-MM-DD HH:mm:ss');
  };

  const startTrade = async (callOrPut) => {
    const time = getUTCDateTime();
    const gxId = uuidv4();
    setTradeList([
      {
        startDate: cryptoTimeRate.time,
        strikeRate: cryptoTimeRate.value,
        call: callOrPut,
        endDate: moment.utc().add(seconds, 'second').valueOf(),
        coin,
        seconds,
        gxId,
      },
      ...tradeList,
    ]);
    setStartDate(cryptoTimeRate.time);
    setStrikeRate({ value: cryptoTimeRate.value, call: callOrPut });
    // const response = await Axios.get(
    //   "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD"
    // );
    setIsTrading(true);
    Axios.post('https://tokenoptions.azurewebsites.net/api/Trade/TradeData', {
      Time: time,
      Counter: amount, // / cryptoPrice[coin],
      RequestType: callOrPut ? 'Upper' : 'Lower',
      GxId: gxId,
      Email: email,
      UserId: username,
      Asset: coin,
      StrikeType: isLimit ? 'Limit' : 'Market',
      Market: '',
      Limit: isLimit ? limitVal : '',
    })
      .then((res) => {
        setTimeout(setBalanceChange(!balanceChange), (seconds + 2) * 1000);
      })
      .catch((err) => {
        alert(JSON.stringify(err));
      });
  };

  const refreshBalance = () => {
    setBalanceChange(!balanceChange);
  };
  const setAppLoginData = (
    tokenAppToken,
    tokenAppAccessToken,
    tokenAppLoginAccount,
    device_key,
  ) => {
    localStorage.setItem('tokenAppToken', tokenAppToken);
    localStorage.setItem('tokenAppAccessToken', tokenAppAccessToken);
    localStorage.setItem('tokenAppLoginAccount', tokenAppLoginAccount);
    localStorage.setItem('device_key', device_key);
  };
  const findFiat = (value) => {
    if (
      value == 'GXT' ||
      value == 'USDT' ||
      value == 'AED' ||
      value == 'AUD' ||
      value == 'CAD' ||
      value == 'CNY' ||
      value == 'EUR' ||
      value == 'GBP' ||
      value == 'INR' ||
      value == 'JPY' ||
      value == 'USD'
    ) {
      return true;
    } else {
      return false;
    }
  };

  const findFiat2 = (value) => {
    if (
      value == 'AED' ||
      value == 'AUD' ||
      value == 'CAD' ||
      value == 'CNY' ||
      value == 'EUR' ||
      value == 'GBP' ||
      value == 'INR' ||
      value == 'JPY' ||
      value == 'USD'
    ) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <OptionsContext.Provider
      value={{
        //-----------------------------new-------------------//

        selectedTab,
        setSelectedTab,
        bankerCount,
        setBankerCount,
        socketPriceData,
        setSocketPriceData,
        showTheatre,
        setshowTheatre,
        terminalCoins,
        prevTradeCoin,
        setPreviousTradeCoin,
        setTerminalCoins,
        socketOrderBook,
        setsocketOrderBook,
        balanceTrigger,
        setBalanceTrigger,
        showVaults,
        setshowVaults,
        userRegisteredTerminals,
        setUserRegisteredTerminals,
        tempLoginDetails,
        setTempLoginDetails,
        vaultSets,
        setVaultSets,
        terminals,
        setTerminals,
        selectedTerminal,
        setSelectedTerminal,
        selectedCoin,
        setSelectedCoin,
        selectedBasePairs,
        setSelectedBasePairs,
        setAppLoginData,
        findFiat,
        findFiat2,
        requestType,
        returnToVault,
        setReturnToVault,
        setRequestType,
        setdepositAsset,
        setCoin,
        setTradeCoin,
        tradeCoin,
        setRandomValue,
        buysellorder,
        setbuysellorder,
        randomValue,
        coin,
        depositAsset,
        logout,
        // Start Trade
        startTrade,
        setAmount,
        amount,
        seconds,
        setSeconds,
        profilePic,
        // Chart Change
        chartSymbol,
        setChartSymbol,
        // Count Down
        isTrading,
        setIsTrading,
        countDown,
        setCountDown,

        isLimit,
        setIsLimit,
        limitVal,

        setLimitVal,

        tickerTime,
        setTickerTime,
        //
        endDate,
        startDate,
        strikeRate,
        // Chart Type
        chartType,
        setChartType,
        email,
        setEmailId,
        // usdAmountFormatter
        usdAmountFormatter,
        btcAmountFormatter,
        // Chart
        cryptoTimeRate,
        setCryptoTimeRate,
        // TradeList
        tradeList,
        balanceToggle,
        // transactions
        // Fullname Of USer
        name,
        // Coin To Withdraw Deposit
        transCoin,
        setTransCoin,
        username,
        token,
        refreshBalance,
        // Search Sidebar
        // Coin Detail Full
        coinDetail,
        setCoinDetail,
        setloginModal,
        showCandleChart,
        setshowCandleChart,
        userViewClicked,
        appColor,
        terminalID,
        getCoinPairData,
        appImage,
        appfullLogoColoured,
        appEmail,
        iconColoured,
        appfullLogoWhite,
        setappfullLogoWhite,
        coinModal,
        setcoinModal,
        appName,
        acceptedBasePairs,
        setuserViewClicked,
        setwindowTerminalId,
        terminalLiteList_pairs,
        setterminalLiteList_pairs,
        loginModal,
        // Exchange in terminal
        showAdminPage,
        setshowAdminPage,
        dynamicApi,
        socketLastTrade,
        handleLeaveBasePair,
      }}
    >
      {children}
    </OptionsContext.Provider>
  );
};
export default withRouter(OptionsContextProvider);

export const TerminalConsumer = OptionsContext.Consumer;
