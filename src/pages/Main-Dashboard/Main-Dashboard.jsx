import React, { useState, useContext, useEffect } from 'react';
import Lottie from 'react-lottie';
import { Modal as Modal2 } from 'antd';
import Gx_chart_card from '../../components/gx-dashboard-components/gx-chart-components/gx-chart-card';
import Gx_trading_card from '../../components/gx-dashboard-components/gx-trading-card-details/gx-trading-card';
import Gx_order_book from '../../components/gx-dashboard-components/gx-order-book/gx-order-book';
import Gx_order_card from '../../components/gx-dashboard-components/gx-order-card/gx-order-card';
import Gx_layout from '../../components/gx-dashboard-components/gx-layout';
import { OptionsContext } from '../../context/Context';
import '../../assets/scss/mainpage.scss';
import * as animationData from '../../assets/animation/new_slow_chart.json';
import { withRouter } from 'react-router-dom';
import Gx_mobile_view_landing from '../../components/gx-mobile-view/gx-mobile-view-landing';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

const Gx_Terminal_Dashboard = () => {
  const { getCoinPairData, setshowAdminPage } = useContext(OptionsContext);

  const [loading, setLoading] = useState(true);
  const handleClose = async () => {
    localStorage.setItem('tokenAppLoginAccount', 'shorupan@gmail.com');
    localStorage.removeItem('tokenAppLoginAccount1');
    window.location.reload();
  };

  useEffect(() => {
    async function getData() {
      setLoading(true);
      let n1 = window.location.href.split('/#/');
      getCoinPairData(n1[1], setLoading);
    }
    getData();
  }, []);

  return (
    <>
      <div className="gx-mobileRes">
        {/* {clickedUser && !token && !email ? <Gx_login /> : ''} */}
        <div>
          {/* main home page */}

          {!loading ? (
            <Gx_layout
              className="home-page"
              loading={loading}
              setLoading={setLoading}
            >
              <div className="row m-0 pl-3 pt-2 pb-0 h-50 ">
                <div className="col-lg-9 p-2 d-flex">
                  <Gx_chart_card />
                </div>
                <div className="col-lg-3 p-2 d-flex">
                  <Gx_order_book />
                </div>
              </div>
              <div className="row m-0 pl-3 pt-0 pb-2 h-50">
                <div
                  className="col-lg-9 p-2 d-flex"
                  style={{
                    height: localStorage.getItem('tokenAppLoginAccount1')
                      ? 'calc((100vh/2) - 108px)'
                      : 'calc((100vh/2) - 48px)',
                  }}
                >
                  <Gx_trading_card />
                </div>
                <div
                  className="col-lg-3 p-2 d-flex"
                  style={{
                    height: localStorage.getItem('tokenAppLoginAccount1')
                      ? 'calc((100vh/2) - 108px)'
                      : 'calc((100vh/2) - 48px)',
                  }}
                >
                  <Gx_order_card />
                </div>
              </div>

              {localStorage.getItem('tokenAppLoginAccount1') ? (
                <div style={{ position: 'absolute', bottom: '0' }}>
                  <div
                    style={{
                      width: 'calc(100vw - 108px)',
                      height: '64px',
                      backgroundColor: 'white',
                      border: '1px solid rgba(52, 133, 192, 0.25)',
                    }}
                  >
                    <div style={{ padding: '20px' }}>
                      <span style={{ color: '#3485C0' }}>
                        Currently Viewing as{' '}
                        {localStorage.getItem('tokenAppLoginAccount1')} {'->'}{' '}
                        {localStorage.getItem('tokenAppLoginAccount')}
                      </span>
                    </div>

                    <div
                      className="d-flex"
                      style={{
                        position: 'absolute',
                        bottom: '16px',
                        right: '36px',
                      }}
                    >
                      <button
                        style={{
                          backgroundColor: '#3485C0',
                          color: 'white',
                          fontWeight: 'bold',
                          padding: '4px',
                          marginRight: '4px',
                          border: 'none',
                          height: '32px',
                          width: '160px',
                        }}
                        onClick={() => {
                          // console.log('userchanger');
                          setshowAdminPage(true);
                          // setuserViewClicked(true);
                        }}
                      >
                        Change User
                      </button>
                      <button
                        style={{
                          backgroundColor: 'white',
                          color: '#3485C0',
                          fontWeight: 'bold',
                          padding: '4px',
                          border: '1px solid #3485C0',
                          height: '32px',
                          width: '160px',
                        }}
                        onClick={() => handleClose()}
                      >
                        Close User View
                      </button>
                    </div>
                  </div>
                </div>
              ) : (
                ''
              )}
            </Gx_layout>
          ) : (
            <Lottie
              options={defaultOptions}
              height={300}
              width={300}
              style={{ marginTop: '25vh' }}
            />
          )}
        </div>
      </div>

      <Gx_mobile_view_landing />
    </>
  );
};

export default withRouter(Gx_Terminal_Dashboard);
