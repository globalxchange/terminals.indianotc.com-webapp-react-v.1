import React, { useState, useContext, useEffect } from 'react';
import { Skeleton, message } from 'antd';
import { Navbar, Nav } from 'react-bootstrap';
import { motion } from 'framer-motion';
import { Carousel } from 'react-responsive-carousel';

import {
  new_terminal_logo,
  new_search,
  new_users2,
  new_trade2,
  new_coin2,
  new_ham,
} from '../../assets/image/terminal-images';
import Gx_mobile_view from '../../components/gx-mobile-view/gx-mobile-view-landing';
import { OptionsContext } from '../../context/Context';
import '../../assets/scss/mainpage.scss';
import GX_apis from '../../utils/services/gx-terminal-apis.service';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader

import { withRouter } from 'react-router-dom';

const Gx_terminal_landing = ({ history }) => {
  //-------------------------------------------------------------------states--------------------------------------------------------------------------------------//
  const {
    setwindowTerminalId,
    selectedTerminal,
    setSelectedTerminal,
    terminals,
    setTerminals,
    bankerCount,
    setBankerCount,
  } = useContext(OptionsContext);
  const [terminalLoading, setterminalLoading] = useState(false);
  const [selectedTerminalIndex, setselectedTerminalIndex] = useState('');
  //-------------------------------------------------------------------states--------------------------------------------------------------------------------------//

  //-------------------------------------------------------------------useffect------------------------------------------------------------------------------------//

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      if (window.innerWidth < 600) {
        return;
      }
      setterminalLoading(true);
      let res;
      if (terminals?.length === 0) {
        res = await GX_apis.getAllTerminals();
        let bankerCount_v;
        try {
          bankerCount_v = await GX_apis.getBankerCounts();
        } catch (e) {
          //
        }
        setBankerCount(bankerCount_v);
        setTerminals(res);
        setSelectedTerminal(res[0]);
        setterminalLoading(false);
        setselectedTerminalIndex(0);
      } else {
        setterminalLoading(false);
        setselectedTerminalIndex(0);
      }
    } catch (E) {
      message.error(E?.message);
    }
  };

  //-------------------------------------------------------------------useffect------------------------------------------------------------------------------------//

  return (
    <>
      <div className="gx-mobileRes">
        {terminalLoading ? (
          <div className="mainImg">
            <img
              className="animate__animated animate__heartBeat animate__infinite	infinite"
              style={{ height: '80px', padding: '4px' }}
              src={new_terminal_logo}
            />
          </div>
        ) : (
          <>
            <Navbar expand="lg" className="navbar">
              <Navbar.Brand>
                <motion.div whileHover={{ scale: 1.2 }}>
                  <img src={new_ham} />
                </motion.div>
                <div className="inner-nav">
                  <img src={new_terminal_logo} />
                </div>
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto"></Nav>

                <motion.div whileHover={{ scale: 1.2 }}>
                  <img src={new_search} />
                </motion.div>
              </Navbar.Collapse>
            </Navbar>

            <>
              {terminalLoading ? (
                <div style={{ height: '40vh' }}>
                  <Skeleton />
                </div>
              ) : (
                <Carousel
                  centerMode={true}
                  centerSlidePercentage="33.33"
                  showStatus={false}
                  autoPlay
                  interval={5000000}
                  infiniteLoop
                  showArrows={false}
                  showIndicators={false}
                  showThumbs={false}
                  selectedItem={selectedTerminalIndex}
                  onClickItem={(index) => {
                    setselectedTerminalIndex(index);
                    setSelectedTerminal(terminals[index]);
                  }}
                >
                  {terminals?.map((elem, i) => {
                    let ele = elem?.terminal;

                    return (
                      <>
                        {selectedTerminal?.terminal ? (
                          <div
                            key={i}
                            name={ele?.name}
                            id={ele?.id}
                            style={{
                              display: 'flex',
                              alignItems: 'center',
                              zIndex: `${
                                selectedTerminalIndex === i ? '1' : '-1'
                              }`,
                              padding:
                                selectedTerminalIndex === i ? '15%' : '12%',
                              width: `${
                                selectedTerminalIndex === i ? '102%' : '100%'
                              }`,

                              backgroundColor: 'white',
                              height: '100%',
                              border:
                                selectedTerminalIndex === i
                                  ? `1px solid ${
                                      selectedTerminal?.terminal
                                        ? `#${selectedTerminal?.terminal?.color_code}`
                                        : '#000000'
                                    }`
                                  : '1px solid #EBEBEB',
                              marginTop:
                                selectedTerminalIndex === i ? '5px' : '35px',
                              opacity:
                                selectedTerminalIndex === i ? '1' : '0.2',
                              borderBottom:
                                selectedTerminalIndex === i
                                  ? '0px solid black'
                                  : '',

                              marginLeft: `${
                                selectedTerminalIndex === i
                                  ? '-1% !important'
                                  : ''
                              }`,
                              marginRight: `${
                                selectedTerminalIndex === i
                                  ? '-1% !important'
                                  : ''
                              }`,
                              position: `${
                                selectedTerminalIndex === i ? 'absolute' : ''
                              }`,
                              boxShadow: `${
                                selectedTerminalIndex === i
                                  ? '0px 47px 70px -4px'
                                  : '-2px 0px 10px 0px '
                              }`,
                            }}
                            onClick={async () => {
                              if (selectedTerminalIndex === i) {
                                setwindowTerminalId(ele.id);
                                history?.push(`/${ele.id}`);
                              }
                            }}
                            className="carouselCards"
                          >
                            <img
                              style={{ maxHeight: '100px' }}
                              src={
                                ele?.full_logo_coloured
                                  ? ele?.full_logo_coloured
                                  : new_terminal_logo
                              }
                            />
                          </div>
                        ) : (
                          ''
                        )}
                      </>
                    );
                  })}
                </Carousel>
              )}

              <div
                style={{
                  backgroundColor: `#${
                    selectedTerminal
                      ? selectedTerminal?.terminal?.color_code
                      : '#000000'
                  }`,
                }}
                className="blackmiddle"
              >
                <div className="row">
                  <div className=" w-33 c-white">
                    <strong>
                      $ 0.00
                      {/* {selecteduserStats
                        ? selecteduserStats.tradingVolume.toFixed(2)
                        : '0.00'} */}
                    </strong>{' '}
                    Trading Volume
                  </div>
                  <div className="w-33">
                    <motion.div whileHover={{ scale: 1.4 }}>
                      <strong
                        style={{ cursor: 'pointer', color: 'white' }}
                        onClick={() =>
                          history?.push(selectedTerminal?.terminal?.id)
                        }
                      >
                        {selectedTerminal?.terminal?.name}
                      </strong>
                    </motion.div>
                  </div>{' '}
                  <div className=" w-33 c-white">
                    <strong>
                      {
                        bankerCount?.[
                          selectedTerminal?.terminal?.ter_user?.email
                        ]
                      }
                    </strong>{' '}
                    Followers
                  </div>
                </div>
              </div>

              <div className="row mainStats">
                <div className="col-md-4 pl-3">
                  <img src={new_users2} className="borderE" />
                  <p>
                    {selectedTerminal ? selectedTerminal?.usersCount : ''} Users
                  </p>
                </div>
                <div className="col-md-4 pl-3">
                  <img src={new_trade2} className="borderE" />
                  <p>
                    {selectedTerminal ? selectedTerminal.totalPairs : ''} Pairs
                  </p>
                </div>
                <div className="col-md-4 pl-3">
                  <img className="borderE " src={new_coin2} />
                  <p>
                    {selectedTerminal ? selectedTerminal.totalCurrencies : ''}{' '}
                    Currencies
                  </p>
                </div>
              </div>
            </>
          </>
        )}
      </div>
      <Gx_mobile_view />
    </>
  );
};

export default withRouter(Gx_terminal_landing);
