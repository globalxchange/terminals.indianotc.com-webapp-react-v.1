import React, { useState, useEffect, useContext } from 'react';
import { Button } from 'react-bootstrap';
import { Input, Divider, message, Modal as Modal2, Row, Col } from 'antd';
import { withRouter } from 'react-router-dom';

import { CheckCircleOutlined } from '@ant-design/icons';
import jwt from 'jsonwebtoken';
import { profile } from '../../assets/image/terminal-images';
import '../../assets/scss/login.scss';
import { OptionsContext } from '../../context/Context';
import GX_apis from '../../utils/services/gx-terminal-apis.service';
import Gx_terminal_user_api from '../../utils/services/gx-terminal-user.service';
import Gx_commsService from '../../utils/services/gx-comms.service';
import Gx_mobile_view_landing from '../../components/gx-mobile-view/gx-mobile-view-landing';

function Gx_TerminalSignUp({ match, history }) {
  const { selectedTerminal, tempLoginDetails, setloginModal, setAppLoginData } =
    useContext(OptionsContext);
  const [appColor, setappColor] = useState('');

  const [appImage, setappImage] = useState('');
  const [firstName, setfirstName] = useState('');
  const [imageURL, setimageURL] = useState('');
  const [load_img, setload_img] = useState(false);
  const [lastName, setlastName] = useState('');

  const [username, setusername] = useState('');
  const [page, setpage] = useState(1);
  const [bgColor, setbgColor] = useState('red');

  useEffect(() => {
    console.log(tempLoginDetails);
    const getData = async () => {
      if (tempLoginDetails.token) {
        if (!selectedTerminal) {
          try {
            let data = await GX_apis.getTerminalById(match?.params?.id);
            setappColor(data?.color_code);
            setappImage(data?.full_logo_coloured);
          } catch (e) {
            message.error(e?.message);
          }
        } else {
          setappColor(selectedTerminal?.terminal?.color_code);
          setappImage(selectedTerminal?.terminal?.full_logo_coloured);
        }
      } else {
        message?.warning('Please login to continue terminals registration');
        history?.push(`/${match?.params?.id}`);
        setloginModal(true);
      }
    };
    getData();
  }, []);

  useEffect(() => {
    if (username.length == 0) {
      setbgColor('red');
    }
  }, [username]);

  const handleImageChange = async (e) => {
    setimageURL('');
    e.preventDefault();
    setload_img(true);
    if (e.target.files[0] === undefined) {
      message.error('Please upload image');
    } else {
      let filename = e.target.files[0].name;

      let selected_file = e.target.files[0];
      let fileext = e.target.files[0].type;

      if (fileext.substring(0, 5) === 'image') {
        const formData = new FormData();
        formData.append('files', selected_file);
        const secret = 'uyrw7826^&(896GYUFWE&*#GBjkbuaf'; //secret not to be disclosed anywhere.
        const email = 'ram@nvestbank.com'; //email of the developer.
        const path_inside_brain = 'root/CounselAppImages/';
        let fileName = 'image' + Math.random() + '.' + /[^.]+$/.exec(filename);
        console.log(fileName);
        const token = jwt.sign({ name: fileName, email: email }, secret, {
          algorithm: 'HS512',
          expiresIn: 240,
          issuer: 'gxjwtenchs512',
        });

        console.log(/[^.]+$/.exec(filename) + ' name-of-the-file');
        let response = await Gx_commsService.uploadFileinDrive(
          email,
          path_inside_brain,
          token,
          fileName,
          formData,
        );

        setload_img(false);
        if (response.status) {
          setimageURL(response?.payload.url);
          setpage(4);
        } else {
          message.error(response?.payload);
        }
      } else {
        message.error('Please upload only images');
      }
    }
  };

  const handleComplete = async () => {
    try {
      const headers = {
        token: tempLoginDetails?.token,
        email: tempLoginDetails?.email,
      };

      const body = {
        username: username,
        firstName: firstName, // optional
        lastName: lastName, // optional
        profilePic: imageURL, // optional
      };
      let res = await Gx_terminal_user_api.addUserToTerminals(headers, body);
      let res1 = await Gx_terminal_user_api.addUserToTerminal(
        headers,
        match?.params?.id,
      );
      if (res && res1) {
        setAppLoginData(
          tempLoginDetails?.token,
          tempLoginDetails?.accessToken,
          tempLoginDetails?.email,
          tempLoginDetails?.device_key,
        );
        history?.push(`/${match.params.id}`);
      }
    } catch (e) {
      console.log(e?.message);
      message.error(e?.message);
    }
  };

  return (
    <>
      <div className="t_signup">
        <Row style={{ padding: '10% 10%' }}>
          <Col span={8}>
            <h1>Registration</h1>
            <br></br>
            <div className="d-flex">
              <div
                style={{
                  width: '70%',
                  fontWeight: page != 1 ? 'bold' : '600',
                  opacity: page == 1 ? '0.5' : '1',
                }}
                onClick={() => {
                  setpage(1);
                }}
              >
                Step 1: Identify Yourself{' '}
              </div>
              <div>
                {page != 1 ? (
                  <CheckCircleOutlined style={{ marginLeft: '48px' }} />
                ) : (
                  ''
                )}
              </div>
            </div>
            <br></br>
            <div className="d-flex">
              <div
                style={{
                  width: '70%',
                  fontWeight: page >= 2 ? (page == 2 ? '600' : 'bold') : '',
                  opacity: page >= 2 ? (page == 2 ? '0.5' : '1') : '0.25',
                }}
                onClick={() => {
                  setpage(2);
                }}
              >
                Step 2: Terminals Username{' '}
              </div>
              <div>
                {page > 2 ? (
                  <CheckCircleOutlined style={{ marginLeft: '48px' }} />
                ) : (
                  ''
                )}
              </div>
            </div>
            <br></br>
            <div className="d-flex">
              <div
                style={{
                  width: '70%',
                  fontWeight: page >= 3 ? (page == 3 ? '600' : 'bold') : '',
                  opacity: page >= 3 ? (page == 3 ? '0.5' : '1') : '0.25',
                }}
              >
                Step 3: Profile Picture
              </div>
              <div>
                {page > 3 ? (
                  <CheckCircleOutlined style={{ marginLeft: '48px' }} />
                ) : (
                  ''
                )}
              </div>
            </div>{' '}
          </Col>
          <Col span={4}>
            <Divider type="vertical" style={{ height: '100%' }} />
          </Col>

          <Col span={12} style={{ textAlign: 'center' }}>
            <img src={appImage} style={{ height: '96px', width: '80%' }} />
            <br></br>

            {page == 1 ? (
              <>
                <Input
                  placeholder="First Name"
                  value={firstName}
                  onChange={(e) => setfirstName(e.target.value)}
                  className="gx-reg-m"
                />
                <Input
                  placeholder="Last Name"
                  value={lastName}
                  onChange={(e) => setlastName(e.target.value)}
                  style={{ marginTop: '16px', width: '80%' }}
                />
                <br></br>
                <Button
                  style={{
                    textAlign: 'center',
                    marginTop: '48px',
                    backgroundColor: `#${appColor}`,
                  }}
                  onClick={() => {
                    if (firstName && lastName) {
                      setpage(2);
                    }
                  }}
                >
                  Next Step
                </Button>
              </>
            ) : (
              ''
            )}

            {page == 2 ? (
              <>
                <div
                  className="d-flex"
                  style={{ marginTop: '48px', marginLeft: '108px' }}
                >
                  <Input value="@" style={{ width: '40px' }} />
                  <Input
                    placeholder="Username"
                    value={username}
                    onChange={async (e) => {
                      let n1 = e.target.value;
                      setusername(n1);
                      try {
                        let res =
                          await Gx_terminal_user_api.checkUserNameIsAvailable(
                            e?.target?.value,
                          );

                        if (res) {
                          setbgColor(`#${appColor}`);
                        } else {
                          setbgColor('red');
                        }
                      } catch (e) {
                        message.error(e?.message);
                      }
                    }}
                    style={{ width: '70%' }}
                  />
                  <div style={{ width: '20px', backgroundColor: bgColor }}>
                    {' '}
                  </div>
                </div>
                <Button
                  style={{ marginTop: '48px', backgroundColor: `#${appColor}` }}
                  onClick={() => {
                    if (bgColor != 'red') {
                      setpage(3);
                    }
                  }}
                >
                  Next Step
                </Button>
              </>
            ) : (
              ''
            )}

            {page == 3 || page == 4 ? (
              <div style={{ textAlign: 'center' }}>
                {imageURL ? (
                  <>
                    <img src={imageURL} className="gx-image-reg" />
                    <br></br>
                    <input
                      className="fileInput mt-4"
                      type="file"
                      accept="image/x-png,image/jpeg"
                      onChange={(e) => handleImageChange(e)}
                      style={{ marginTop: '24px', marginLeft: '25%' }}
                    />
                    <br></br>
                    <button
                      className="mt-4 cust_btn"
                      style={{ backgroundColor: `#${appColor}` }}
                      onClick={() => handleComplete()}
                    >
                      Complete
                    </button>
                  </>
                ) : (
                  <>
                    <img
                      src={profile}
                      style={{ width: '96px', marginTop: '48px' }}
                    />
                    <br></br>
                    <input
                      className="fileInput"
                      type="file"
                      accept="image/x-png,image/jpeg"
                      onChange={(e) => handleImageChange(e)}
                      style={{ marginTop: '24px', marginLeft: '25%' }}
                    />
                    <br></br>
                    {load_img ? 'Uploading ...' : ''}
                    {imageURL ? 'Upload Successful' : ''}
                  </>
                )}
              </div>
            ) : (
              ''
            )}
          </Col>
        </Row>
      </div>
      <Gx_mobile_view_landing />
    </>
  );
}

export default withRouter(Gx_TerminalSignUp);
