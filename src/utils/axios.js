import axios from 'axios';
const Axios = axios.create({
  baseURL: 'https://api.terminals.dev/',
});

// Axios.interceptors.response.use(
//   (response) => response,
//   async (error) => {
//     const originalRequest = error.config;

//     if (error.response.status === 401) {
//       const refreshToken = localStorage.getItem('tokenAppAccessToken');

//       if (refreshToken) {
//         const resp = await axios.post(
//           'https://gxauth.apimachine.com/gx/user/refresh/tokens',
//           {
//             refreshToken: localStorage.getItem('tokenAppAccessToken'),
//             device_key: localStorage.getItem('device_key'),
//           },
//         );

//         if (resp.data.status) {
//           localStorage.setItem('tokenAppToken', resp.data.accessToken);

//           originalRequest.headers[
//             'Authorization'
//           ] = `Bearer ${localStorage.getItem('tokenAppToken')}`;

//           return Axios(originalRequest);
//         } else {
//           localStorage.clear();
//           return Promise.reject(error);
//         }
//       } else {
//         return Promise.reject(error);
//       }
//     }
//     // specific error handling done elsewhere
//     return Promise.reject(error);
//   },
// );
export default Axios;
