import Axios from '../axios';

const GX_apis = {
  getAllTerminals: async () => {
    try {
      let res = await Axios.get(`terminal/all/stats`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getBankerCounts: async () => {
    try {
      let res = await Axios.get(`banker/followerCount`);
      return res.data;
    } catch (error) {
      if (error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  addUserTerminal: async () => {
    try {
      let res = await Axios.get(`terminal-lite/stats/withTerminal`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  newUserTerminal: async () => {
    try {
      let res = await Axios.get(`terminal-lite/stats/withTerminal`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getTerminalById: async (id) => {
    try {
      let res = await Axios.get(`terminal/byId/${id}`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getCoinPairDataById: async (id) => {
    try {
      let res = await Axios.get(`terminal/pair/byQuoteAsset/byTID/${id}`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getStatsChartsAndVolume: async (trade, id) => {
    try {
      let res = await Axios.get(
        `/stats/trade/chartAndVolumeStats/${trade?.pair}?time=${id}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getBaseFormattedPrices: async (trade, id) => {
    try {
      let res = await Axios.get(
        `terminal/pair/prices?id=${id}&toAsset=${trade}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getOrderStatsInCharts: async (pair, date) => {
    try {
      let res = await Axios.get(
        `order-stats/chartPrices?basePair=${pair}&dateType=${date}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getBaseFormattedPricesInUSD: async (trade, id) => {
    try {
      let res = await Axios.get(
        `terminal/pair/prices?id=${id}&toAsset=${trade}&valueIn=USD`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};
export default GX_apis;
