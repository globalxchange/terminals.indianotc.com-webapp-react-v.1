import Axios from 'axios';
const Gx_commsService = {
  getCoinPrices: async () => {
    try {
      let res = await Axios.get(
        `https://comms.globalxchange.com/coin/getCmcPrices`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getInnerVaultcoins: async () => {
    try {
      let res = await Axios.get(
        `https://comms.globalxchange.com/coin/vault/coins_data?email=${localStorage.getItem(
          'tokenAppLoginAccount',
        )}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getVaultscoinsService: async (data) => {
    try {
      let res = await Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/coins/get',
        data,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  get_affiliate_logs: async (email) => {
    try {
      let res = await Axios.post(
        'https://comms.globalxchange.com/get_affiliate_data_no_logs',
        {
          email,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  sendInvite: async (body) => {
    try {
      let res = await Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/send/app/links/invite',
        body,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  checkUsernameAndEmail: async (body) => {
    try {
      let res = await Axios.post(
        `https://comms.globalxchange.com/check/username/email`,
        body,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  login: async (email, pass, gpin, googlePin) => {
    try {
      let body = {
        email,
        password: pass,
      };
      if (gpin) {
        body.totp_code = googlePin;
      }
      let res = await Axios.post(
        'https://gxauth.apimachine.com/gx/user/login',
        body,
      );

      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getForex: async (id) => {
    try {
      let res = await Axios.get(
        `https://comms.globalxchange.com/forex/convert?buy=BTC&from=${id}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  uploadFileinDrive: async (
    email,
    path_inside_brain,
    token,
    fileName,
    formData,
  ) => {
    try {
      let response = await Axios.post(
        `https://drivetest.globalxchange.io/file/dev-upload-file?email=${email}&path=${path_inside_brain}&token=${token}&name=${fileName}`,
        formData,
        {
          headers: {
            'Access-Control-Allow-Origin': '*',
          },
        },
      );
      return response.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  regsiterUser: async (body) => {
    try {
      let res = await Axios.post(
        'https://gxauth.apimachine.com/gx/user/signup',
        body,
      );

      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  regsiterUserConfirm: async (body) => {
    try {
      let res = await Axios.post(
        'https://gxauth.apimachine.com/gx/user/confirm',
        body,
      );

      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getUserTerminalDetailsFromGx: async (data) => {
    try {
      let res = await Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/get/user/app/profile',
        data,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getRegisteredAppsData: async (id) => {
    try {
      let res = await Axios.get(
        `https://comms.globalxchange.com/gxb/apps/registered/user?email=${id}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  getExchangedRates: async (n1, symbol) => {
    try {
      let res = await Axios.get(
        `https://comms.globalxchange.com/forex/getdetails?buy=${n1}&from=${symbol}`,
      );

      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  WithdrawToSubApp: async (body) => {
    try {
      let res = await Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/fund/lx',
        body,
      );

      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};
export default Gx_commsService;
