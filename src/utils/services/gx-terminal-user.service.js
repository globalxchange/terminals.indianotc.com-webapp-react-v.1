import Axios from '../axios';

const Gx_terminal_user_api = {
  checkToken: async () => {
    try {
      let res = await Axios.get(`check-token`, {
        headers: {
          email: localStorage.getItem('tokenAppLoginAccount'),
          token: localStorage.getItem('tokenAppToken'),
        },
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Not registered');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getAllTerminalUsers: async () => {
    try {
      let res = await Axios.get(`/user/all`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Not registered');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  checkUserNameIsAvailable: async (username) => {
    try {
      let res = await Axios.get(`user/username/available?username=${username}`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Not registered');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  checkUserIsRegisteredToTerminals: async (email) => {
    try {
      let res = await Axios.get(`user?email=${email}`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Not Registered');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  checkUserIsRegistered: async (id, headers) => {
    try {
      let res = await Axios.get(`terminal-user/registered-to-terminal/${id}`, {
        body: {},
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 500) {
        throw new Error('Invalid Terminal ID');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getUserRegisteredTerminals: async (headers) => {
    try {
      let res = await Axios.get(`/terminal-user/user-terminals`, { headers });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Not registered');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  addUserToTerminal: async (headers, id) => {
    try {
      let res = await Axios.post(
        `terminal-user/register/${id}`,
        {},
        { headers },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  addUserToTerminals: async (headers, body) => {
    try {
      let res = await Axios.post(`user/new`, body, { headers });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};

export default Gx_terminal_user_api;
