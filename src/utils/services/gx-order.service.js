import Axios from '../axios';
import axios from 'axios';
const Gx_order_apis = {
  getBasicOrderBook: async (pair) => {
    try {
      let res = await axios.get(
        `https://charts.terminals.dev/charts/binance/orderbook?basePair=${pair}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  placeMarketOrder: async (body, headers) => {
    try {
      let res = await Axios.post(`/order/place`, body, {
        headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getBalancesWallet: async (pair, headers) => {
    try {
      let res = await Axios.get(
        `userwallet/balance/basepair?basePair=${pair}`,
        {
          headers: headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getBalancesVault: async (pair, vault, headers) => {
    try {
      let res = await Axios.get(
        `vault-set/balance/basepair?basePair=${pair}&vaultSet=${vault}`,
        {
          headers: headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getOpenOrders: async (headers) => {
    try {
      let res = await Axios.get(
        `order-stats/user-orders?status=open`,

        {
          headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getOrderHistory: async (headers, pair) => {
    try {
      let res = await Axios.get(
        `order-stats/trade-history?basePair=${pair}`,

        {
          headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getMyTradesHistory: async (headers, page, limit) => {
    try {
      let res = await Axios.get(
        `order-stats/lite-order-transactions?email=${localStorage.getItem(
          'tokenAppLoginAccount',
        )}&page=${page}&limit=${limit}`,

        {
          headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getBuySellOrders: async (pair) => {
    try {
      let res = await Axios.get(
        `order-stats/orderbook?basePair=${pair}`,
        // {
        //   headers,
        // },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};
export default Gx_order_apis;
