import Axios from 'axios';
const Terminal_ChartService = {
  getForexChart: async (pair, timeFrame) => {
    try {
      let res = await Axios.get(
        `https://charts.terminals.dev/forex/polygon?basePair=${pair}&dateType=${timeFrame}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getTerminalLiteCharts: async (pair, timeFrame) => {
    try {
      let res = await Axios.get(
        `https://charts.terminals.dev/charts/binance/prices?basePair=${pair}&dateType=${timeFrame}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status !== 500 && error?.response?.data?.message) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getNonTerminalCharts: async (pair, date) => {
    try {
      let res = await Axios.get(
        `https://api.terminals.dev/order-stats/chartPrices?basePair=${pair}&dateType=${date}`,
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};
export default Terminal_ChartService;
