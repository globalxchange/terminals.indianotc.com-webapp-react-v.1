import Axios from '../axios';
const Gx_vault_apis = {
  getVaultsetStats: async (headers) => {
    try {
      let res = await Axios.get('/vault-set/user', {
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getVaultBalanaces: async (vault, headers) => {
    try {
      let res = await Axios.get(
        `vault-set/balance?email=${headers.email}&vaultSet=${vault}`,
        {
          headers: headers,
        },
      );
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getWalletBalanaces: async (headers) => {
    try {
      let res = await Axios.get(`/userwallet/balance?includeTotalUSD=true`, {
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getWalletTransactions: async (params, headers) => {
    try {
      let res = await Axios.get(`userwallet/transactions`, {
        params: params,
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getVaulTransactions: async (params, headers) => {
    try {
      let res = await Axios.get(`vault-set/transactions`, {
        params: params,
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  getCoinsData: async () => {
    try {
      let res = await Axios.get(`coin`);
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  depositFromSubApp: async (body, headers) => {
    try {
      let res = await Axios.post('userwallet/fundTerminals/subApp', body, {
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },

  vault_depositFromSubApp: async (body, headers) => {
    try {
      let res = await Axios.post('userwallet/fundTerminals/vault', body, {
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
  vault_withdrawFromSubApp: async (body, headers) => {
    try {
      let res = await Axios.post('userwallet/withdrawTerminals/vault', body, {
        headers: headers,
      });
      return res.data;
    } catch (error) {
      if (error?.response?.status === 401) {
        throw new Error('Session Expired');
      } else if (
        error?.response?.status !== 500 &&
        error?.response?.data?.message
      ) {
        throw new Error(error?.response?.data?.message);
      } else {
        throw new Error('something went wrong');
      }
    }
  },
};
export default Gx_vault_apis;
