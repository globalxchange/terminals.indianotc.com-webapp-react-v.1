const events = require('events');
const eventEmitter = new events.EventEmitter();

eventEmitter.setMaxListeners(2);

console.log(`++++++++++++++ Calling Websockets ++++++++++++`);

let ws;

const onCloseFunction = (lite) => {
  if (lite) {
    ws.onclose = (cg) => {
      if (cg.code === 4001) {
        console.log('yes maual closing');
      } else {
        ws = new WebSocket('wss://charts.terminals.dev');
        console.log('emit--- reconnect emit from forex');

        eventEmitter.emit(`orderbook-connected`);
        ws.onmessage = (event) => {
          eventEmitter.emit('orderbook-message', event.data);
        };
        onCloseFunction(true);
      }
    };
  } else {
    ws.onclose = (cg) => {
      if (cg.code === 4001) {
        // console.log('Manual closing');
      } else {
        console.log('emit--- reconnect emit from terminal lite ');

        ws = new WebSocket('wss://api.terminals.dev');
        eventEmitter.emit(`orderbook-connected`);
        ws.onmessage = (event) => {
          eventEmitter.emit('orderbook-message', event.data);
        };
        onCloseFunction(false);
      }
    };
  }
};

const init = (coin) => {
  if (coin?.is_forex) {
    console.log('forex pair connection');
    if (ws?.readyState === 1 && ws?.url === 'wss://api.terminals.dev/') {
      console.log('closing');
      // eventEmitter.removeAllListeners();
      ws.close(4001);
      // ws = undefined;
    }
    ws = new WebSocket('wss://charts.terminals.dev');
    ws.onopen = () => {
      console.log('emit---connect emit from forex  ');
      console.log(ws);
      eventEmitter.emit(`orderbook-connected`);
      ws.onmessage = (event) => {
        eventEmitter.emit('orderbook-message', event.data);
      };
    };
    onCloseFunction(true);
  } else {
    if (!coin.is_terminal_lite) {
      console.log('not a lite terminal');
      if (ws?.readyState === 1 && ws?.url === 'wss://charts.terminals.dev/') {
        console.log('closing');
        // eventEmitter.removeAllListeners();

        ws.close(4001);
      }
      ws = new WebSocket('wss://api.terminals.dev');
      ws.onopen = () => {
        console.log('emit---connect emit from non terminal lite ');

        eventEmitter.emit(`orderbook-connected`);
        console.log(coin);
        ws.onmessage = (event) => {
          eventEmitter.emit('orderbook-message', event.data);
        };
      };

      onCloseFunction(false);
    } else {
      console.log('yes a lite terminal');
      if (ws?.readyState === 1 && ws?.url === 'wss://api.terminals.dev/') {
        // console.log('closing');
        // eventEmitter.removeAllListeners();

        ws.close(4001);
        // ws = undefined;
      }
      ws = new WebSocket('wss://charts.terminals.dev');
      ws.onopen = () => {
        console.log('emit---connect emit from terminal lite ');

        eventEmitter.emit(`orderbook-connected`);
        ws.onmessage = (event) => {
          eventEmitter.emit('orderbook-message', event.data);
        };
      };

      onCloseFunction(true);
    }
  }
};
//clg

const orderbookWSHelpers = {
  joinBasePair: (currentPair, previousPair, setPreviousTradeCoin) => {
    if (!ws) {
      init(currentPair);
      return;
    }
    if (ws.readyState === 0) {
      console.log('not in ready state');
      setTimeout(() => {
        orderbookWSHelpers.joinBasePair(
          currentPair,
          previousPair,
          setPreviousTradeCoin,
        );
      }, 1000);
      return;
    }
    // checking if the websocket is closed
    if (ws.readyState === 2 || ws.readyState === 3) {
      console.log('checking state');

      init(currentPair);
      return;
    }
    if (previousPair) {
      console.log(`leaving previous base pair ${previousPair?.pair}`);
      ws.send(
        JSON.stringify({
          event: 'leaveBasePair',
          data: {
            basePair: previousPair?.pair,
          },
        }),
      );
      setPreviousTradeCoin(currentPair);
    }

    ws.send(
      JSON.stringify({
        event: 'joinBasePair',
        data: {
          basePair: currentPair?.pair,
        },
      }),
    );
    console.log(`joined base pair ${currentPair?.pair}`);
    // console.log(ws);
    if (!previousPair?.pair) {
      setPreviousTradeCoin(currentPair);
    }
  },
  leaveBasePair: (coins) => {
    ws.send(
      JSON.stringify({
        event: 'leaveBasePair',
        data: {
          basePair: coins?.pair,
        },
      }),
    );
    console.log('left the previous base pair' + coins?.pair);
  },
};

export { eventEmitter as OrderBookeventEmitter, orderbookWSHelpers, init };
