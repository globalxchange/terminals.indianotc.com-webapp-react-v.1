const orderbookHelpers = {
  calculateSumAndTotalBid: (orderBookList, type) => {
    let sellBaseSum = 0;
    let sellPairSum = 0;
    if (type) {
      orderBookList.sell = orderBookList.asks?.map((item) => {
        item.totalAsk = Number(item[1]) * Number(item[0]);
        sellBaseSum += Number(item[1]);
        item.baseSum = sellBaseSum;
        sellPairSum += Number(item.totalAsk);
        item.pairSum = sellPairSum;
        item.amount = item[1];
        item.price = item[0];
        return item;
      });

      let buyBaseSum = 0;
      let buyPairSum = 0;
      orderBookList.buy = orderBookList.bids?.map((item) => {
        item.totalBid = Number(item[1]) * Number(item[0]);
        buyBaseSum += Number(item[1]);
        item.baseSum = buyBaseSum;
        buyPairSum += Number(item.totalBid);
        item.pairSum = buyPairSum;
        item.amount = item[1];
        item.price = item[0];
        return item;
      });
    } else {
      orderBookList.sell = orderBookList.sell?.map((item) => {
        item.totalAsk = Number(item.amount) * Number(item.price);
        sellBaseSum += Number(item.amount);
        item.baseSum = sellBaseSum;
        sellPairSum += Number(item.totalAsk);
        item.pairSum = sellPairSum;
        return item;
      });

      let buyBaseSum = 0;
      let buyPairSum = 0;
      orderBookList.buy = orderBookList.buy?.map((item) => {
        item.totalBid = Number(item.amount) * Number(item.price);
        buyBaseSum += Number(item.amount);
        item.baseSum = buyBaseSum;
        buyPairSum += Number(item.totalBid);
        item.pairSum = buyPairSum;
        return item;
      });
    }

    return orderBookList;
  },
};

export default orderbookHelpers;
