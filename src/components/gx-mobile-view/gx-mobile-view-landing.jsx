import React, { useState } from 'react';
import {
  gxToken,
  apple,
  andorid,
  email1,
  sms,
} from '../../assets/image/terminal-images';
import axios from 'axios';
import { withRouter } from 'react-router';
import { Input, message, Select } from 'antd';
import { Modal as Modal2 } from 'antd';
import * as animationData from '../../assets/animation/new_slow_chart.json';
import '../../assets/scss/mainpage.scss';
import countries from '../../assets/js/phonenumbers';
import Lottie from 'react-lottie';
import Gx_commsService from '../../utils/services/gx-comms.service';
const { Option } = Select;

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};
const Gx_mobile_view_landing = () => {
  const [appType, setAppType] = useState('');
  const [appsMessageType, setappsMessageType] = useState('');
  const [appsEmail, setappsEmail] = useState('');
  const [appsPhoneCode, setappsPhoneCode] = useState('');
  const [appsPhoneNumber, setappsPhoneNumber] = useState('');
  const [showPhone2, setshowPhone2] = useState(false);
  const [appsPage, setAppsPage] = useState(1);

  //-------------------------------------------------------------------function------------------------------------------------------------------------------------//

  const handleSend = async (type) => {
    let p1 = appsPage;
    if (appsEmail === '' && appsPhoneNumber === '') {
      message.error(`Please enter a valid ${type}`);
      return;
    }
    setAppsPage('loading');
    let body = {
      userEmail: 'shorupan@nvestbank.com',
      app_code: 'lx',
      email: appsEmail,
      mobile: `${appsPhoneCode}${appsPhoneNumber}`,
      app_type: appType,
    };

    let res = await Gx_commsService.sendInvite(body);
    setAppsPage(p1);
    if (!res.status) {
      Modal2.error({
        title: 'Error',
        content: res.message,
      });
    } else {
      if (appsMessageType === 'email') {
        if (res.emailData.status) {
          setAppsPage(4);
        } else {
          setAppsPage(p1);
          Modal2.error({
            title: 'Error',
            content: res.emailData.message,
          });
        }
      } else if (appsMessageType === 'sms') {
        if (res.smsData.status) {
          setAppsPage(4);
        } else {
          setAppsPage(p1);
          Modal2.error({
            title: 'Error',
            content: res.smsData.message,
          });
        }
      }
    }
  };

  //-------------------------------------------------------------------function------------------------------------------------------------------------------------//

  return (
    <div className="gx-mobileRes2">
      {appsPage === 1 ? (
        <div className="gx-mobileRes-inner">
          <img src={gxToken} className="gx-terminal-logo" />
          <h2
            style={{
              color: '#292929',
              fontWeight: 'bold',
              marginTop: '20px',
            }}
          >
            Terminals
          </h2>
          <br></br>
          <span style={{ wordSpacing: '8px', textAlign: 'center' }}>
            Please Download The Mobile App For The Best Trading Experience. Full
            Trading Abilities Are Available On Destop Version Of This Website
          </span>
          <br></br>
          <div style={{ marginTop: '40px' }}>
            {/* <a href={androidLink}> */}
            <button
              className="mobileBtn mt-4"
              onClick={() => {
                setAppType('android');
                setAppsPage(2);
              }}
            >
              {' '}
              <img src={andorid} className="h_100 mr-2" /> Android
            </button>
            {/* </a> */}

            <br></br>
            {/* <a href={iosLink}> */}
            <button
              className="mobileBtn mt-4"
              onClick={() => {
                setAppType('ios');
                setAppsPage(2);
              }}
            >
              {' '}
              <img src={apple} className="h_100 mr-2" />
              IOS
            </button>
            {/* </a> */}
          </div>
        </div>
      ) : (
        ''
      )}

      {appsPage === 2 ? (
        <>
          <div className="gx-mobileRes-inner-2">
            <h4 className="gx-black mt-4 font-weight-bold">
              How Do You Want To Receive The Link?
            </h4>
            <br></br>
            <div className="gx-mb-buttons">
              <button
                className="app_btn"
                onClick={() => {
                  setappsMessageType('email');
                  setAppsPage(3);
                }}
              >
                {' '}
                <img src={email1} className=" mr-2" /> Email
              </button>
              <button
                className="app_btn "
                onClick={() => {
                  setappsMessageType('sms');
                  setAppsPage(3);
                }}
              >
                {' '}
                <img src={sms} className=" mr-2" />
                SMS
              </button>
            </div>
          </div>
        </>
      ) : (
        ''
      )}

      {appsPage === 3 ? (
        <>
          {appsMessageType === 'email' ? (
            <>
              {' '}
              <div className="gx-mobileRes-inner-2 mt-4">
                <h4 className="gx-black mt-4 font-weight-bold">Input Email</h4>

                <span>
                  Enter The Email You Want To Send The Invitation Link To
                </span>
                <br></br>
                <br></br>
                <div className="gx-100">
                  <div style={{ width: '80%' }}>
                    <Input
                      placeholder="Enter Email"
                      onChange={(e) => setappsEmail(e.target.value)}
                    />
                  </div>
                  <div style={{ width: '20%' }}>
                    <button
                      onClick={() => {
                        handleSend('Email');
                      }}
                    >
                      <img src={email1} className=" mr-2" />
                    </button>
                  </div>
                </div>
                <br />
                <br />
                <button
                  className="gx-back-button"
                  onClick={() => {
                    setAppsPage(2);
                  }}
                >
                  Back
                </button>
              </div>
            </>
          ) : (
            ''
          )}

          {appsMessageType === 'sms' ? (
            <>
              <div className="gx-mobileRes-inner-2 mt-4">
                {showPhone2 ? (
                  <>
                    <h4 className="gx-black mt-4 font-weight-bold">
                      Input Destination
                    </h4>

                    <span>
                      Enter The Phone Number You Want To Send The Invitation
                      Link To
                    </span>
                    <br></br>

                    <div className="d-flex mt-4">
                      <div style={{ width: '80%', height: '44px' }}>
                        <Input
                          style={{ height: '44px', boxShadow: 'none' }}
                          placeholder="Phone Number"
                          onChange={(e) => setappsPhoneNumber(e.target.value)}
                        />
                      </div>
                      <div style={{ width: '20%' }}>
                        <button
                          style={{ height: '44px', padding: '0px 10px' }}
                          onClick={() => {
                            handleSend('Phone Number');
                          }}
                        >
                          <img src={sms} height="20px" />
                        </button>
                      </div>
                    </div>
                    <br />
                    <button
                      className="gx-back-button"
                      onClick={() => {
                        setAppsPage(2);
                      }}
                    >
                      Back
                    </button>
                  </>
                ) : (
                  <>
                    {' '}
                    <h4 className="gx-black mt-4 font-weight-bold">
                      Get Country Code
                    </h4>
                    <div>
                      <Select
                        className="select-box-mobile"
                        showSearch
                        style={{ width: '100%', height: '44px' }}
                        placeholder="Select"
                        optionFilterProp="children"
                        onChange={(ele) => {
                          setappsPhoneCode(ele);
                          setshowPhone2(true);
                        }}
                      >
                        {countries.map((ele, i) => {
                          return (
                            <Option value={ele.code} key={i}>
                              {ele.code} {ele.name}
                            </Option>
                          );
                        })}
                      </Select>
                    </div>
                    <br />
                    <br />
                    <button
                      className="gx-back-button"
                      onClick={() => {
                        setAppsPage(2);
                      }}
                    >
                      Back
                    </button>
                  </>
                )}
              </div>
            </>
          ) : (
            ''
          )}
        </>
      ) : (
        ''
      )}

      {appsPage === 4 ? (
        <>
          <div className="mt-4">
            <h2 className="gx-black font-weight-bold">Congratulations</h2>
            <span>
              You Were Just Sent The Latest Version Of The {appType} GXTerminal
              App.
            </span>
          </div>

          <div className="d-flex mt-4">
            <button onClick={() => setAppsPage(1)} className="app_btn">
              {appType === 'android' ? 'ios' : 'Android'}
            </button>
            <button className="app_btn ml-4">Register</button>
          </div>
        </>
      ) : (
        ''
      )}

      {appsPage === 'loading' ? (
        <>
          {' '}
          <Lottie options={defaultOptions} height={400} width={200} />
        </>
      ) : (
        ''
      )}
    </div>
  );
};
export default withRouter(Gx_mobile_view_landing);
