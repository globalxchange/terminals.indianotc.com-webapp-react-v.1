import React from 'react';
import { withRouter } from 'react-router';
import { new_terminal_logo } from '../../assets/image/terminal-images';
const Gx_Reload = ({ history }) => {
  return (
    <div
      style={{
        textAlignLast: 'center',
        padding: '5vw',
      }}
    >
      <img
        src={new_terminal_logo}
        alt="logo"
        height={window.innerWidth < 650 ? '70px' : '100px'}
      />
      <br />

      <p style={{ marginTop: '10px' }}>
        {' '}
        Oops!! Looks like something went wrong
      </p>

      <p style={{ color: '#181818' }}>
        Click here to{' '}
        <a
          href="/"
          style={{ color: '#2f3193', fontWeight: '600' }}
          onClick={() => history?.push(`/`)}
        >
          Reload
        </a>
      </p>
    </div>
  );
};
export default withRouter(Gx_Reload);
