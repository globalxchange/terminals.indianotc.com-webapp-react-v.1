/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
import React, { useState, useContext, useEffect } from 'react';
import { OptionsContext } from '../../context/Context';
import { motion } from 'framer-motion';
import { Skeleton } from 'antd';
import { withRouter } from 'react-router-dom';

const Gx_coinsider = () => {
  const {
    selectedCoin,
    setSelectedCoin,
    tradeCoin,
    setTradeCoin,
    acceptedBasePairs,
    setterminalLiteList_pairs,
    coinModal,
    setcoinModal,
    handleLeaveBasePair,
  } = useContext(OptionsContext);

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    if (selectedCoin) {
      let n1 = acceptedBasePairs.find((ele) => {
        return ele.basePair == tradeCoin.quote;
      });
      setterminalLiteList_pairs(n1.pairs);
      setSelectedCoin(n1);
      setTradeCoin(n1.pairs[0]);
    }

    setLoading(false);
  }, []);

  const handleRotate = async (itm) => {
    setLoading(true);

    if (tradeCoin) {
      await handleLeaveBasePair();
    }
    setterminalLiteList_pairs(itm.pairs);
    setSelectedCoin(itm);
    setTradeCoin(itm.pairs[0]);
    setLoading(false);
  };

  return (
    <div
      className={`coins-sidebar d-flex flex-column`}
      style={{ marginTop: '48px' }}
      onClick={() => {
        if (coinModal) {
          setcoinModal(false);
        }
      }}
    >
      <div className="main d-flex flex-column h-100">
        <div
          className="coin-list flex-grow-1 d-flex flex-column position-relative"
          style={{ marginTop: '24px' }}
        >
          {loading ? (
            <div style={{ width: '96px', marginTop: '45vh' }}>
              <Skeleton style={{ transform: [{ rotate: '90deg' }] }} />
            </div>
          ) : (
            <>
              {acceptedBasePairs.map((itm, i) => {
                return (
                  <>
                    <motion.div>
                      <div
                        key={i}
                        className={`d-flex coin-itm flex-grow-1${
                          itm.quote === selectedCoin.quote ? 'active' : ''
                        }`}
                        onClick={() => handleRotate(itm, i)}
                      >
                        <motion.div
                          className="d-flex flex-column mx-auto my-3"
                          whileHover={{ scale: 1.5 }}
                        >
                          <img
                            src={itm ? itm.quoteImage : ''}
                            style={{
                              height:
                                itm.quote === selectedCoin.quote
                                  ? '60px'
                                  : '36px',
                              width:
                                itm.quote === selectedCoin.quote
                                  ? '60px'
                                  : '36px',
                              marginTop: '36px',
                              opacity:
                                itm.quote === selectedCoin.quote ? '1' : '0.2',
                            }}
                          />
                        </motion.div>
                      </div>
                    </motion.div>
                  </>
                );
              })}
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default withRouter(Gx_coinsider);
