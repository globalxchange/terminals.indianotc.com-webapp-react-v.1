import React, { useState, useContext, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Select, Modal as Modal2 } from 'antd';
import { OptionsContext } from '../../../context/Context';

import {
  new_email,
  new_apps,
  new_username,
  new_search,
} from '../../../assets/image/terminal-images';
import * as animationData from '../../../assets/animation/new_slow_chart.json';
import { motion } from 'framer-motion';
import Gx_terminal_user_api from '../../../utils/services/gx-terminal-user.service';
import Lottie from 'react-lottie';
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

const { Option } = Select;

const Gx_AdminPage = ({ history }) => {
  const {
    appColor,
    userViewClicked,
    setuserViewClicked,
    appfullLogoColoured,
    showAdminPage,
    setshowAdminPage,
  } = useContext(OptionsContext);
  // const [show, setShow] = useState(false);
  const [showEnterEmail, setshowEnterEmail] = useState(false);
  const [enteredEmail, setenteredEmail] = useState('');
  const [userList, setuserList] = useState([]);
  const [userList_copy, setuserList_copy] = useState([]);
  const [loading, setloading] = useState(true);

  const handleClose = async () => {
    // setShow(false);
    setshowAdminPage(false);
    setshowEnterEmail(false);
    setuserViewClicked(false);
  };

  // useEffect(() => {
  //   console.log(userViewClicked);
  //   if (userViewClicked) {
  //     // setShow(true);
  //     setshowAdminPage(true);

  //     setshowEnterEmail(true);
  //   }
  // }, [userViewClicked]);

  const onEnterClick = async () => {
    if (enteredEmail !== '') {
      localStorage.setItem('tokenAppLoginAccount1', 'shorupan@gmail.com');
      localStorage.setItem('tokenAppLoginAccount', enteredEmail);
      window.location.reload();
    }
  };

  useEffect(async () => {
    setloading(true);
    let res = await Gx_terminal_user_api.getAllTerminalUsers();
    setuserList(res);
    setuserList_copy(res);
    setloading(false);
  }, []);

  const handleSearch = (value) => {
    let n1 = value;
    setenteredEmail('');

    let n2 = userList.filter((ele) => {
      return ele.email.toLowerCase().includes(n1.toLowerCase());
    });

    if (n1.length == 0) {
      setuserList_copy(userList);
    } else {
      setuserList_copy(n2);
    }
  };

  // useEffect(() => {
  //   if (showAdminPage) {
  //     setShow(!showAdminPage);
  //   }
  // }, [showAdminPage]);

  return (
    <>
      <div className="Vaults">
        <Modal
          show={showAdminPage}
          onHide={handleClose}
          className="modal-container-ham"
          centered
        >
          <>
            {loading ? (
              <Lottie
                options={defaultOptions}
                height={200}
                width={180}
                // style={{ marginTop: '25vh' }}
              />
            ) : (
              <div style={{ padding: '8%' }}>
                <img
                  src={appfullLogoColoured}
                  style={{
                    height: '64px',
                    display: 'block',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginBottom: '52px',
                    marginTop: '28px',
                  }}
                />

                {showEnterEmail ? (
                  <div style={{ textAlign: 'center' }}>
                    <Select
                      showSearch
                      style={{ width: '100%' }}
                      placeholder="Enter Email"
                      optionFilterProp="children"
                      onChange={(val) => setenteredEmail(val)}
                      onSearch={handleSearch}
                    >
                      {userList_copy.map((ele, i) => {
                        return (
                          <Option key={i} value={ele.email}>
                            {ele.email}
                          </Option>
                        );
                      })}
                    </Select>

                    <Button
                      onClick={() => onEnterClick()}
                      style={{
                        width: '100%',
                        marginTop: '12vh',
                        backgroundColor: { appColor },
                        borderRadius: '0px',
                        fontWeight: '800',
                      }}
                    >
                      Enter
                    </Button>
                  </div>
                ) : (
                  <div className="row">
                    <div
                      className="col-md-4"
                      style={{ padding: '4%' }}
                      onClick={() => setshowEnterEmail(true)}
                    >
                      <motion.div whileHover={{ scale: 1.2 }}>
                        <div
                          style={{
                            border: '0.5px solid #E7E7E7',
                            textAlign: 'center',
                            padding: '24px',
                          }}
                        >
                          <img src={new_email} className="mb-3" />
                          <br></br>
                          <span style={{ fontWeight: '600' }}>Email</span>
                        </div>
                      </motion.div>
                    </div>

                    <div className="col-md-4" style={{ padding: '4%' }}>
                      <motion.div whileHover={{ scale: 1.2 }}>
                        <div
                          style={{
                            border: '0.5px solid #E7E7E7',
                            textAlign: 'center',
                            padding: '24px',
                            opacity: '0.4',
                          }}
                        >
                          <img src={new_username} className="mb-2" />
                          <br></br>
                          <span style={{ fontWeight: '600' }}>Username</span>
                        </div>
                      </motion.div>
                    </div>

                    <div className="col-md-4" style={{ padding: '4%' }}>
                      <motion.div whileHover={{ scale: 1.2 }}>
                        <div
                          style={{
                            border: '0.5px solid #E7E7E7',
                            textAlign: 'center',
                            padding: '24px',
                            opacity: '0.4',
                          }}
                        >
                          <img src={new_search} className="mb-2" />
                          <br></br>
                          <span style={{ fontWeight: '600' }}>Search</span>
                        </div>
                      </motion.div>
                    </div>

                    <div className="col-md-4" style={{ padding: '4%' }}>
                      <motion.div whileHover={{ scale: 1.2 }}>
                        <div
                          style={{
                            border: '0.5px solid #E7E7E7',
                            textAlign: 'center',
                            padding: '24px',
                            opacity: '0.4',
                          }}
                        >
                          <img src={new_apps} className="mb-2" />
                          <br></br>
                          <span style={{ fontWeight: '600' }}>Apps</span>
                        </div>
                      </motion.div>
                    </div>
                  </div>
                )}
              </div>
            )}
          </>
        </Modal>
      </div>
    </>
  );
};

export default Gx_AdminPage;
