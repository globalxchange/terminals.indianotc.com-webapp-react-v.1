import React, { useContext, useEffect, useState } from 'react';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
import Gx_commsService from '../../../../../utils/services/gx-comms.service';
import { notification, Skeleton } from 'antd';
const Gx_page_5 = ({
  setPage,
  innerVaultCoin,
  deposit_withdraw_method,
  innerVaultAppsCoin,
  vaultCoin,
  setconvertedValue,
  enteredAmount,
  setenteredAmount,
  page,
}) => {
  const { appColor, findFiat, logout } = useContext(OptionsContext);
  const [localLoading, setLocalloading] = useState(true);
  const [convertedRate, setCovertedRate] = useState('');

  useEffect(() => {
    console.log(page);
    if (page === '5') {
      convertAmount();
    }
  }, [page]);

  const convertAmount = async () => {
    console.log('in conv');
    setLocalloading(true);
    setconvertedValue('');
    let n1 =
      deposit_withdraw_method === 'Vault'
        ? innerVaultCoin?.coinSymbol
        : innerVaultAppsCoin.coinSymbol;

    if (vaultCoin?.symbol === 'GXT' && n1 === 'GXT') {
      setconvertedValue(1);
      setLocalloading(false);
    } else {
      try {
        let data = await Gx_commsService.getExchangedRates(
          n1,
          vaultCoin?.coin_code,
        );

        if (data.status) {
          setconvertedValue(data.gx_price);
          setCovertedRate(data.gx_price);
          setLocalloading(false);
        }
        setLocalloading(false);
      } catch (E) {
        notification?.error(E?.message);
        setLocalloading(false);
      }
    }
  };
  return (
    <>
      <div
        style={{
          height: 'calc(70vh - 40px)',
          overflowY: 'scroll',
          width: '100%',
        }}
      >
        {localLoading ? (
          <Skeleton />
        ) : (
          <div
            style={{
              position: 'absolute',
              left: '50%',
              top: '50%',
              transform: 'translate(-50%,-50%)',
              color: `#${appColor}`,
            }}
          >
            <h2
              className="mt-4"
              style={{
                color: `#${appColor}`,
                marginBottom: '2px',
              }}
            >
              {findFiat(vaultCoin?.symbol)
                ? parseFloat(vaultCoin?.balance)?.toFixed(2)
                : parseFloat(vaultCoin?.balance)?.toFixed(4)}
              <span> {vaultCoin?.symbol}</span>
            </h2>

            <strong>Vault Balance</strong>
            <br></br>

            <div className="d-flex mt-4">
              <input
                placeholder="0.00"
                value={enteredAmount}
                style={{
                  width: '240px',
                  border: '0px',
                  borderBottom: `1px solid #${appColor}`,
                }}
                onChange={(e) => setenteredAmount(e.target.value)}
              />
              <span>
                {deposit_withdraw_method == 'Vault'
                  ? innerVaultCoin.coinSymbol
                  : innerVaultAppsCoin.coinSymbol}
              </span>
            </div>

            <div
              className="d-flex"
              style={{ pointerEvents: 'none' }}
              disabled={true}
            >
              <p style={{ width: '240px' }}>
                {enteredAmount * parseFloat(convertedRate)?.toFixed(4)}
              </p>
              <span>{vaultCoin?.coin_code}</span>
            </div>

            <button
              style={{
                backgroundColor: `#${appColor}`,
                color: 'white',
                border: `1px solid #${appColor}`,
                height: '34px',
                marginLeft: '6vw',
              }}
              onClick={() => {
                setPage('6');
              }}
            >
              Next
            </button>
          </div>
        )}
      </div>
    </>
  );
};
export default Gx_page_5;
