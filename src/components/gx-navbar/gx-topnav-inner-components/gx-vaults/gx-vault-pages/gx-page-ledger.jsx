import React, { useContext, useEffect, useState } from 'react';
import { Skeleton, Tabs, Modal as Modal2 } from 'antd';
import { OptionsContext } from '../../../../../context/Context';
import Gx_vault_apis from '../../../../../utils/services/gx-vaults.service';
import '../../../../../assets/scss/gx-vaults.scss';

const { TabPane } = Tabs;

const Gx_page_ledger = ({
  ledgerLoading,
  clickedIcon,
  setledgerLoading,
  selectedTab,
  setloginModal,
}) => {
  const { appColor } = useContext(OptionsContext);
  const [selected, setSelected] = useState('1');
  const [userTransactions, setUserTransactions] = useState([]);

  useEffect(() => {
    // console.log('triggering');
    getFilterDiv();
  }, [selected]);

  const getFilterDiv = async () => {
    console.log(selected);
    setledgerLoading(true);
    let headers = {
      email: localStorage.getItem('tokenAppLoginAccount'),
      token: localStorage.getItem('tokenAppToken'),
    };
    let n1 = [];
    try {
      if (selected === '3') {
        console.log('debit');
        let params = {
          txType: 'debit',
          asset: clickedIcon.coin_code,
          page: '1',
          limit: 50,
        };
        let res = '';
        if (selectedTab.nick_name === 'Main Vault') {
          res = await Gx_vault_apis.getWalletTransactions(params, headers);
        } else {
          params.vaultSet = selectedTab?.vault_set;

          res = await Gx_vault_apis.getVaulTransactions(params, headers);
        }
        setUserTransactions(res?.items);
      } else if (selected === '2') {
        console.log('credit');

        let params = {
          txType: 'credit',
          asset: clickedIcon.coin_code,
          page: '1',
          limit: 50,
        };
        let res = '';
        if (selectedTab.nick_name === 'Main Vault') {
          res = await Gx_vault_apis.getWalletTransactions(params, headers);
        } else {
          params.vaultSet = selectedTab?.vault_set;

          res = await Gx_vault_apis.getVaulTransactions(params, headers);
        }

        setUserTransactions(res?.items);
      } else if (selected === '1') {
        console.log('dsad');
        let params = {
          asset: clickedIcon.coin_code,
          page: '1',
          limit: 50,
        };
        let res = '';
        if (selectedTab.nick_name === 'Main Vault') {
          res = await Gx_vault_apis.getWalletTransactions(params, headers);
        } else {
          params.vaultSet = selectedTab?.vault_set;

          res = await Gx_vault_apis.getVaulTransactions(params, headers);
        }
        n1 = res?.items;
        setUserTransactions(n1);
      }
      setledgerLoading(false);
    } catch (error) {
      if (
        error?.message === 'User is not registered for terminals' ||
        error?.message === 'Session Expired'
      ) {
        Modal2.error({
          title: 'Error',
          content: 'Token Expired,',
        });
        localStorage?.clear();
        setloginModal(true);
      } else {
        Modal2.error({
          title: 'Error',
          content: error?.message,
        });
      }
      setledgerLoading(false);
    }
  };
  const display = () => {
    if (userTransactions?.length === 0) {
      return 'No Transactions For This Selected Coin';
    }
    return (
      <>
        {ledgerLoading ? (
          <Skeleton />
        ) : (
          <>
            {userTransactions?.length === 0 ? (
              'No Transactions For This Selected Coin'
            ) : (
              <>
                {userTransactions?.map((ele, i) => {
                  return (
                    <div
                      style={{
                        height: '72px',
                        border: '0.25px solid #E7E7E7',
                        marginTop: '10px',
                        color:
                          ele.tx_type === 'credit' ? `#${appColor}` : 'red',
                      }}
                      key={i}
                    >
                      <div className="d-flex" style={{ marginTop: '18px' }}>
                        <img
                          src={clickedIcon?.image}
                          style={{
                            width: '40px',
                            height: '40px',
                            marginLeft: '2.5vw',
                          }}
                        />
                        <span
                          className="mt-2"
                          style={{
                            width: '120px',
                            marginLeft: '7vw',
                          }}
                        >
                          {parseFloat(ele?.amount)?.toFixed?.(4)}
                        </span>
                        <span
                          className="mt-2"
                          style={{
                            width: '120px',
                            marginLeft: '7vw',
                          }}
                        >
                          {parseFloat(ele?.previous_balance)?.toFixed?.(4)}
                        </span>
                        <span
                          className="mt-2"
                          style={{
                            width: '120px',
                            marginLeft: '7vw',
                          }}
                        >
                          {parseFloat(ele?.updated_balance)?.toFixed?.(4)}
                        </span>
                        <span
                          className="mt-2"
                          style={{
                            width: '120px',
                            marginLeft: '7vw',
                          }}
                        >
                          {ele.tx_method}
                        </span>
                      </div>
                    </div>
                  );
                })}
              </>
            )}
          </>
        )}
      </>
    );
  };

  return (
    <div style={{ marginTop: '64px', padding: '24px' }}>
      {ledgerLoading ? (
        <>Loading....</>
      ) : (
        <Tabs
          className="gx-ledger"
          onChange={(e) => {
            setSelected(e);
          }}
          defaultActiveKey={'1'}
          activeKey={selected}
        >
          <TabPane tab="All" key="1">
            <div
              style={{
                height: 'calc(70vh - 160px)',
                overflowY: 'scroll',
                width: '100%',
              }}
            >
              {selected === '1' && display()}
            </div>
          </TabPane>
          <TabPane
            style={{
              height: 'calc(70vh - 160px)',
              overflowY: 'scroll',
              width: '100%',
            }}
            tab="Deposits"
            key="2"
          >
            {selected === '2' && display()}
          </TabPane>
          <TabPane
            style={{
              height: 'calc(70vh - 160px)',
              overflowY: 'scroll',
              width: '100%',
            }}
            tab="Withdrawls"
            key="3"
          >
            {selected === '3' && display()}
          </TabPane>
        </Tabs>
      )}{' '}
    </div>
  );
};
export default Gx_page_ledger;
