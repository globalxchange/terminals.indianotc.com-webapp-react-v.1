import React, { useContext } from 'react';
import { OptionsContext } from '../../../../../context/Context';

const Gx_page_7 = ({
  clickeduserApps,
  deposit_withdraw_method,
  innerVaultAppsCoin,
  innerVaultCoin,
  vaultCoin,
  innerVaultBalances,
  allBalances,
  subAppBalances,
  handleClose,
}) => {
  console.log(innerVaultBalances);
  const { appColor, findFiat } = useContext(OptionsContext);

  const getTop = () => {
    let symbol = '';
    let result = '';
    if (deposit_withdraw_method === 'Vault') {
      symbol = innerVaultCoin?.coinSymbol;
      result = innerVaultBalances?.find((item) => item.coinSymbol === symbol);
    } else {
      symbol = innerVaultAppsCoin?.coinSymbol;
      result = subAppBalances?.find((item) => item.coinSymbol === symbol);
    }
    let final = findFiat(result?.coinSymbol)
      ? parseFloat(result?.coinValue)?.toFixed(2)
      : parseFloat(result?.coinValue)?.toFixed(4);
    return final;
  };

  const getBottom = () => {
    let result = allBalances.find(
      (item) => item.coin_code === vaultCoin?.coin_code,
    );
    let final = findFiat(result?.coin_code)
      ? parseFloat(result?.balance)?.toFixed(2)
      : parseFloat(result?.balance)?.toFixed(4);

    return final;
  };

  return (
    <div style={{ marginTop: '64px', padding: '24px' }}>
      <div
        style={{
          height: 'calc(70vh - 160px)',
          overflowY: 'scroll',
          width: '100%',
        }}
        className="newVaultDesign"
      >
        <span style={{ color: `#${appColor}` }}>
          Your New{' '}
          {deposit_withdraw_method === 'Vault'
            ? 'GX Vault '
            : clickeduserApps?.app_name}
          {deposit_withdraw_method === 'Vault'
            ? innerVaultCoin?.coinSymbol
            : innerVaultAppsCoin?.coinSymbol}{' '}
          Vault Balanace
        </span>
        <br></br>
        <div
          className="d-flex"
          style={{
            marginTop: '8px',
            border: '1px solid #E7E7E7',
            padding: '24px',
          }}
        >
          <img
            src={
              deposit_withdraw_method === 'Vault'
                ? innerVaultCoin?.coinImage
                : innerVaultAppsCoin.coinImage
            }
            style={{
              width: '40px',
              height: '40px',
              marginLeft: '2vw',
            }}
          />
          <span
            className="mt-2"
            style={{
              width: '120px',
              marginLeft: '1vw',
            }}
          >
            {deposit_withdraw_method === 'Vault'
              ? innerVaultCoin?.coinName
              : innerVaultAppsCoin.coinName}
          </span>
          <span className="mt-2" style={{ width: '80px', marginLeft: '32vw' }}>
            {getTop()}
            <small>
              {deposit_withdraw_method === 'Vault'
                ? innerVaultCoin?.coinSymbol
                : innerVaultAppsCoin.coinSymbol}
            </small>
          </span>
        </div>
        <div className="mt-2 newVaultDesign1"></div>

        <span style={{ color: `#${appColor}` }}>
          Your New Master Vault
          {` ${vaultCoin?.coin_code}`} Balance
        </span>
        <br></br>
        <div
          className="d-flex"
          style={{
            marginTop: '8px',
            border: '1px solid #E7E7E7',
            padding: '24px',
          }}
        >
          <img
            src={vaultCoin?.image}
            style={{
              width: '40px',
              height: '40px',
              marginLeft: '2vw',
            }}
          />
          <span
            className="mt-2"
            style={{
              width: '120px',
              marginLeft: '1vw',
            }}
          >
            {vaultCoin?.coin_name}
          </span>
          <span
            className="mt-2"
            style={{ width: '80px', marginLeft: '32vw', whiteSpace: 'nowrap' }}
          >
            {getBottom()}
            {` ${vaultCoin?.coin_code}`}
          </span>
        </div>

        <div
          style={{
            position: 'absolute',
            bottom: '24px',
            right: '24px',
          }}
        >
          <div className="d-flex newVaultDesign2">
            <button
              style={{
                backgroundColor: 'white',
                color: `#${appColor}`,
                border: `1px solid #${appColor}`,
                height: '34px',

                marginLeft: '2vw',
              }}
              onClick={handleClose}
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Gx_page_7;
