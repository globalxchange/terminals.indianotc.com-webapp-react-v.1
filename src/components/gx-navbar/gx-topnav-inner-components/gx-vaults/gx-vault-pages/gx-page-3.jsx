import React, { useContext, useEffect, useState } from 'react';
import { Skeleton, Input, Modal as Modal2, notification } from 'antd';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
import { vault_c } from '../../../../../assets/image/terminal-images';
import Gx_commsService from '../../../../../utils/services/gx-comms.service';
const Gx_page_3 = ({
  setPage,
  setselectAppLoader,
  selectAppLoader,
  setclickeduserApps,
  setsubAppBalances_copy,
  setsubAppBalances,
  setUserApps,
  page,
}) => {
  const { appColor } = useContext(OptionsContext);
  const [userAppsCopy, setUserAppsCopy] = useState([]);
  useEffect(() => {
    if (page === '3') {
      getAppsData();
    }
  }, [page]);

  const getAppsData = async () => {
    try {
      setselectAppLoader(true);
      let res = await Gx_commsService.getRegisteredAppsData(
        localStorage.getItem('tokenAppLoginAccount'),
      );

      if (res.status) {
        setUserApps(res.userApps);
        setUserAppsCopy(res.userApps);
      }
      setselectAppLoader(false);
    } catch (e) {
      notification.error(e?.message);
    }
  };
  const handleUserApp = async (ele) => {
    setselectAppLoader(true);
    setclickeduserApps(ele);
    let res = await Gx_commsService.getVaultscoinsService({
      app_code: ele.app_code,
      profile_id: ele.profile_id,
    });

    if (res.status) {
      setsubAppBalances(res.coins_data);
      setsubAppBalances_copy(res.coins_data);
    }
    setPage('4');
    setselectAppLoader(false);
  };
  return (
    <>
      <div style={{ marginTop: '96px', padding: '24px' }}>
        <h4
          style={{
            fontWeight: 'bold',
            float: 'left',
            color: `#${appColor}`,
          }}
        >
          Select The App
        </h4>
        <Input
          placeholder="Search Your Registered Apps"
          style={{
            float: 'right',
            width: '50%',
            marginRight: '36px',
          }}
          // onChange={(e) => handleSearch(e)}
        />
        <br></br>
        <div
          style={{
            height: 'calc(70vh - 160px)',
            overflowY: 'scroll',
            width: '100%',
          }}
        >
          {selectAppLoader ? (
            <Skeleton />
          ) : (
            <div className="row">
              <>
                {userAppsCopy?.length === 0 ? (
                  <div style={{ marginLeft: '40px' }}>
                    No Apps Mapped Under This Logged in Account
                  </div>
                ) : (
                  <>
                    {userAppsCopy?.map((ele, i) => {
                      return (
                        <div
                          key={i}
                          className="col-md-3"
                          onClick={() => handleUserApp(ele)}
                        >
                          <div className="gx-flex-center">
                            {ele?.app_icon ? (
                              <img
                                src={ele.app_icon}
                                style={{
                                  width: '64px',
                                  height: '64px',
                                  margin: '35px 0px 10px 0px',
                                  boxShadow:
                                    '2px 2px 2px 2px rgba(0, 0, 0, 0.05)',
                                  borderRadius: '4px',
                                }}
                                alt={vault_c}
                              />
                            ) : (
                              <img
                                src={vault_c}
                                style={{
                                  width: '64px',
                                  height: '64px',
                                  margin: '35px 0px 10px 0px',

                                  boxShadow:
                                    '2px 2px 2px 2px rgba(0, 0, 0, 0.05)',
                                  borderRadius: '4px',
                                }}
                                alt={vault_c}
                              />
                            )}
                            <p>{ele.app_name}</p>
                          </div>
                        </div>
                      );
                    })}
                  </>
                )}
              </>
            </div>
          )}
        </div>
      </div>
    </>
  );
};
export default Gx_page_3;
