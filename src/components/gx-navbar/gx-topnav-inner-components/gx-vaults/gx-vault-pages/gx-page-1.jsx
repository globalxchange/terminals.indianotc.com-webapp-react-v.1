import React, { useContext } from 'react';
import { Input, Skeleton, Tabs, Modal as Modal2 } from 'antd';
import { OptionsContext } from '../../../../../context/Context';
import { motion } from 'framer-motion';
import '../../../../../assets/scss/gx-vaults.scss';
import { vault_c } from '../../../../../assets/image/terminal-images';
const { TabPane } = Tabs;

const Gx_Page_1 = ({
  balenceLoading,
  selectedTab,
  setAllBalancesCopy,
  allBalances,
  vaultSets,
  setselectedTab,
  setselectedTabIndex,
  selectedTabIndex,
  getUserWalletBalanceData,
  getUserWalletBalanceData2,
  allBalancesCopy,
  setPage,
  setclickedIcon,
  setdeposite_withdraw,
  setVaultCoin,
}) => {
  const { appColor, findFiat, appfullLogoColoured } =
    useContext(OptionsContext);

  const handleSearch_1 = (e) => {
    let n1 = e.target.value;

    if (n1.length === 0) {
      setAllBalancesCopy([...allBalances]);
    } else {
      let i1 = allBalances?.filter((ele) => {
        return (
          ele.coin_name?.toLowerCase()?.includes(n1?.toLowerCase()) ||
          ele.coin_code?.toLowerCase()?.includes(n1?.toLowerCase())
        );
      });
      setAllBalancesCopy([...i1]);
    }
  };

  const handleClick = async (n1, ele) => {
    setdeposite_withdraw(n1);
    // settype(n1);
    setVaultCoin(ele);
    setPage('2');
  };

  const handleLedger = async (ele) => {
    setclickedIcon(ele);
    setPage('ledger');
  };

  const showVaultUI = (showButtons) => {
    return (
      <>
        {allBalancesCopy?.map((ele, i) => {
          return (
            <div className="gx-page1-coins" key={i}>
              <div className="d-flex">
                <img src={ele.image} className="mt-1 gx-p1-three-img" />
                <span className="mt-2 gx-page1-three-span1">
                  {ele.coin_name}
                </span>
                <span className="mt-2 gx-page1-three-span2">
                  <div className="gx-page1-tsi">
                    <img src={ele.image} className="mr-2" />
                    <small className="gx-page1-three-s1">{ele.symbol} </small>
                  </div>
                  <span className="gx-page1-three-s2">
                    {findFiat(ele?.coin_code)
                      ? ele?.balance
                        ? parseFloat(ele?.balance).toFixed(2)
                        : '0.0000'
                      : ele?.balance
                      ? parseFloat(ele?.balance).toFixed(4)
                      : '0.0000'}{' '}
                  </span>
                </span>
                <motion.div
                  whileHover={{ scale: 1.2 }}
                  className="gx-page1-md-1"
                >
                  <button
                    className="gx-page1-md-1-btn"
                    style={{
                      color: `#${appColor}`,
                      border: `1px solid #${appColor}`,
                    }}
                    onClick={() => {
                      setclickedIcon(ele);
                      handleLedger(ele);
                    }}
                  >
                    Ledger
                  </button>
                </motion.div>

                {showButtons ? (
                  <>
                    <motion.div
                      whileHover={{ scale: 1.2 }}
                      className="gx-page1-md-2"
                    >
                      {' '}
                      <button
                        className="gx-page1-md-2-btn"
                        style={{
                          backgroundColor: `#${appColor}`,

                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => handleClick('DEPOSIT', ele)}
                      >
                        Deposit
                      </button>
                    </motion.div>
                    <motion.div
                      whileHover={{ scale: 1.2 }}
                      className="gx-page1-md-3"
                    >
                      <button
                        className="gx-page1-md-3-btn"
                        style={{
                          color: `#${appColor}`,
                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => handleClick('WITHDRAW', ele)}
                      >
                        Withdraw
                      </button>
                    </motion.div>
                  </>
                ) : (
                  ''
                )}
              </div>
            </div>
          );
        })}
      </>
    );
  };

  return (
    <>
      <div className="gx-page1-one">
        <img
          className="gx-p1-img"
          style={{
            fontWeight: 'bold',
            float: 'left',
            color: `#${appColor}`,
            position: 'absolute',
            height: '48px',
            width: '240px',
          }}
          src={appfullLogoColoured}
        />
        <Input
          className="gx-p1-input"
          placeholder={`Search Currencies In ${
            selectedTab?.nick_name ? selectedTab?.nick_name : 'Main Vault'
          }`}
          onChange={(e) => handleSearch_1(e)}
        />
      </div>
      <div className="gx-page1-two">
        <div>
          <Tabs
            defaultActiveKey="1"
            activeKey={selectedTabIndex}
            tabBarGutter={60}
            onChange={async (t1) => {
              setselectedTabIndex(t1);
              console.log(t1);
              if (t1 === '1') {
                setselectedTab({ nick_name: 'Main Vault' });
                setTimeout(() => {
                  getUserWalletBalanceData();
                }, 200);
              } else {
                setselectedTab(vaultSets[parseFloat(t1) - 2]);
                // console.log('seletedTAn', vaultSets[parseFloat(t1) - 2]);

                setTimeout(() => {
                  getUserWalletBalanceData2(
                    vaultSets[parseFloat(t1) - 2]?.vault_set,
                  );
                }, 500);
              }
            }}
          >
            <TabPane
              tab={
                <>
                  <img
                    src={vault_c}
                    style={{
                      marginLeft: '0px',
                      height: '12px',
                      width: '100% !important',
                    }}
                  />{' '}
                  <span>Main Vault</span>
                </>
              }
              key="1"
            >
              <div
                style={{
                  height: 'calc(70vh - 160px)',
                  overflowY: 'scroll',
                  width: '100% !important',
                }}
              >
                {balenceLoading ? <Skeleton /> : <>{showVaultUI(true)}</>}
              </div>
            </TabPane>

            {vaultSets?.map((ele, i) => {
              return (
                <TabPane
                  tab={
                    <>
                      <img
                        src={ele.vault_set_icon ? ele.vault_set_icon : ''}
                        style={{ height: '12px' }}
                      />{' '}
                      <span>{ele.nick_name}</span>
                    </>
                  }
                  key={i + 2}
                >
                  <div
                    style={{
                      height: 'calc(70vh - 160px)',
                      overflowY: 'scroll',
                      width: '100% !important',
                    }}
                  >
                    {balenceLoading ? <Skeleton /> : <>{showVaultUI(false)}</>}
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </div>
      </div>
    </>
  );
};
export default Gx_Page_1;
