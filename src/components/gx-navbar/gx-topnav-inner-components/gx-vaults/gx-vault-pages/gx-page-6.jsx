import React, { useContext, useEffect } from 'react';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
import { Modal as Modal2 } from 'antd';
import Gx_vault_apis from '../../../../../utils/services/gx-vaults.service';
import { message } from 'antd';
import Gx_commsService from '../../../../../utils/services/gx-comms.service';

const Gx_page_6 = ({
  clickeduserApps,
  setPage,
  vaultCoin,
  deposit_withdraw,
  deposit_withdraw_method,
  innerVaultAppsCoin,
  innerVaultCoin,
  innerdepositWithdrawType,
  enteredAmount,
  convertedValue,
  getUserWalletBalanceData,
  getInnerVaultCoinData,
  getTerminalDetailsFromGX,
  terminalProfile_ID,
}) => {
  const {
    appColor,
    findFiat,
    selectedTerminal,
    logout,
    setloginModal,
    setBalanceTrigger,
    balanceTrigger,
  } = useContext(OptionsContext);

  useEffect(() => {
    getTerminalDetailsFromGX();
  }, []);

  const handleFinalApi = async () => {
    setPage('loading');
    const headers = {
      token: localStorage.getItem('tokenAppToken'),
      email: localStorage.getItem('tokenAppLoginAccount'),
    };

    if (deposit_withdraw_method === 'Vault') {
      let body2 = {
        gxAsset: innerVaultCoin?.coinSymbol, // this will be debited from gx
        asset: vaultCoin?.coin_code, // this will be credited to teller vault
        // assetType: findFiat2(clickedsubvault.coinSymbol) ? 'fiat' : 'crypto',
        amount: enteredAmount * parseFloat(convertedValue), // amount in asset, in this example amount in 'USD'
        stats: false,
        terminalId: selectedTerminal?.terminal?.id,
      };
      if (deposit_withdraw === 'DEPOSIT') {
        try {
          await Gx_vault_apis.vault_depositFromSubApp(body2, headers);
          message.success('Success');
          await getFinalPrices();
          setBalanceTrigger(!balanceTrigger);
          setPage('7');
        } catch (error) {
          setPage('6');
          if (error?.message === 'Session Expired') {
            Modal2.error({
              title: 'Error',
              content: 'Token Expired,',
            });
            logout();
            setPage('0');
          } else {
            Modal2.error({
              title: 'Error',
              content: error?.message,
            });
            message.error(error?.message);
          }
        }
      } else {
        try {
          await Gx_vault_apis.vault_withdrawFromSubApp(body2, headers);
          await getFinalPrices();
          setBalanceTrigger(!balanceTrigger);

          setPage('7');
          message.success('Success');
        } catch (error) {
          setPage('6');

          if (
            error?.message === 'User is not registered for terminals' ||
            error?.message === 'Session Expired'
          ) {
            Modal2.error({
              title: 'Error',
              content: 'Token Expired,',
            });
            localStorage?.clear();
            setloginModal(true);
          } else {
            Modal2.error({
              title: 'Error',
              content: error?.message,
            });
          }
        }
      }
    } else {
      let body = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
        to_amount: parseFloat(enteredAmount),
        to: {
          app_code: clickeduserApps?.app_code,
          profile_id: clickeduserApps?.profile_id,
          coin:
            deposit_withdraw_method === 'Vault'
              ? innerVaultCoin?.coinSymbol
              : innerVaultAppsCoin?.coinSymbol,
        },
        from: {
          app_code: 'terminals',
          profile_id: terminalProfile_ID.profile_id,
          coin: vaultCoin?.coin_code,
        },
        transfer_for: 'gxterminal',
        terminalId: selectedTerminal?.terminal?.id,
      };
      //subapp deposit
      if (innerdepositWithdrawType === 'DEPOSIT') {
        try {
          let res = await Gx_commsService.WithdrawToSubApp(body, headers);
          await getFinalPrices();
          message.success(res?.txMessage);
          setBalanceTrigger(!balanceTrigger);

          setPage('7');
        } catch (error) {
          setPage('6');

          if (
            error?.message === 'User is not registered for terminals' ||
            error?.message === 'Session Expired'
          ) {
            Modal2.error({
              title: 'Error',
              content: 'Token Expired,',
            });
            localStorage?.clear();
            setloginModal(true);
          } else {
            Modal2.error({
              title: 'Error',
              content: error?.message,
            });
          }
        }
      } else {
        try {
          let body1 = {
            amount: parseFloat(enteredAmount * convertedValue),
            from: {
              app_code: clickeduserApps.app_code,
              profile_id: clickeduserApps.profile_id,
              coin: innerVaultAppsCoin?.coinSymbol,
            },
            to: {
              app_code: 'terminals',
              profile_id: terminalProfile_ID.profile_id,
              coin: vaultCoin?.coin_code,
            },
            transfer_for: 'gxterminal',
            terminalId: selectedTerminal?.terminal?.id,
          };
          let res = await Gx_vault_apis.depositFromSubApp(body1, headers);

          if (res.receipt) {
            setPage('7');
            await getFinalPrices();
            setBalanceTrigger(!balanceTrigger);
            message.success(res?.message);
          } else {
            setPage('6');
            Modal2.error({
              title: 'Error',
              content: res?.message,
            });
          }
        } catch (error) {
          setPage('6');

          if (
            error?.message === 'User is not registered for terminals' ||
            error?.message === 'Session Expired'
          ) {
            Modal2.error({
              title: 'Error',
              content: 'Token Expired,',
            });
            localStorage?.clear();
            setloginModal(true);
          } else {
            Modal2.error({
              title: 'Error',
              content: error?.message,
            });
          }
        }
      }
    }
  };

  const getFinalPrices = async () => {
    await getUserWalletBalanceData();
    await getInnerVaultCoinData();
  };

  return (
    <>
      {' '}
      <div style={{ marginTop: '64px', padding: '24px' }}>
        <div
          style={{
            height: 'calc(70vh - 160px)',
            overflowY: 'scroll',
            width: '100%',
          }}
          className="newVaultDesign"
        >
          <span style={{ color: `#${appColor}` }}>
            Your{' '}
            {deposit_withdraw_method === 'Vault'
              ? 'GX Vault '
              : clickeduserApps?.app_name}
            {deposit_withdraw_method === 'Vault'
              ? innerVaultCoin?.coinSymbol
              : innerVaultAppsCoin?.coinSymbol}{' '}
            Will Be {deposit_withdraw === 'DEPOSIT' ? 'Debited' : 'Credited'}
          </span>
          <br></br>
          <div
            className="d-flex"
            style={{
              marginTop: '8px',
              border: '1px solid #E7E7E7',
              padding: '24px',
            }}
          >
            <img
              src={
                deposit_withdraw_method === 'Vault'
                  ? innerVaultCoin.coinImage
                  : innerVaultAppsCoin.coinImage
              }
              style={{
                width: '40px',
                height: '40px',
                marginLeft: '2vw',
              }}
            />
            <span
              className="mt-2"
              style={{
                width: '120px',
                marginLeft: '1vw',
              }}
            >
              {deposit_withdraw_method == 'Vault'
                ? innerVaultCoin.coinName
                : innerVaultAppsCoin.coinName}
            </span>
            <span
              className="mt-2"
              style={{
                width: '80px',
                marginLeft: '32vw',
                fontWeight: 'bold',
                fontSize: '20px',
              }}
            >
              {(enteredAmount * 10000) / 10000}
              <small style={{ fontSize: '8px', marginLeft: '4px' }}>
                {deposit_withdraw_method == 'Vault'
                  ? innerVaultCoin?.coinSymbol
                  : innerVaultAppsCoin?.coinSymbol}
              </small>
            </span>
          </div>
          <div className="mt-2 newVaultDesign1"></div>
          <span style={{ color: `#${appColor}` }}>
            Your Master Vault {vaultCoin?.coin_code} Will Be{' '}
            {deposit_withdraw !== 'DEPOSIT' ? 'Debited' : 'Credited'}
          </span>
          <br></br>
          <div
            className="d-flex"
            style={{
              marginTop: '8px',
              border: '1px solid #E7E7E7',
              padding: '24px',
            }}
          >
            <img
              src={vaultCoin?.image}
              style={{
                width: '40px',
                height: '40px',
                marginLeft: '2vw',
              }}
            />
            <span
              className="mt-2"
              style={{
                width: '120px',
                marginLeft: '1vw',
              }}
            >
              {vaultCoin?.coin_name}
            </span>
            <span
              className="mt-2"
              style={{
                width: '80px',
                marginLeft: '32vw',
                fontWeight: 'bold',
                fontSize: '20px',
              }}
            >
              {findFiat(vaultCoin?.symbol)
                ? (enteredAmount * parseFloat(convertedValue)).toFixed(2)
                : (enteredAmount * parseFloat(convertedValue)).toFixed(4)}
              <small style={{ fontSize: '8px', marginLeft: '4px' }}>
                {vaultCoin?.coin_code}
              </small>
            </span>
          </div>

          <div
            style={{
              position: 'absolute',
              bottom: '24px',
              right: '24px',
            }}
          >
            <div className="d-flex newVaultDesign2 mt-2">
              <button
                style={{
                  backgroundColor: 'white',
                  color: `#${appColor}`,
                  border: `1px solid #${appColor}`,
                  height: '34px',

                  marginLeft: '6vw',
                }}
                onClick={() => setPage('5')}
              >
                Edit
              </button>
              <button
                style={{
                  backgroundColor: `#${appColor}`,
                  color: 'white',
                  border: `1px solid #${appColor}`,
                  height: '34px',
                  marginLeft: '2vw',
                }}
                onClick={handleFinalApi}
              >
                Confirm
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Gx_page_6;
