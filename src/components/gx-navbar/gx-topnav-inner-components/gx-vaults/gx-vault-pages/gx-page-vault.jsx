import React, { useContext, useState } from 'react';
import { Skeleton, Input } from 'antd';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
const Gx_page_vault = ({
  deposit_withdraw,
  vaultBalances_loading,
  innerVaultBalances_copy,
  setInnerVaultBalances_copy,
  setPage,
  innerVaultBalances,
  setInnerVaultCoin,
  setInnerdepositWithdrawType,
}) => {
  const { appColor, findFiat } = useContext(OptionsContext);
  const [searchValue, setSearchValue] = useState('');

  const handleSearch = (e) => {
    let n1 = e.target.value;
    setSearchValue(n1);

    if (n1.length == 0) {
      setInnerVaultBalances_copy(innerVaultBalances);
    } else {
      let i1 = innerVaultBalances.filter((ele) => {
        return (
          ele?.coin_name?.toLowerCase().includes(n1?.toLowerCase()) ||
          ele?.symbol?.toLowerCase().includes(n1?.toLowerCase())
        );
      });
      setInnerVaultBalances_copy([...i1]);
    }
  };

  return (
    <>
      {' '}
      <div style={{ marginTop: '96px', padding: '24px' }}>
        <h4
          style={{
            fontWeight: 'bold',
            float: 'left',
            color: `#${appColor}`,
          }}
        >
          Balances
        </h4>
        <Input
          placeholder="What Vault Are You Looking For?"
          style={{ float: 'right', width: '50%' }}
          value={searchValue}
          onChange={(e) => handleSearch(e)}
        />
        <br></br>
        <div
          style={{
            height: 'calc(70vh - 160px)',
            overflowY: 'scroll',
            width: '100%',
          }}
        >
          {vaultBalances_loading ? (
            <Skeleton />
          ) : (
            <>
              {innerVaultBalances_copy?.map((ele, i) => {
                return (
                  <div
                    key={i}
                    style={{
                      height: '72px',
                      border: '0.25px solid #E7E7E7',
                      marginTop: '10px',
                    }}
                  >
                    <div className="d-flex" style={{ marginTop: '18px' }}>
                      <img
                        src={ele.coinImage}
                        className="mt-1"
                        style={{
                          width: '30px',
                          height: '30px',
                          marginLeft: '2.5vw',
                        }}
                      />
                      <span
                        className="mt-2"
                        style={{
                          width: '240px',
                          marginLeft: '2vw',
                        }}
                      >
                        {ele.coinName}
                      </span>
                      <span
                        className="mt-2"
                        style={{
                          width: '160px',
                          marginLeft: '2vw',
                        }}
                      >
                        <img
                          src={ele?.coinImage}
                          className="mr-2"
                          style={{
                            width: '12px',
                            height: '12px',
                          }}
                        />
                        {findFiat(ele.coinSymbol)
                          ? Math.floor(ele.coinValue * 100) / 100
                          : Math.floor(ele.coinValue * 10000) / 10000}{' '}
                        <small>{ele.symbol}</small>
                      </span>

                      {deposit_withdraw === 'DEPOSIT' ? (
                        <button
                          style={{
                            backgroundColor: 'white',
                            color: `#${appColor}`,
                            border: `1px solid #${appColor}`,
                            height: '34px',
                            // marginLeft: '24px',
                            marginLeft: '3.25vw',
                          }}
                          onClick={() => {
                            setInnerdepositWithdrawType(ele.coinSymbol);
                            setInnerVaultCoin(ele);
                            setPage('5');
                          }}
                        >
                          Withdraw
                        </button>
                      ) : (
                        <button
                          style={{
                            backgroundColor: `#${appColor}`,
                            color: 'white',
                            border: `1px solid #${appColor}`,
                            height: '34px',
                            marginLeft: '4vw',
                          }}
                          onClick={() => {
                            setInnerVaultCoin(ele);
                            setInnerdepositWithdrawType(ele.coinSymbol);
                            setPage('5');
                          }}
                        >
                          Deposit
                        </button>
                      )}
                    </div>
                  </div>
                );
              })}
            </>
          )}
        </div>
      </div>
    </>
  );
};
export default Gx_page_vault;
