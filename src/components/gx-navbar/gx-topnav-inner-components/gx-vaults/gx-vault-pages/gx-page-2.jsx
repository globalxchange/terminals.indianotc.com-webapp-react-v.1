import React, { useContext } from 'react';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
import { vault_c, scan } from '../../../../../assets/image/terminal-images';
const Gx_page_2 = ({
  vaultCoin,
  setPage,
  setdeposit_withdraw_method,
  totalCyptoBalance,
  setenteredAmount,
}) => {
  const { findFiat, appColor } = useContext(OptionsContext);

  return (
    <>
      <div
        style={{
          height: 'calc(75vh - 40px)',
          overflowY: 'scroll',
          width: '100%',
        }}
      >
        <div
          style={{
            position: 'absolute',
            left: '50%',
            top: '55%',
            transform: 'translate(-50%,-50%)',
            color: `#${appColor}`,
          }}
        >
          <h2
            className="mt-4"
            style={{
              color: `#${appColor}`,
              marginBottom: '2px',
            }}
          >
            {findFiat(vaultCoin?.coin_code)
              ? parseFloat(vaultCoin?.balance)?.toFixed?.(2)
              : parseFloat(vaultCoin?.balance)?.toFixed?.(4)}{' '}
            {vaultCoin?.coin_code}
          </h2>

          <strong>Current GXTerminal Balance</strong>

          <br></br>
          <div
            className="d-flex"
            style={{
              border: `0.5px solid #${appColor}`,
              borderRadius: '4px',
              padding: '16px',
              marginTop: '32px',
            }}
            onClick={() => {
              setdeposit_withdraw_method('Vault');
              setPage('Vault');
              setenteredAmount('1');
            }}
          >
            <img src={vault_c} />
            <h6
              style={{
                marginLeft: '8px',
                color: `#${appColor}`,
                marginTop: '8px',
              }}
            >
              Vault
            </h6>
          </div>
          <br></br>

          <div
            className="d-flex"
            style={{
              border: `0.5px solid #${appColor}`,
              borderRadius: '4px',
              padding: '16px',
              marginTop: '32px',
            }}
            onClick={() => {
              setdeposit_withdraw_method('SUB APP');
              setenteredAmount('1');
              setPage('3');
            }}
          >
            <img src={vault_c} />
            <h6
              style={{
                marginLeft: '8px',
                color: `#${appColor}`,
                marginTop: '8px',
              }}
            >
              Vault Connect
            </h6>
          </div>

          <div
            className="d-flex"
            style={{
              border: `0.5px solid #${appColor}`,
              borderRadius: '4px',
              padding: '16px',
              marginTop: '32px',
              opacity: '0.5',
            }}
          >
            <img src={scan} />
            <h6
              style={{
                marginLeft: '8px',
                color: `#${appColor}`,
                marginTop: '8px',
              }}
            >
              via {vaultCoin?.coin_code} Address
            </h6>
          </div>
        </div>
      </div>

      <div
        style={{
          height: '48px',
          position: 'absolute',
          bottom: '-36px',
          backgroundColor: `#${appColor}`,
          width: '100%',
        }}
      >
        <h6
          style={{
            paddingLeft: '64px',
            float: 'left',
            color: 'white',
            paddingTop: '12px',
          }}
        >
          Total Balance
        </h6>

        <h6
          style={{
            paddingRight: '64px',
            float: 'right',
            color: 'white',
            paddingTop: '12px',
          }}
        >
          $ {Math.floor(totalCyptoBalance * 100) / 100} USD
        </h6>
      </div>
    </>
  );
};
export default Gx_page_2;
