import React, { useContext } from 'react';
import { Input, Modal as Modal2 } from 'antd';
import { OptionsContext } from '../../../../../context/Context';
import '../../../../../assets/scss/gx-vaults.scss';
const Gx_page_4 = ({
  setPage,
  clickeduserApps,
  setInnerVaultAppsCoin,
  setsubAppBalances_copy,
  subAppBalances,
  subAppBalances_copy,
  deposit_withdraw,
  setInnerdepositWithdrawType,
}) => {
  const { appColor, findFiat } = useContext(OptionsContext);

  const handleClick2 = async (n1, ele) => {
    console.log(ele);
    setInnerdepositWithdrawType(n1);
    setInnerVaultAppsCoin(ele);
    setPage('5');
  };

  return (
    <>
      <div style={{ marginTop: '96px', padding: '24px' }}>
        <h4
          style={{
            fontWeight: 'bold',
            float: 'left',
            color: `#${appColor}`,
          }}
        >
          {clickeduserApps?.app_name}
        </h4>

        <Input
          placeholder="What Vault Are You Looking For?"
          style={{ float: 'right', width: '50%' }}
          onChange={(e) => {
            let n1 = e.target.value;

            let i1 = subAppBalances?.filter((subaa) => {
              return (
                subaa.coinName.toLowerCase().includes(n1.toLowerCase()) ||
                subaa.coinSymbol.toLowerCase().includes(n1.toLowerCase())
              );
            });

            if (n1?.length == 0) {
              setsubAppBalances_copy(subAppBalances);
            } else {
              setsubAppBalances_copy(i1);
            }
          }}
        />
        <br></br>
        <div
          style={{
            height: 'calc(70vh - 160px)',
            overflowY: 'scroll',
            width: '100%',
          }}
        >
          {subAppBalances_copy?.map((ele, i) => {
            return (
              <div
                key={i}
                style={{
                  height: '72px',
                  border: '0.25px solid #E7E7E7',
                  marginTop: '10px',
                }}
              >
                <div className="d-flex" style={{ marginTop: '18px' }}>
                  <img
                    src={ele.coinImage}
                    style={{
                      width: '40px',
                      height: '40px',
                      marginLeft: '5vw',
                    }}
                  />
                  <span
                    className="mt-2"
                    style={{
                      width: '120px',
                      marginLeft: '7vw',
                    }}
                  >
                    {ele.coinName}
                  </span>
                  <span
                    className="mt-2"
                    style={{ width: '80px', marginLeft: '6vw' }}
                  >
                    {findFiat(ele)
                      ? Math.floor(ele.coinValue * 100) / 100
                      : Math.floor(ele.coinValue * 10000) / 10000}{' '}
                    <small>{ele.coinSymbol}</small>
                  </span>
                  {deposit_withdraw !== 'DEPOSIT' ? (
                    <button
                      style={{
                        backgroundColor: `#${appColor}`,
                        color: 'white',
                        border: `1px solid #${appColor}`,
                        height: '34px',
                        marginLeft: '6vw',
                      }}
                      onClick={() => handleClick2('DEPOSIT', ele)}
                    >
                      Deposit
                    </button>
                  ) : (
                    <button
                      style={{
                        backgroundColor: 'white',
                        color: `#${appColor}`,
                        border: `1px solid #${appColor}`,
                        height: '34px',
                        // marginLeft: '24px',
                        marginLeft: '2vw',
                      }}
                      onClick={() => handleClick2('WITHDRAW', ele)}
                    >
                      Withdraw
                    </button>
                  )}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};
export default Gx_page_4;
