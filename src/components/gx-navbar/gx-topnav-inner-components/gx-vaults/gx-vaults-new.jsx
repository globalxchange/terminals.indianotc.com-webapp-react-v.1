import React, { useState, useContext, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import { message, Modal as Modal2, notification } from 'antd';
import Lottie from 'react-lottie';
import Gx_page_ledger from './gx-vault-pages/gx-page-ledger';
import { OptionsContext } from '../../../../context/Context';
import * as animationData from '../../../../assets/animation/new_slow_chart.json';
import Gx_Login from '../../../gx-login/gx_login';
import '../../../../assets/scss/navbar.scss';
import { withRouter } from 'react-router-dom';
import Gx_vault_apis from '../../../../utils/services/gx-vaults.service';
import Gx_commsService from '../../../../utils/services/gx-comms.service';

import Gx_Page_1 from './gx-vault-pages/gx-page-1';
import Gx_page_2 from './gx-vault-pages/gx-page-2';
import Gx_page_3 from './gx-vault-pages/gx-page-3';
import Gx_page_4 from './gx-vault-pages/gx-page-4';
import Gx_page_5 from './gx-vault-pages/gx-page-5';
import Gx_page_vault from './gx-vault-pages/gx-page-vault';
import Gx_page_6 from './gx-vault-pages/gx-page-6';
import Gx_page_7 from './gx-vault-pages/gx-page-7';
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

const Gx_Vaults_new = ({ history }) => {
  const {
    terminalCoins,
    setloginModal,
    appName,
    vaultSets,
    setVaultSets,
    appColor,
    showVaults,
    setshowVaults,
    setReturnToVault,
  } = useContext(OptionsContext);

  //---------------------------------------------------------states-----------------------------------------------------//
  //Top bar page1 tab control
  const [selectedTabIndex, setselectedTabIndex] = useState('1');
  const [selectedTab, setselectedTab] = useState({ nick_name: 'Main Vault' });
  const [page, setPage] = useState('1');

  //Page 1 vaults
  const [mainVault, setMainVault] = useState([]);
  const [allBalances, setAllBalances] = useState([]);
  const [allBalancesCopy, setAllBalancesCopy] = useState([]);

  //ledger in page ledger
  const [totalCyptoBalance, setTotalCryptoBalance] = useState([]);

  const [terminalProfile_ID, setterminalProfile_ID] = useState('');
  const [clickedIcon, setclickedIcon] = useState('');

  //page1 selected coin on click
  const [vaultCoin, setVaultCoin] = useState('');

  //determines deposit or withdraw
  const [deposit_withdraw, setdeposite_withdraw] = useState('');

  //determines vault or sub app
  const [deposit_withdraw_method, setdeposit_withdraw_method] = useState('');

  //if deposit_withdraw_method === Vault
  const [innerVaultBalances, setInnerVaultBalances] = useState('');
  const [innerVaultBalances_copy, setInnerVaultBalances_copy] = useState('');
  const [innerVaultCoin, setInnerVaultCoin] = useState('');

  //if deposit_withdraw_method === sub app
  const [userApps, setUserApps] = useState([]);
  const [clickeduserApps, setclickeduserApps] = useState('');
  const [subAppBalances, setsubAppBalances] = useState([]);
  const [subAppBalances_copy, setsubAppBalances_copy] = useState([]);
  const [innerVaultAppsCoin, setInnerVaultAppsCoin] = useState('');

  const [final2, setfinal2] = useState('');

  const [innerdepositWithdrawType, setInnerdepositWithdrawType] = useState('');
  const [enteredAmount, setenteredAmount] = useState('1');

  const [convertedValue, setconvertedValue] = useState('');

  //loaders
  const [loading, setLoading] = useState(true);
  const [selectAppLoader, setselectAppLoader] = useState(true);
  const [balenceLoading, setBalanceLoading] = useState(true);
  const [ledgerLoading, setledgerLoading] = useState(true);
  const [vaultBalances_loading, setvaultBalances_loading] = useState(true);

  //---------------------------------------------------------states-----------------------------------------------------//

  //-------------------------------------------------------useeffects---------------------------------------------------//

  useEffect(async () => {
    if (showVaults) {
      if (localStorage?.getItem('tokenAppToken')) {
        await getUserVaultSets();
        if (selectedTabIndex === '1') {
          await getUserWalletBalanceData();
        }
        setLoading(false);
      } else {
        setLoading(false);
      }
    }
  }, [showVaults, selectedTab, localStorage?.getItem('tokenAppToken')]);

  useEffect(() => {
    setPage('1');
  }, [showVaults]);

  useEffect(async () => {
    if (deposit_withdraw_method === 'Vault' && page === 'Vault') {
      getInnerVaultCoinData();
    }
  }, [deposit_withdraw_method, page]);

  //-------------------------------------------------------useeffects---------------------------------------------------//

  //--------------------------------------------------------functions---------------------------------------------------//
  const handleClose = () => {
    setshowVaults(false);
    setPage('1');
  };

  const getInnerVaultCoinData = async () => {
    setvaultBalances_loading(true);
    let res = await Gx_commsService.getInnerVaultcoins();

    if (res.status) {
      setInnerVaultBalances(res.coins);
      setInnerVaultBalances_copy(res.coins);
      let n2 = res.coins.filter((ele) => {
        return ele.coinSymbol === vaultCoin.symbol;
      });
      if (n2.length > 0) {
        setfinal2(n2[0].coinValue);
      }
      setvaultBalances_loading(false);
    }
  };

  const getTerminalDetailsFromGX = async () => {
    try {
      let data = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        app_code: 'terminals',
      };
      let res = await Gx_commsService.getUserTerminalDetailsFromGx(data);
      setterminalProfile_ID(res);
    } catch (error) {
      if (
        error?.message === 'User is not registered for terminals' ||
        error?.message === 'Session Expired'
      ) {
        notification?.error(error?.message);
        localStorage?.clear();
        setloginModal(true);
      } else {
        console.log(error);
        notification.error({ message: error?.message });
      }
    }
  };

  const getUserVaultSets = async () => {
    try {
      let headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };
      setBalanceLoading(true);

      let res = await Gx_vault_apis.getVaultsetStats(headers);
      if (res?.length > 0) {
        setVaultSets(res);
        setMainVault({});
      }
      setBalanceLoading(false);
    } catch (e) {
      if (
        e?.message === 'User is not registered for terminals' ||
        e?.message === 'Session Expired'
      ) {
        message?.info(e?.message);
        localStorage?.clear();
        setloginModal(true);
        setshowVaults(false);
      } else {
        message?.info(e?.message);
      }
      setBalanceLoading(false);
    }
  };

  const getUserWalletBalanceData = async () => {
    try {
      // settotalCrypto('...');
      setBalanceLoading(true);

      let headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };

      if (selectedTabIndex === '1') {
        let res = await Gx_vault_apis.getWalletBalanaces(headers);

        let finalbalance = [];
        if (res?.wallet?.length === 0) {
          finalbalance = terminalCoins;
        } else {
          finalbalance = res.wallet.map((item) => {
            return {
              balance: item.balance,
              coin_code: item.ter_coin.coin_code,
              image: item.ter_coin.image,
              coin_name: item.ter_coin.coin_name,
              symbol: item.ter_coin.symbol,
            };
          });
          for (let i = 0; i < terminalCoins.length; i++) {
            let balanceExists = false;
            for (let j = 0; j < res.wallet.length; j++) {
              const item = res.wallet[j];
              if (terminalCoins[i].coin_code === item.ter_coin.coin_code) {
                balanceExists = true;
                break;
              }
            }
            if (!balanceExists) {
              finalbalance.push(terminalCoins[i]);
            }
          }
        }

        setAllBalances([...finalbalance]);
        setAllBalancesCopy([...finalbalance]);

        setTotalCryptoBalance(res?.totalUSD);
        setBalanceLoading(false);
      }
    } catch (e) {
      if (
        e?.message === 'User is not registered for terminals' ||
        e?.message === 'Session Expired'
      ) {
        message?.info(e?.message);
        localStorage?.clear();
        setshowVaults(false);
        setloginModal(true);
      } else {
        message?.info(e?.message);
      }
      setBalanceLoading(false);
    }
  };

  const getUserWalletBalanceData2 = async (v_set) => {
    try {
      let headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };
      setBalanceLoading(true);

      let res = await Gx_vault_apis.getVaultBalanaces(v_set, headers);

      let finalbalance = [];
      if (res?.wallet?.length === 0) {
        finalbalance = terminalCoins;
      } else {
        finalbalance = res.wallet.map((item) => {
          return {
            balance: item.balance,
            coin_code: item.ter_coin.coin_code,
            image: item.ter_coin.image,
            coin_name: item.ter_coin.coin_name,
            symbol: item.ter_coin.symbol,
          };
        });
        for (let i = 0; i < terminalCoins.length; i++) {
          let balanceExists = false;
          for (let j = 0; j < res.wallet.length; j++) {
            const item = res.wallet[j];
            if (terminalCoins[i].coin_code === item.ter_coin.coin_code) {
              balanceExists = true;
              break;
            }
          }
          if (!balanceExists) {
            finalbalance.push(terminalCoins[i]);
          }
        }
      }

      setAllBalances([...finalbalance]);
      setAllBalancesCopy([...finalbalance]);

      setTotalCryptoBalance(res?.totalUSD);
      setBalanceLoading(false);
    } catch (e) {
      if (
        e?.message === 'User is not registered for terminals' ||
        e?.message === 'Session Expired'
      ) {
        notification?.error(e?.message);
        localStorage?.clear();
        setloginModal(true);
      } else {
        message?.info(e?.message);
      }
      setBalanceLoading(false);
    }
  };

  const pushToLogin = () => {
    setReturnToVault('Vault');
    setloginModal(true);
    setshowVaults(false);
  };

  const renderPageHeader = () => {
    switch (page) {
      case '1':
        break;
      case '2':
        return (
          <h6
            style={{
              color: 'white',
              fontWeight: 'bold',
              marginTop: '12px',
              paddingLeft: '16px',
            }}
            onClick={() => {
              setPage('1');
            }}
          >
            How Do You Want {deposit_withdraw} {vaultCoin?.coin_code} ?
          </h6>
        );
      case '3':
        return (
          <>
            <h6
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
              onClick={() => {
                setPage('2');
              }}
            >
              Which App Do You Want To{' '}
              {deposit_withdraw === 'DEPOSIT' ? 'Withdraw' : 'Deposit'} From ?
            </h6>
          </>
        );
      case '4':
        return (
          <h6
            style={{
              color: 'white',
              fontWeight: 'bold',
              marginTop: '12px',
              paddingLeft: '16px',
            }}
            onClick={() => {
              setPage('3');
            }}
          >
            Which {clickeduserApps.app_name} Vault Do You Want To{' '}
            {deposit_withdraw == 'DEPOSIT' ? 'Withdraw' : 'Deposit'} From?
          </h6>
        );
      case '5':
        return (
          <>
            <h6
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
              onClick={() => {
                setPage('2');
              }}
            >
              Enter the {innerdepositWithdrawType} Amount
            </h6>
          </>
        );
      case '6':
        return (
          <>
            <h6
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
              onClick={() => {
                setPage('5');
              }}
            >
              Enter the {innerdepositWithdrawType} Amount
            </h6>
          </>
        );
      case '7':
        return (
          <>
            <h6
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
            >
              Congratulations
            </h6>
          </>
        );
      case 'ledger':
        return (
          <>
            <h4
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
              onClick={() => setPage('1')}
            >
              {appName}
            </h4>
          </>
        );
      case 'vault':
        return (
          <>
            <div onClick={() => setPage('1')} style={{ color: 'white' }}>
              {'<--'}
            </div>
            <h6
              style={{
                color: 'white',
                fontWeight: 'bold',
                marginTop: '12px',
                paddingLeft: '16px',
              }}
            >
              Vault{' '}
            </h6>
          </>
        );

      default:
        break;
    }
  };

  const renderPageBody = () => {
    switch (page) {
      case '1':
        return (
          <Gx_Page_1
            getUserWalletBalanceData={getUserWalletBalanceData}
            loading={loading}
            selectedTab={selectedTab}
            selectedTabIndex={selectedTabIndex}
            allBalances={allBalances}
            setAllBalancesCopy={setAllBalancesCopy}
            getUserWalletBalanceData2={getUserWalletBalanceData2}
            balenceLoading={balenceLoading}
            mainVault={mainVault}
            vaultSets={vaultSets}
            allBalancesCopy={allBalancesCopy}
            setledgerLoading={setledgerLoading}
            ledgerLoading={ledgerLoading}
            setPage={setPage}
            setclickedIcon={setclickedIcon}
            setVaultCoin={setVaultCoin}
            deposit_withdraw={deposit_withdraw}
            setdeposite_withdraw={setdeposite_withdraw}
            setselectedTab={setselectedTab}
            setselectedTabIndex={setselectedTabIndex}
          />
        );
      case '2':
        return (
          <Gx_page_2
            vaultCoin={vaultCoin}
            deposit_withdraw_method={deposit_withdraw_method}
            setdeposit_withdraw_method={setdeposit_withdraw_method}
            setPage={setPage}
            totalCyptoBalance={totalCyptoBalance}
            setenteredAmount={setenteredAmount}
          />
        );
      case '3':
        return (
          <Gx_page_3
            selectAppLoader={selectAppLoader}
            userApps={userApps}
            setPage={setPage}
            setclickeduserApps={setclickeduserApps}
            setsubAppBalances={setsubAppBalances}
            setsubAppBalances_copy={setsubAppBalances_copy}
            setselectAppLoader={setselectAppLoader}
            setUserApps={setUserApps}
            deposit_withdraw={deposit_withdraw}
            page={page}
          />
        );
      case '4':
        return (
          <Gx_page_4
            clickeduserApps={clickeduserApps}
            setsubAppBalances_copy={setsubAppBalances_copy}
            subAppBalances={subAppBalances}
            subAppBalances_copy={subAppBalances_copy}
            deposit_withdraw={deposit_withdraw}
            setInnerdepositWithdrawType={setInnerdepositWithdrawType}
            setInnerVaultAppsCoin={setInnerVaultAppsCoin}
            setPage={setPage}
          />
        );
      case '5':
        return (
          <Gx_page_5
            innerVaultAppsCoin={innerVaultAppsCoin}
            innerVaultCoin={innerVaultCoin}
            deposit_withdraw_method={deposit_withdraw_method}
            setPage={setPage}
            vaultCoin={vaultCoin}
            setconvertedValue={setconvertedValue}
            enteredAmount={enteredAmount}
            setenteredAmount={setenteredAmount}
            page={page}
          />
        );
      case '6':
        return (
          <Gx_page_6
            clickeduserApps={clickeduserApps}
            deposit_withdraw_method={deposit_withdraw_method}
            deposit_withdraw={deposit_withdraw}
            innerVaultCoin={innerVaultCoin}
            convertedValue={convertedValue}
            enteredAmount={enteredAmount}
            innerVaultAppsCoin={innerVaultAppsCoin}
            vaultCoin={vaultCoin}
            innerdepositWithdrawType={innerdepositWithdrawType}
            getUserWalletBalanceData={getUserWalletBalanceData}
            final2={final2}
            setfinal2={setfinal2}
            terminalProfile_ID={terminalProfile_ID}
            setsubAppBalances={setsubAppBalances}
            setsubAppBalances_copy={setsubAppBalances_copy}
            getTerminalDetailsFromGX={getTerminalDetailsFromGX}
            setPage={setPage}
            page={page}
            getInnerVaultCoinData={getInnerVaultCoinData}
          />
        );
      case '7':
        return (
          <Gx_page_7
            setPage={setPage}
            final2={final2}
            clickeduserApps={clickeduserApps}
            deposit_withdraw_method={deposit_withdraw_method}
            deposit_withdraw={deposit_withdraw}
            innerVaultCoin={innerVaultCoin}
            convertedValue={convertedValue}
            enteredAmount={enteredAmount}
            innerVaultAppsCoin={innerVaultAppsCoin}
            vaultCoin={vaultCoin}
            handleClose={handleClose}
            allBalances={allBalances}
            innerVaultBalances={innerVaultBalances}
            subAppBalances={subAppBalances}
          />
        );
      case 'ledger':
        return (
          <Gx_page_ledger
            ledgerLoading={ledgerLoading}
            clickedIcon={clickedIcon}
            setledgerLoading={setledgerLoading}
            selectedTab={selectedTab}
          />
        );
      case 'Vault':
        return (
          <Gx_page_vault
            deposit_withdraw={deposit_withdraw}
            vaultBalances_loading={vaultBalances_loading}
            innerVaultBalances={innerVaultBalances}
            setInnerVaultBalances={setInnerVaultBalances}
            innerVaultBalances_copy={innerVaultBalances_copy}
            setInnerVaultBalances_copy={setInnerVaultBalances_copy}
            setInnerdepositWithdrawType={setInnerdepositWithdrawType}
            setInnerVaultCoin={setInnerVaultCoin}
            setPage={setPage}
            page={page}
          />
        );
      case 'loading':
        return (
          <div style={{ minHeight: '400px' }}>
            <Lottie
              options={defaultOptions}
              height={250}
              width={150}
              style={{ marginTop: '100px' }}
            />
          </div>
        );
      default:
        break;
    }
  };

  //--------------------------------------------------------functions---------------------------------------------------//

  return (
    <>
      <div className="Vaults">
        {showVaults &&
        !localStorage?.getItem('tokenAppLoginAccount') &&
        !localStorage?.getItem('tokenAppToken') ? (
          pushToLogin()
        ) : (
          <Modal
            show={showVaults}
            onHide={handleClose}
            className="modal-container-vault"
            centered
          >
            {loading ? (
              <Lottie options={defaultOptions} height={250} width={180} />
            ) : (
              <>
                <div
                  style={{
                    height: '80px',
                    position: 'absolute',
                    backgroundColor: page === 1 ? 'white' : `#${appColor}`,
                    width: '100%',
                    cursor: 'pointer',
                  }}
                >
                  {page === 'Vault' ? (
                    <div
                      onClick={() => setPage('1')}
                      style={{ color: 'white' }}
                    >
                      {'<--'}
                    </div>
                  ) : (
                    ''
                  )}
                  <div
                    className="d-flex"
                    style={{
                      position: 'absolute',
                      left: '50%',
                      top: '50%',
                      transform: 'translate(-50%,-50%)',
                    }}
                  >
                    {renderPageHeader()}
                  </div>
                </div>

                {renderPageBody()}
              </>
            )}
          </Modal>
        )}
      </div>
    </>
  );
};
export default withRouter(Gx_Vaults_new);
