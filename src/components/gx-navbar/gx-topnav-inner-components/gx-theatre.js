import React, { useState, useContext, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { Modal as Modal2 } from 'antd';
import { OptionsContext } from '../../../context/Context';
import Gx_login from '../../gx-login/gx_login';

function Theatre() {
  const {
    email,
    appEmail,
    setloginModal,
    appColor,
    appName,
    appfullLogoColoured,
    showTheatre,
    setshowTheatre,
  } = useContext(OptionsContext);
  // const [show, setShow] = useState(false);

  const handleClose = async () => {
    // setShow(false);
    setshowTheatre(false);
  };
  // useEffect(() => {
  //   // setShow(false);
  // }, [email]);

  const handleConfirm = async () => {
    localStorage.setItem('tokenAppLoginAccount2', email);
    localStorage.setItem('tokenAppLoginAccount', appEmail);
    window.location.reload();
  };

  const handleLeave = async () => {
    let n1 = localStorage.getItem('tokenAppLoginAccount2');
    localStorage.setItem('tokenAppLoginAccount', n1);
    localStorage.removeItem('tokenAppLoginAccount2');
    window.location.reload();
  };

  useEffect(() => {
    if (
      !localStorage.getItem('tokenAppLoginAccount') &&
      !localStorage.getItem('tokenAppToken')
    ) {
      setloginModal(true);
      setshowTheatre(false);
    }
  }, [showTheatre]);

  return (
    <>
      <div className="">
        {showTheatre &&
        !localStorage.getItem('tokenAppLoginAccount') &&
        !localStorage.getItem('tokenAppToken') ? (
          <Gx_login vault={true} />
        ) : (
          <Modal
            show={showTheatre}
            onHide={handleClose}
            className="modal-container-ham"
            centered
          >
            <>
              <div style={{ padding: '8%' }}>
                <img
                  src={appfullLogoColoured}
                  style={{
                    height: '64px',
                    display: 'block',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginBottom: '52px',
                    marginTop: '28px',
                  }}
                />

                <div style={{ textAlign: 'center' }}>
                  <h6 style={{ fontWeight: '800', color: `#${appColor}` }}>
                    {localStorage.getItem('tokenAppLoginAccount2') ? (
                      <>
                        You Are About To Leave Theatre Mode And Go Back To Your
                        Own Account
                      </>
                    ) : (
                      <>
                        You Are About To Enter Theatre Mode For The Owner Of{' '}
                        {appName}
                      </>
                    )}
                  </h6>

                  <div className="d-flex" style={{ marginTop: '12%' }}>
                    {localStorage.getItem('tokenAppLoginAccount2') ? (
                      <Button
                        style={{
                          backgroundColor: 'white',
                          color: `#${appColor}`,
                          borderRadius: '0px',
                          margin: '0 auto',
                          display: 'block',
                          marginRight: '4px',
                          border: `0.5px solid #${appColor}`,
                        }}
                        onClick={() => handleLeave()}
                      >
                        Confirm
                      </Button>
                    ) : (
                      <Button
                        style={{
                          backgroundColor: 'white',
                          color: `#${appColor}`,
                          borderRadius: '0px',
                          margin: '0 auto',
                          display: 'block',
                          marginRight: '4px',
                          border: `0.5px solid #${appColor}`,
                        }}
                        onClick={() => handleConfirm()}
                      >
                        Confirm
                      </Button>
                    )}

                    <Button
                      style={{
                        backgroundColor: `#${appColor}`,
                        color: 'white',
                        borderRadius: '0px',
                        margin: '0 auto',
                        display: 'block',
                        marginLeft: '4px',
                        border: `0.5px solid #${appColor}`,
                      }}
                      onClick={handleClose}
                    >
                      Never Mind
                    </Button>
                  </div>
                </div>
              </div>
            </>
          </Modal>
        )}
      </div>

      {/* <Vaults showVaults={showVaults} setshowVaults={setshowVaults} /> */}
      {/* show login modal if required */}
      {/* {loginModal && !email && !token ? <Login /> : ''} */}
    </>
  );
}

export default Theatre;
