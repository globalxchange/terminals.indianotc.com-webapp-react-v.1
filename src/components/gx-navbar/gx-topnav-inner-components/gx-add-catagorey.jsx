import React, { useState, useContext, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import {
  Input,
  Row,
  Col,
  Select,
  Modal as Modal2,
  Tabs,
  DatePicker,
} from 'antd';
import Lottie from 'react-lottie';
import { OptionsContext } from '../../../context/Context';
import * as animationData from '../../../assets/animation/new_slow_chart.json';
import Login from '../../../components/gx-login/gx_login';
import axios from 'axios';
const { Option } = Select;
const { TabPane } = Tabs;
const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

function Gx_AddCategory(props) {
  const { email, token, loginModal, tradeCoin, findFiat } =
    useContext(OptionsContext);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const [category, setCategory] = useState('');
  const [itemTitle, setitemTitle] = useState('');
  const [itemIconURL, setitemIconURL] = useState('');
  const [itemDescription, setitemDescription] = useState('');
  const [link, setlink] = useState('');
  const [videoLinkURL, setvideoLinkURL] = useState('');
  const [videoThumbURL, setvideoThumbURL] = useState('');
  const [selectedTab, setselectedTab] = useState('Category');
  const [coin_list, setcoin_list] = useState([]);
  const [clickedCoin, setClickedCoin] = useState(
    findFiat(tradeCoin.pair.split('/')[0]),
  );
  const [imageUrl, setimageUrl] = useState('');
  const [asset_type, setasset_type] = useState('');
  const [coinName, setcoinName] = useState('');
  const [userEmail, setuserEmail] = useState('');
  const [userCognitoName, setuserCognitoName] = useState('');
  const [tradePrice, settradePrice] = useState('');
  const [tradeAmount, settradeAmount] = useState('');
  const [tradeCount, settradeCount] = useState('');
  const [timeType, settimeType] = useState('min');
  const [chartDate, setchartDate] = useState('');
  const [priceIn, setpriceIn] = useState('');
  const [priceOut, setpriceOut] = useState('');
  const [sellAmount, setsellAmount] = useState('');
  const [buyAmount, setbuyAmount] = useState('');

  const handleClose = () => {
    setShow(false);
    props.setshowaddCat(false);
  };

  useEffect(() => {
    setShow(loginModal);
  }, [loginModal]);

  useEffect(() => {
    setShow(true);

    axios
      .post('https://comms.globalxchange.com/coin/vault/service/coins/get', {
        app_code: 'Malls',
        profile_id: 'mallsu1548d7ff67e6bt1592127548247',
      })
      .then((res) => {
        if (res.data.status) {
          //console.log(res.data);
          setcoin_list(res.data.coins_data);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    setShow(props.showaddCat);
  }, [props.showaddCat]);

  const handleSave = () => {
    setLoading(true);
    const headers = {
      email: localStorage.getItem('tokenAppLoginAccount'),
      token: localStorage.getItem('tokenAppToken'),
    };

    let body = {
      category,
      itemTitle,
      itemIconURL, //optional
      itemDescription, //optional
      link, //optional
      videoLinkURL, //optional
      videoThumbURL, //optional
    };
    let imageApi = {
      basePair: `GXT${clickedCoin}`, // "basePair must be one of [GXTBTC, GXTETH, GXTUSDT, GXTUSD, GXTCAD, GXTGBP, GXTINR, GXTEUR]"
      imageURL: imageUrl,
      currency: coinName,
      type: asset_type,
    };

    let tradeApi = {
      userEmail: userEmail, // trade with account email
      userCognitoName: userCognitoName, // trade with account GXID
      price: tradePrice, // Price per unit
      amount: tradeAmount, // GXT amount
      basePair: `GXT${clickedCoin}`,
      count: tradeCount, // Number of orders to be placed
      // "timeStamp": "1597725326000"
    };

    if (selectedTab == 'Category') {
      axios
        .post(
          'https://teller2.apimachine.com/tradeSearch/add/category/item',
          { headers },
          body,
        )
        .then((res) => {
          setLoading(false);
          //console.log(res.data);
          if (res.data.status) {
            Modal2.success({
              title: 'Success',
              content: res.data.message,
            });
          } else {
            Modal2.error({
              title: 'Error',
              content: res.data.message,
            });
          }
        });
    } else if (selectedTab == 'Image URL') {
      axios
        .post(
          'https://teller2.apimachine.com/trade/admin/basePairs/info',
          imageApi,
          { headers },
        )
        .then((res) => {
          setLoading(false);
          if (res.data.status) {
            //console.log(res.data);
            Modal2.success({
              title: 'Success',
              content: res.data.message,
            });
          } else {
            Modal2.error({
              title: 'Error',
              content: res.data.message,
            });
          }
        });
    } else if (selectedTab == 'Trade') {
      axios
        .post(
          'https://teller2.apimachine.com/trade/admin/bi/multiOrder',
          tradeApi,
          { headers },
        )
        .then((res) => {
          setLoading(false);
          if (res.data.status) {
            //console.log(res.data);
            Modal2.success({
              title: 'Success',
              content: res.data.message,
            });
          } else {
            Modal2.error({
              title: 'Error',
              content: res.data.message,
            });
          }
        });
    } else if (selectedTab == 'Trade Chart') {
      let x1 = chartDate.slice(0, 4);
      let x2 = chartDate.slice(5, 7);
      let x3 = chartDate.slice(8, 10);

      let d1 = `${x3}-${x2}-${x1}${chartDate.slice(10)}`;

      let chartApi = {
        timeType: timeType,
        date: d1,
        priceIn,
        priceOut,
        sellAmount,
        buyAmount,
      };

      axios
        .post(
          `https://teller2.apimachine.com/stats/trade/chart/${tradeCoin}`,
          chartApi,
          { headers },
        )
        .then((res) => {
          setLoading(false);
          if (res.data.status) {
            //console.log(res.data);
            Modal2.success({
              title: 'Success',
              content: res.data.message,
            });
          } else {
            Modal2.error({
              title: 'Error',
              content: res.data.message,
            });
          }
        });
    }
  };

  return (
    <>
      <div className="">
        {show && !email && !token ? (
          <Login vault={false} />
        ) : (
          <>
            {show ? (
              <Modal
                show={show}
                onHide={handleClose}
                className="modal-container-vault"
                centered
              >
                {loading ? (
                  <Lottie options={defaultOptions} height={500} width={300} />
                ) : (
                  <Modal.Body>
                    <Tabs
                      defaultActiveKey="1"
                      onChange={(key) => setselectedTab(key)}
                    >
                      <TabPane tab="Category" key="Category">
                        <Row>
                          <Col span={6} className="mt-2">
                            Category
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={category}
                              onChange={(e) => setCategory(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Item Title
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={itemTitle}
                              onChange={(e) => setitemTitle(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Item Icon URL
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={itemIconURL}
                              onChange={(e) => setitemIconURL(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Item Description
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={itemDescription}
                              onChange={(e) =>
                                setitemDescription(e.target.value)
                              }
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Link
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={link}
                              onChange={(e) => setlink(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Video Link URL
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={videoLinkURL}
                              onChange={(e) => setvideoLinkURL(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Video Thumb URL
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={videoThumbURL}
                              onChange={(e) => setvideoThumbURL(e.target.value)}
                            />
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tab="Image URL" key="Image URL">
                        <Row>
                          <Col span={6} className="mt-2">
                            Base Pair Coin
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Select
                              showSearch
                              defaultValue={clickedCoin}
                              optionFilterProp="children"
                              onChange={(value, ele) => {
                                setClickedCoin(value);
                                setasset_type(ele.asset_type);
                                setcoinName(ele.coinName);
                              }}
                              style={{ width: '80%' }}
                            >
                              {coin_list.map((ele, i) => {
                                return (
                                  <Option
                                    key={i}
                                    value={ele.coinSymbol}
                                    asset_type={ele.asset_type}
                                    coinName={ele.coinName}
                                  >
                                    <div
                                      style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                      }}
                                    >
                                      <img
                                        src={ele.coinImage}
                                        alt=""
                                        width="20px"
                                      />{' '}
                                      <span
                                        style={{
                                          color: '#6569B0',
                                          marginLeft: '8px',
                                          fontSize: '12px',
                                        }}
                                      >
                                        &nbsp;{ele.coinName}
                                      </span>
                                    </div>
                                  </Option>
                                );
                              })}
                            </Select>
                          </Col>
                          <Col span={6} className="mt-2">
                            Image
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={imageUrl}
                              onChange={(e) => setimageUrl(e.target.value)}
                            />
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tab="Trade" key="Trade">
                        <h4>Create Multiple Trade And Fill</h4>
                        <Row>
                          <Col span={6} className="mt-2">
                            User Email
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={userEmail}
                              onChange={(e) => setuserEmail(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            User Cognito Name
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={userCognitoName}
                              onChange={(e) =>
                                setuserCognitoName(e.target.value)
                              }
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Price Per Amount
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={tradePrice}
                              onChange={(e) => settradePrice(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            GXT Amount
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={tradeAmount}
                              onChange={(e) => settradeAmount(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Base Pair Coin
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Select
                              showSearch
                              defaultValue={clickedCoin}
                              optionFilterProp="children"
                              onChange={(value, ele) => {
                                setClickedCoin(value);
                              }}
                              style={{ width: '80%' }}
                            >
                              {coin_list.map((ele, i) => {
                                return (
                                  <Option
                                    value={ele.coinSymbol}
                                    asset_type={ele.asset_type}
                                    coinName={ele.coinName}
                                    key={i}
                                  >
                                    <div
                                      style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                      }}
                                    >
                                      <img
                                        src={ele.coinImage}
                                        alt=""
                                        width="20px"
                                      />{' '}
                                      <span
                                        style={{
                                          color: '#6569B0',
                                          marginLeft: '8px',
                                          fontSize: '12px',
                                        }}
                                      >
                                        &nbsp;{ele.coinName}
                                      </span>
                                    </div>
                                  </Option>
                                );
                              })}
                            </Select>
                          </Col>
                          <Col span={6} className="mt-2">
                            Number of Orders To Be Placed
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={tradeCount}
                              onChange={(e) => settradeCount(e.target.value)}
                            />
                          </Col>
                        </Row>
                      </TabPane>
                      <TabPane tab="Trade Chart" key="Trade Chart">
                        <Row>
                          <Col span={6} className="mt-2">
                            Time Type
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Select
                              showSearch
                              defaultValue={timeType}
                              optionFilterProp="children"
                              onChange={(value, ele) => {
                                settimeType(value);
                              }}
                              style={{ width: '80%' }}
                            >
                              <Option value="min">Minute</Option>
                              <Option value="hour">Hour</Option>
                              <Option value="day">Day</Option>
                              <Option value="month">Month</Option>
                            </Select>
                          </Col>
                          <Col span={6} className="mt-2">
                            Date
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <DatePicker
                              renderExtraFooter={() => 'extra footer'}
                              showTime={
                                timeType == 'min' || timeType == 'hour'
                                  ? true
                                  : false
                              }
                              onChange={(value, dateString) => {
                                if (value) {
                                  setchartDate(dateString);
                                } else {
                                  setchartDate('');
                                }
                              }}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Price In
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={priceIn}
                              onChange={(e) => setpriceIn(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Price Out
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={priceOut}
                              onChange={(e) => setpriceOut(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Sell Amount
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={sellAmount}
                              onChange={(e) => setsellAmount(e.target.value)}
                            />
                          </Col>
                          <Col span={6} className="mt-2">
                            Buy Amount
                          </Col>
                          <Col
                            span={18}
                            className="mt-2"
                            style={{ width: '100%' }}
                          >
                            <Input
                              placeholder="Enter Here"
                              value={buyAmount}
                              onChange={(e) => setbuyAmount(e.target.value)}
                            />
                          </Col>
                        </Row>
                      </TabPane>
                    </Tabs>
                  </Modal.Body>
                )}

                <Modal.Footer>
                  <button
                    style={{
                      backgroundColor: '#3485C0',
                      color: 'white',
                      border: '1px solid #3485C0',
                    }}
                    onClick={handleSave}
                  >
                    Save
                  </button>
                </Modal.Footer>
              </Modal>
            ) : (
              ''
            )}
          </>
        )}
      </div>

      {/* second modal */}
    </>
  );
}

export default Gx_AddCategory;
