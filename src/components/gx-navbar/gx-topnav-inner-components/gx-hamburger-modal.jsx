import React, { useState, useContext, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import { OptionsContext } from '../../../context/Context';
import { motion } from 'framer-motion';

import Gx_Login from '../../../components/gx-login/gx_login';
import {
  new_refresh,
  new_login,
  new_mobile,
  new_admin,
  new_vaults,
  new_theater,
} from '../../../assets/image/terminal-images';
// import Gx_Vaults from './gx-vaults';
import Gx_AdminPage from './gx-adminPage';
import Theatre from './gx-theatre';
import { withRouter } from 'react-router-dom';
import { message } from 'antd';

function Gx_hamburger_modal({ showHamburger, history, setshowHamburger }) {
  const {
    email,
    setloginModal,
    loginModal,
    appfullLogoColoured,
    terminalID,
    setshowVaults,
    showAdminPage,
    setshowAdminPage,
    showTheatre,
    setshowTheatre,
  } = useContext(OptionsContext);
  const [show, setShow] = useState(false);
  const handleClose = () => {
    setShow(false);
    setshowHamburger(false);
  };

  useEffect(() => {
    setShow(showHamburger);
  }, [showHamburger]);

  return (
    <>
      <div className="Vaults">
        <Modal
          show={show}
          onHide={handleClose}
          className="modal-container-ham"
          centered
        >
          <>
            <div style={{ padding: '8%' }}>
              <img
                src={appfullLogoColoured}
                style={{
                  height: '64px',
                  display: 'block',
                  marginLeft: 'auto',
                  marginRight: 'auto',
                  marginBottom: '52px',
                  marginTop: '28px',
                }}
              />

              <div className="row">
                <div
                  className="col-md-4"
                  style={{ padding: '4%' }}
                  onClick={() => {
                    setshowVaults(true);
                    // setloginModal(true);
                    setShow(false);
                    setshowHamburger(false);
                  }}
                >
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                      }}
                    >
                      <img src={new_vaults} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>Vaults</span>
                    </div>
                  </motion.div>
                </div>
                <div
                  className="col-md-4"
                  style={{ padding: '4%' }}
                  onClick={() => window.location.reload()}
                >
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                      }}
                    >
                      <img src={new_refresh} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>Refresh</span>
                    </div>
                  </motion.div>
                </div>
                <div className="col-md-4" style={{ padding: '4%' }}>
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                        opacity: '0.4',
                      }}
                    >
                      <img src={new_mobile} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>Mobile</span>
                    </div>
                  </motion.div>
                </div>

                <div
                  className="col-md-4"
                  style={{ padding: '4%' }}
                  onClick={() => {
                    setshowTheatre(true);
                    handleClose();
                    // message?.info('Currently not available');
                  }}
                >
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                        // opacity: '0.4',
                      }}
                    >
                      <img src={new_theater} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>
                        {' '}
                        {localStorage.getItem('tokenAppLoginAccount2') ? (
                          <>Leave Theatre</>
                        ) : (
                          <>Theatre</>
                        )}
                      </span>
                    </div>
                  </motion.div>
                </div>

                <div
                  className="col-md-4"
                  style={{ padding: '4%' }}
                  onClick={() => {
                    if (email == 'shorupan@gmail.com') {
                      setshowAdminPage(true);
                      handleClose();
                    }
                  }}
                >
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                        opacity:
                          localStorage.getItem('tokenAppLoginAccount') ===
                          'shorupan@gmail.com'
                            ? '1'
                            : '0.4',
                      }}
                    >
                      <img src={new_admin} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>Admin</span>
                    </div>
                  </motion.div>
                </div>
                <div className="col-md-4" style={{ padding: '4%' }}>
                  <motion.div whileHover={{ scale: 1.2 }}>
                    <div
                      style={{
                        border: '0.5px solid #E7E7E7',
                        textAlign: 'center',
                        padding: '24px',
                      }}
                      onClick={() => {
                        if (
                          !localStorage.getItem('tokenAppLoginAccount') &&
                          !localStorage.getItem('tokenAppToken')
                        ) {
                          console.log('no');
                          setShow(false);
                          setshowHamburger(false);
                          setloginModal(true);
                        } else {
                          console.log('yes');

                          localStorage.clear();
                          setloginModal(false);
                          history?.push(`/${terminalID}`);
                        }
                      }}
                    >
                      <img src={new_login} className="mb-2" />
                      <br></br>
                      <span style={{ fontWeight: '600' }}>
                        {localStorage.getItem('tokenAppToken')
                          ? localStorage.getItem('tokenAppToken') !== ''
                            ? 'Logout'
                            : 'Login'
                          : 'Login'}
                      </span>
                    </div>
                  </motion.div>
                </div>
              </div>
            </div>
          </>
        </Modal>
      </div>

      {/* {<Gx_Vaults showVaults={showVaults} setshowVaults={setshowVaults} />} */}
      {/* show login modal if required */}
      {loginModal &&
      !localStorage.getItem('tokenAppLoginAccount') &&
      !localStorage.getItem('tokenAppToken') ? (
        <Gx_Login />
      ) : (
        ''
      )}

      {showAdminPage ? <Gx_AdminPage /> : ''}
      {showTheatre ? <Theatre /> : ''}
    </>
  );
}

export default withRouter(Gx_hamburger_modal);
