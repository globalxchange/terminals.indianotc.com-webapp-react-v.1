/* eslint-disable no-unreachable */
import React, { useContext, useState, useEffect } from 'react';
import { OptionsContext } from '../../context/Context';
import { NavLink, withRouter } from 'react-router-dom';
import { ReactComponent as TokenLogo } from '../../assets/image/gx-icon.svg';
import hamburger from '../../assets/image/hamburger.svg';
import { Modal } from 'react-bootstrap';
import {
  apple,
  andorid,
  email1,
  sms,
  logo,
} from '../../assets/image/terminal-images';
import { Tabs, Button, Input, Select, Modal as Modal2 } from 'antd';
import Gx_Vaults_new from './gx-topnav-inner-components/gx-vaults/gx-vaults-new';
import countries from '../../assets/js/phonenumbers';
import axios from 'axios';
import * as animationData from '../../assets/animation/new_slow_chart.json';
import Gx_hamburger_modal from './gx-topnav-inner-components/gx-hamburger-modal';

import Lottie from 'react-lottie';
import { motion } from 'framer-motion';
import '../../assets/scss/navbar.scss';

const { Option } = Select;

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

const Gx_TopNav = ({ isToken, setIsToken, setTab }) => {
  const { appColor, appImage, appfullLogoWhite, coinModal, setcoinModal } =
    useContext(OptionsContext);

  const [appsClicked, setappsClicked] = useState(false);
  const [showHamburger, setshowHamburger] = useState(false);
  const [appsPage, setAppsPage] = useState(1);
  const [appType, setAppType] = useState('');
  const [appsMessageType, setappsMessageType] = useState('');
  const [appsEmail, setappsEmail] = useState('');
  const [appsPhoneCode, setappsPhoneCode] = useState('');
  const [appsPhoneNumber, setappsPhoneNumber] = useState('');
  const [appsLoader, setappsLoader] = useState(false);

  const handleSend = () => {
    setappsLoader(true);
    let body = {
      userEmail: 'shorupan@nvestbank.com',
      app_code: 'lx',
      email: appsEmail,
      mobile: `${appsPhoneCode}${appsPhoneNumber}`,
      app_type: appType,
      //  "custom_message": "hey Yo! Cool Man!" // custom Message set by User
    };
    axios
      .post(
        'https://comms.globalxchange.com/coin/vault/service/send/app/links/invite',
        body,
      )
      .then((res) => {
        // console.log(res);
        setappsLoader(false);
        if (!res.data.status) {
          Modal2.error({
            title: 'Error',
            content: res.data.message,
          });
        } else {
          if (appsMessageType == 'email') {
            if (res.data.emailData.status) {
              setAppsPage(4);
            } else {
              Modal2.error({
                title: 'Error',
                content: res.data.emailData.message,
              });
            }
          } else if (appsMessageType == 'sms') {
            if (res.data.smsData.status) {
              setAppsPage(4);
            } else {
              Modal2.error({
                title: 'Error',
                content: res.data.smsData.message,
              });
            }
          }
        }
      });
  };

  useEffect(() => {
    // console.log(selectedTerminal);
    // console.log(appColor);
  });

  return (
    <nav
      className="row opt-top-nav px-3 mx-0"
      style={{ backgroundColor: `#${appColor}` }}
      onClick={() => {
        if (coinModal) {
          setcoinModal(false);
        }
      }}
    >
      <div className="col-lg-9 d-flex pl-0">
        <NavLink to="/" params={{ testvalue: 'hello' }}>
          <div
            className={`d-flex brand py-2${isToken ? ' inactive' : ''}`}
            onClick={() => {
              setIsToken(false);
              setTab('default');
            }}
          >
            {' '}
            <motion.div whileHover={{ scale: 1.1 }}>
              {appImage ? (
                <img
                  src={appfullLogoWhite ? appfullLogoWhite : ''}
                  style={{ height: '40px', margin: '0 10px' }}
                />
              ) : (
                <TokenLogo />
              )}
            </motion.div>
            <div className="nav-title"></div>
          </div>
        </NavLink>
      </div>

      <div
        style={{
          position: 'absolute',
          right: '0',
          backgroundColor: `#${appColor}`,
          height: '70px',
          width: '102px',
          top: '0',
        }}
        onClick={() => {
          setshowHamburger(true);
        }}
      >
        <div className={`d-flex brand p-0`}>
          <motion.div className="d-flex" whileHover={{ scale: 1.1 }}>
            <img src={hamburger} />
          </motion.div>
        </div>
      </div>

      {/* show login modal if required */}
      {/* {loginModal && !email && !token ? <Gx_login /> : ''} */}

      {/* show search modal  */}

      {/* {show ? (
        <div
          style={{
            height: '360px',
            width: 'calc(100vw - 218px)',
            position: 'absolute',
            top: '70px',
            zIndex: '2',
            backgroundColor: 'white',
            boxShadow: '4px 4px 4px #888',
          }}
        >
          <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="All" key="1"></TabPane>
            <TabPane tab="Trending" key="2">
              <div style={{ paddingLeft: '28px', paddingTop: '8px' }}>
                Trending
              </div>
            </TabPane>
            <TabPane tab="Token Economics" key="3">
              <div style={{ paddingLeft: '28px', paddingTop: '8px' }}>
                Token Economics
              </div>
            </TabPane>
            <TabPane tab="Analysis" key="4">
              <div style={{ paddingLeft: '28px', paddingTop: '8px' }}>
                Analysis
              </div>
            </TabPane>
            <TabPane tab="Consortium" key="5">
              <div style={{ paddingLeft: '28px', paddingTop: '8px' }}>
                Consortium
              </div>
            </TabPane>
            <TabPane tab="Knowledge Base" key="6">
              <div style={{ paddingLeft: '28px', paddingTop: '8px' }}>
                Knowledge Base
              </div>
            </TabPane>
          </Tabs>
        </div>
      ) : (
        ''
      )} */}

      {/* show apps clicked modal */}

      <Modal
        show={appsClicked}
        backdrop={true}
        centered
        onHide={() => {
          setappsClicked(false);
          setAppsPage(1);
        }}
        className="navApps"
      >
        {appsLoader ? (
          <Lottie options={defaultOptions} height={600} width={300} />
        ) : (
          <>
            <span
              style={{
                color: '#3485C0',
                padding: '24px',
                cursor: 'pointer',
                fontWeight: 'bold',
              }}
              onClick={() => {
                setappsClicked(false);
                setAppsPage(1);
              }}
            >
              {' <'} Go Back
            </span>
            <div
              className="text-center"
              style={{ padding: '72px', paddingTop: '16px' }}
            >
              <img src={logo} style={{ minHeight: '40%' }} />
              <br></br>

              {appsPage == 1 ? (
                <h2 className="gx-blue mt-4 font-weight-bold">GXToken</h2>
              ) : (
                ''
              )}

              <div>
                {appsPage == 1 ? (
                  <>
                    {' '}
                    <Button
                      className="app_btn"
                      onClick={() => {
                        setAppType('android');
                        setAppsPage(2);
                      }}
                    >
                      {' '}
                      <img src={andorid} className="h-100 mr-2" /> Android
                    </Button>
                    <Button
                      className="app_btn ml-4"
                      onClick={() => {
                        setAppType('ios');
                        setAppsPage(2);
                      }}
                    >
                      {' '}
                      <img src={apple} className="h-100 mr-2" />
                      IOS
                    </Button>
                  </>
                ) : (
                  ''
                )}

                {appsPage == 2 ? (
                  <>
                    <div className="mt-4">
                      <h4 className="gx-blue mt-4 font-weight-bold">
                        How Do You Want To Receive The Link?
                      </h4>

                      <Button
                        className="app_btn"
                        onClick={() => {
                          setappsMessageType('email');
                          setAppsPage(3);
                        }}
                      >
                        {' '}
                        <img src={email1} className="h-100 mr-2" /> Email
                      </Button>
                      <Button
                        className="app_btn ml-4"
                        onClick={() => {
                          setappsMessageType('sms');
                          setAppsPage(3);
                        }}
                      >
                        {' '}
                        <img src={sms} className="h-100 mr-2" />
                        SMS
                      </Button>
                    </div>
                  </>
                ) : (
                  ''
                )}

                {appsPage == 3 ? (
                  <>
                    {appsMessageType == 'email' ? (
                      <>
                        {' '}
                        <div className="mt-4">
                          <h4 className="gx-blue mt-4 font-weight-bold">
                            Whats Your Email?
                          </h4>
                          <Input
                            placeholder="Email"
                            onChange={(e) => setappsEmail(e.target.value)}
                            style={{ width: '75%' }}
                          />
                          <br></br>
                          <Button onClick={handleSend} className="mt-2">
                            {'->'}
                          </Button>
                        </div>
                      </>
                    ) : (
                      ''
                    )}
                    {appsMessageType == 'sms' ? (
                      <>
                        <div className="mt-4">
                          <h4 className="gx-blue mt-4 font-weight-bold">
                            Whats Your Phone Number?
                          </h4>
                          <div className="d-flex">
                            <Select
                              showSearch
                              style={{ width: 120 }}
                              placeholder="Select"
                              optionFilterProp="children"
                              onChange={(ele) => setappsPhoneCode(ele)}
                              // onFocus={onFocus}
                              // onBlur={onBlur}
                              // onSearch={onSearch}
                              // filterOption={(input, option) =>
                              //   option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                              // }
                            >
                              {countries.map((ele, i) => {
                                return (
                                  <Option value={ele.code} key={i}>
                                    {ele.code} {ele.name}
                                  </Option>
                                );
                              })}
                            </Select>
                            <Input
                              placeholder="Phone Number"
                              onChange={(e) =>
                                setappsPhoneNumber(e.target.value)
                              }
                            />
                          </div>

                          <Button onClick={handleSend} className="mt-2">
                            {'->'}
                          </Button>
                        </div>
                      </>
                    ) : (
                      ''
                    )}
                  </>
                ) : (
                  ''
                )}

                {appsPage == 4 ? (
                  <>
                    <div className="mt-4">
                      <h4 className="gx-blue mt-4 font-weight-bold">
                        The GXToken App Has Been Sent To Your{' '}
                        {appsMessageType == 'email' ? 'Email' : 'Phone'}
                      </h4>
                    </div>
                    <Button
                      onClick={() => setappsClicked(false)}
                      className="app_btn"
                    >
                      {' '}
                      Close
                      {/* <img src={email1} className="h-100 mr-2" /> Email */}
                    </Button>
                    <Button className="app_btn ml-4">
                      {' '}
                      {/* <img src={sms} className="h-100 mr-2" /> */}
                      Invite
                    </Button>
                  </>
                ) : (
                  ''
                )}
              </div>
            </div>
          </>
        )}
      </Modal>

      {/* show vaults modal */}
      <Gx_Vaults_new />

      <Gx_hamburger_modal
        showHamburger={showHamburger}
        setshowHamburger={setshowHamburger}
      />

      {/* {showaddCat ? (
        <Gx_AddCategory showaddCat={showaddCat} setshowaddCat={setshowaddCat} />
      ) : (
        ''
      )} */}
    </nav>
  );
};

export default withRouter(Gx_TopNav);
