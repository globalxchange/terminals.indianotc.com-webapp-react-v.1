import React from 'react';

import { new_terminal_logo } from '../../assets/image/terminal-images';

const Gx_Loading = () => {
  return (
    <div className="mainImg">
      <img
        className="animate__animated animate__heartBeat animate__infinite	infinite"
        style={{ height: '80px', padding: '4px' }}
        src={new_terminal_logo}
      />
    </div>
  );
};
export default Gx_Loading;
