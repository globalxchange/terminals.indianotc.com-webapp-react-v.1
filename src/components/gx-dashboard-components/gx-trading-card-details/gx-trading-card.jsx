/* eslint-disable react/jsx-key */
import React, { useState, useEffect, useContext } from 'react';
import { Table } from 'react-bootstrap';
import { OptionsContext } from '../../../context/Context';
import { Skeleton, message, Button } from 'antd';
import moment from 'moment';
import Gx_Login from '../../gx-login/gx_login';
import { motion } from 'framer-motion';
import { withRouter } from 'react-router-dom';
import '../../../assets/scss/trading-details.scss';
import Gx_commsService from '../../../utils/services/gx-comms.service';
import Axios from '../../../utils/axios';
import orderbookHelpers from '../../../utils/helpers/orderbook';
import { LoadingOutlined } from '@ant-design/icons';

import {
  gx,
  marketcharts,
  sd_exchange,
  sd_exchange_full_logo,
} from '../../../assets/image/terminal-images';
import { Modal } from 'react-bootstrap';
import Gx_order_apis from '../../../utils/services/gx-order.service';

const Gx_trading_card = () => {
  const {
    tradeCoin,
    email,
    token,
    randomValue,
    setbuysellorder,
    appColor,
    terminalID,
    coinModal,
    setcoinModal,
    setloginModal,
    selectedTab,
    setSelectedTab,
    setsocketOrderBook,
  } = useContext(OptionsContext);
  //--------------------------------------------------------------states-------------------------------------------------------//

  const [orderBuyData, setOrderBuyData] = useState([]);
  const [ordereSellData, setOrderSellData] = useState([]);
  const [mapData, setMapData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [errData, seterrData] = useState('There Are Currently No Order');
  const [oderHistoryData, setOderHistoryData] = useState([]);
  const [openOrders, setopenOrders] = useState([]);
  const [myTrades, setmyTrades] = useState([]);
  const [meta, setMeta] = useState({});
  const [convertedValue, setConvertedValue] = useState(0);
  const [feeDisplay, setFeeDisplay] = useState(false);
  const [displayData, setDisplayData] = useState({});
  const [load, setLoad] = useState(false);
  //--------------------------------------------------------------states-------------------------------------------------------//

  //--------------------------------------------------------------useefects-------------------------------------------------------//

  useEffect(() => {
    getConveted();
  }, [tradeCoin, randomValue, terminalID]);

  useEffect(() => {
    getInitialData();
  }, [selectedTab, email, randomValue, tradeCoin]);
  //--------------------------------------------------------------useeffects-------------------------------------------------------//

  //--------------------------------------------------------------functions-------------------------------------------------------//
  const getInitialData = async () => {
    console.log('getting data');
    if (terminalID && tradeCoin) {
      setLoading(true);
      let headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };

      if (selectedTab === 'Buy Orders' || selectedTab === 'Sell Orders') {
        getBuyandSellOrders(headers);
      }

      if (
        localStorage.getItem('tokenAppLoginAccount') &&
        localStorage.getItem('tokenAppToken')
      ) {
        if (selectedTab === 'My Trades') {
          // console.log('in heremy trades');
          getMyTrades(1, 100, false, true);
        } else if (selectedTab === 'Order History') {
          getOrderHistory(headers);
        } else if (selectedTab == 'My Open Orders') {
          getOpenOrders(headers);
        }
      }
    }
  };
  const hanldeCancel = async (ele) => {
    // console.log(ele);
    let headers = {
      email: localStorage.getItem('tokenAppLoginAccount'),
      token: localStorage.getItem('tokenAppToken'),
    };
    if (ele.status === 'forexOpen') {
      try {
        await Axios.post(
          `/order/close/forex/${ele.id}`,
          {},
          {
            headers,
          },
        );
        message.success('Success');
        getInitialData();
      } catch (error) {
        if (error?.response?.status === 401) {
          message.error('Token Expired,');
        } else if (error?.response?.data?.error) {
          message.error(error?.response?.data?.error);
        } else {
          console.log(error);
          message.error('something went wrong');
        }
      }
    } else {
      try {
        await Axios.post(
          `/order/cancel/${ele.id}`,
          {},
          {
            headers,
          },
        );

        message.success('Success');
        // console.log(res.data)
        getInitialData();
      } catch (error) {
        if (error?.response?.status === 401) {
          message.error('Token Expired,');
        } else if (error?.response?.data?.error) {
          message.error(error?.response?.data?.error);
        } else {
          console.log(error);
          message.error('something went wrong');
        }
      }
    }
  };
  const getBuyandSellOrders = async () => {
    try {
      let res = [];
      if (tradeCoin?.is_terminal_lite && !tradeCoin?.is_forex) {
        res = await Gx_order_apis.getBasicOrderBook(tradeCoin?.pair);
      } else if (tradeCoin?.is_forex || !tradeCoin?.is_terminal_lite) {
        res = await Gx_order_apis.getBuySellOrders(tradeCoin?.pair);
      }

      // console.log('buy and sell', res.data);
      // if (res.data.status) {
      // console.log(res);
      let formattedOrderbook = [];
      if (tradeCoin?.is_terminal_lite && !tradeCoin?.is_forex) {
        formattedOrderbook = orderbookHelpers.calculateSumAndTotalBid(
          res,
          true,
        );
      } else {
        formattedOrderbook = orderbookHelpers.calculateSumAndTotalBid(
          res,
          false,
        );
      }

      // console.log(formattedOrderbook);
      setOrderSellData(formattedOrderbook.sell);
      setOrderBuyData(formattedOrderbook.buy);
      setsocketOrderBook({
        buy: res.buy,
        sell: res.sell,
      });
      if (selectedTab == 'Buy Orders') {
        setMapData(orderBuyData);
      } else if (selectedTab == 'Sell Orders') {
        setMapData(ordereSellData);
      }

      if (formattedOrderbook.buy.length === 0) {
        seterrData(
          `There Are Currently No ${selectedTab} Order For ${tradeCoin.pair}`,
        );
      } else if (formattedOrderbook.sell.length === 0) {
        seterrData(
          `There Are Currently No ${selectedTab} Order For ${tradeCoin.pair}`,
        );
      }
      setLoading(false);
    } catch (error) {
      seterrData(
        `There Are Currently No ${selectedTab} Order For ${tradeCoin.pair}`,
      );
      setOrderSellData([]);
      setOrderBuyData([]);
      setMapData([]);
      setLoading(false);
      if (error?.message === 'Session Expired') {
        message.error('Token Expired, Please Login');
        localStorage?.clear();
        setloginModal(true);
      } else if (error?.message) {
        message.error(error?.message);
      }
    }
  };
  const getMyTrades = async (page, limit, load, clear) => {
    let headers = {
      email: localStorage.getItem('tokenAppLoginAccount'),
      token: localStorage.getItem('tokenAppToken'),
    };
    if (!load) {
      setLoading(true);
    } else {
      setLoad(true);
    }
    if (clear) {
      setmyTrades([]);
    }
    // My Trades history
    try {
      let res = await Gx_order_apis.getMyTradesHistory(headers, page, limit);
      if (!load) {
        setLoading(false);
      } else {
        setLoad(false);
      }
      if (clear) {
        setmyTrades(res.items);
      } else {
        setmyTrades([...myTrades, ...res?.items]);
      }
      setMeta(res?.meta);
      // console.log(res.data);
      if (res?.meta?.itemCount === 0) {
        seterrData(
          `There Are Currently No ${selectedTab} Order For ${tradeCoin?.pair}`,
        );
      }
    } catch (error) {
      seterrData(
        `There Are Currently No ${selectedTab} Order For ${tradeCoin?.pair}`,
      );
      setmyTrades([]);
      if (!load) {
        setLoading(false);
      } else {
        setLoad(false);
      }
      if (error?.message === 'Session Expired') {
        message.error('Token Expired, Please Login');
        localStorage?.clear();
        setloginModal(true);
      } else if (error?.message) {
        message.error(error?.message);
      }
    }
  };
  const getOpenOrders = async (headers) => {
    setLoading(true);
    try {
      let res = await Gx_order_apis.getOpenOrders(headers);
      setLoading(false);
      setopenOrders(res?.items);
      if (res?.meta.itemCount == 0) {
        seterrData(
          `There Are Currently No ${selectedTab} Order For ${tradeCoin?.pair}`,
        );
      }
    } catch (error) {
      seterrData(
        `There Are Currently No ${selectedTab} Order For ${tradeCoin?.pair}`,
      );
      setLoading(false);
      setopenOrders([]);
      if (error?.message === 'Session Expired') {
        message.error('Token Expired, Please Login');
        localStorage?.clear();
        setloginModal(true);
      } else if (error?.message) {
        message.error(error?.message);
      }
    }
  };
  const getOrderHistory = async (headers) => {
    try {
      setLoading(true);
      let res = await Gx_order_apis.getOrderHistory(headers, tradeCoin?.pair);

      setLoading(false);
      setOderHistoryData(res.items);
      if (res.length === 0) {
        seterrData(
          `There Are Currently No ${selectedTab} Order For ${tradeCoin?.pair}`,
        );
      }
    } catch (error) {
      seterrData(
        `There Are Currently No ${selectedTab} Order Fo r${tradeCoin?.pair}`,
      );
      setOderHistoryData([]);
      setLoading(false);
      if (error?.message === 'Session Expired') {
        message.error('Token Expired, Please Login');
        localStorage?.clear();
        setloginModal(true);
      } else if (error?.message) {
        message.error(error?.message);
      }
    }
  };
  const getConveted = async () => {
    setConvertedValue(0);
    if (tradeCoin) {
      let res = await Gx_commsService.getForex(tradeCoin.pair.split('/')[0]);

      if (res.status) {
        setConvertedValue(res.gx_price_inverse);
      }
    }
  };
  //--------------------------------------------------------------functions-------------------------------------------------------//

  return (
    <div
      className="card card-dark flex-fill trade-details"
      style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
      onClick={() => {
        if (coinModal) {
          setcoinModal(false);
        }
      }}
    >
      <div
        className="tabs-header"
        style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
      >
        {tabs.map((item) => (
          <div
            key={item}
            style={{
              backgroundColor: `#${appColor}`,
              color: 'white',
              width: '100%',
            }}
            className={`tab-item ${selectedTab === item && 'active'}`}
            onClick={async () => {
              console.log('hit');
              if (
                item === 'My Trades' ||
                item === 'My Open Orders' ||
                item === 'Order History'
              ) {
                if (localStorage?.getItem('tokenAppToken')) {
                  console.log(item);
                  setSelectedTab(item);
                } else {
                  setloginModal(true);
                  return;
                }
              } else {
                setSelectedTab(item);
              }
            }}
          >
            <motion.div whileHover={{ scale: 1.2 }}>{item}</motion.div>
          </div>
        ))}
      </div>
      {loading ? (
        <Skeleton />
      ) : (
        <Table
          responsive
          className="tradeCard"
          style={{
            color: `#${appColor}`,
            borderColor: `#${appColor}`,
          }}
        >
          {/* order history */}

          {selectedTab == 'Order History' ? (
            <>
              <thead className="t-head-trades">
                <tr style={{ color: `#${appColor}`, fontSize: '10px' }}>
                  <th>Amount</th>
                  <th>Price</th>
                  <th>Date (EST)</th>
                </tr>
              </thead>
              <tbody className="t-body-trades">
                {oderHistoryData.length === 0 ? (
                  <div
                    style={{
                      color: `#${appColor}`,
                      position: 'absolute',
                      top: '50%',
                      left: '50%',
                      transform: 'translate(-50%,-50%)',
                    }}
                  >
                    {errData}
                  </div>
                ) : (
                  <>
                    {oderHistoryData.map((ele, i) => {
                      return (
                        <tr key={i}>
                          <td>{ele.amount}</td>
                          <td>{ele.price}</td>
                          <td>
                            {moment(ele.created_at).format('MMM Do YYYY')} at{' '}
                            {moment(ele.created_at).format('LT')}
                          </td>
                        </tr>
                      );
                    })}
                  </>
                )}
              </tbody>
            </>
          ) : (
            ''
          )}

          {/* open orders based on email */}
          {selectedTab == 'My Open Orders' ? (
            <>
              {loading ? (
                <Skeleton />
              ) : (
                <>
                  {localStorage.getItem('tokenAppLoginAccount') &&
                  localStorage.getItem('tokenAppToken') ? (
                    <>
                      <thead className="t-head-trades">
                        <tr style={{ color: `#${appColor}`, fontSize: '10px' }}>
                          <th style={{ marginLeft: '40px', width: '11.11%' }}>
                            Pair
                          </th>
                          <th style={{ width: '11.11%' }}>Amount </th>
                          <th style={{ width: '11.11%' }}>Price </th>
                          <th style={{ width: '11.11%' }}>Type</th>
                          <th style={{ width: '11.11%' }}>Order Type</th>
                          <th style={{ width: '11.11%' }}>Status</th>
                          <th style={{ width: '11.11%' }}>Balance</th>
                          <th style={{ width: '11.11%' }}>Date (EST)</th>
                          <th style={{ width: '11.11%' }}>Cancel</th>
                        </tr>
                      </thead>
                      <tbody className="t-body-trades">
                        {openOrders.length == 0 ? (
                          <div
                            style={{
                              color: `#${appColor}`,
                              position: 'absolute',
                              top: '50%',
                              left: '50%',
                              transform: 'translate(-50%,-50%)',
                            }}
                          >
                            {errData}
                          </div>
                        ) : (
                          <>
                            {openOrders.map((ele, i) => {
                              return (
                                <tr
                                  key={i}
                                  style={{
                                    color:
                                      ele.type == 'buy'
                                        ? `#${appColor}`
                                        : 'red',
                                    width: '100%',
                                  }}
                                >
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.pair}
                                  </td>

                                  <td style={{ width: '11.11%' }}>
                                    {ele?.amount}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.price}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.type}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.order_type}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.status === 'forexOpen'
                                      ? 'Open'
                                      : ele.status}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele.remaining_amount
                                      ? ele?.remaining_amount
                                      : '-'}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {moment(ele.created_at).format(
                                      'MMM Do YYYY',
                                    )}{' '}
                                    at {moment(ele.created_at).format('LT')}
                                  </td>
                                  <td style={{ width: '11.11%' }}>
                                    {ele?.status === 'forexOpen' ? (
                                      <Button
                                        style={{
                                          borderColor: `red`,
                                          color: `red`,
                                        }}
                                        onClick={() => hanldeCancel(ele)}
                                      >
                                        Close
                                      </Button>
                                    ) : (
                                      <Button
                                        style={{
                                          borderColor: `#${appColor}`,
                                          color: `#${appColor}`,
                                        }}
                                        onClick={() => hanldeCancel(ele)}
                                      >
                                        Cancel
                                      </Button>
                                    )}
                                  </td>
                                </tr>
                              );
                            })}
                          </>
                        )}
                      </tbody>
                    </>
                  ) : (
                    <Gx_Login />
                  )}
                </>
              )}
            </>
          ) : (
            ''
          )}

          {selectedTab == 'Buy Orders' || selectedTab == 'Sell Orders' ? (
            <>
              <thead className="t-head-trades">
                <tr style={{ color: `#${appColor}`, fontSize: '10px' }}>
                  <th style={{ marginLeft: '40px' }}>Amount</th>
                  {selectedTab == 'Buy Orders' ? <th>Bid</th> : <th>Ask</th>}

                  {selectedTab == 'Buy Orders' ? (
                    <th>Total Bid</th>
                  ) : (
                    <th>Total Ask</th>
                  )}
                  <th>Base Sum</th>
                  <th>Pair Sum</th>
                </tr>
              </thead>
              <tbody className="t-body-trades">
                {selectedTab == 'Buy Orders' ? (
                  <>
                    {' '}
                    {orderBuyData?.length == 0 ? (
                      <div
                        style={{
                          color: `#${appColor}`,
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%,-50%)',
                        }}
                      >
                        {errData}
                      </div>
                    ) : (
                      <>
                        {orderBuyData?.map((ele, i) => {
                          return (
                            <tr
                              key={i + 'orderbuy'}
                              onClick={() => {
                                setbuysellorder(ele);
                              }}
                            >
                              <td width="20%">
                                {Number(ele.amount)?.toFixed(4)}
                              </td>
                              <td width="20%">
                                {Number(ele.price)?.toFixed(5)}
                              </td>
                              <td width="20%">
                                {ele.totalBid
                                  ? Number(ele.totalBid)?.toFixed(5)
                                  : Number(ele.totalAsk)?.toFixed(5)}
                              </td>
                              <td width="20%">{ele.baseSum}</td>
                              <td width="20%">
                                {Number(ele.pairSum)?.toFixed(4)}
                              </td>
                            </tr>
                          );
                        })}
                      </>
                    )}
                  </>
                ) : (
                  <>
                    {' '}
                    {ordereSellData?.length == 0 ? (
                      <div
                        style={{
                          color: `#${appColor}`,
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%,-50%)',
                        }}
                      >
                        {errData}
                      </div>
                    ) : (
                      <>
                        {ordereSellData?.map((ele, i) => {
                          return (
                            <tr
                              key={i + 'order'}
                              onClick={() => {
                                setbuysellorder(ele);
                              }}
                            >
                              <td width="20%">{ele.amount}</td>
                              <td width="20%">{ele.price}</td>
                              <td width="20%">
                                {ele.totalBid ? ele.totalBid : ele.totalAsk}
                              </td>
                              <td width="20%">{ele.baseSum}</td>
                              <td width="20%">{ele.pairSum}</td>
                            </tr>
                          );
                        })}
                      </>
                    )}
                  </>
                )}
              </tbody>
            </>
          ) : (
            ''
          )}

          {/* My TRades based on email */}
          {selectedTab == 'My Trades' ? (
            <>
              {localStorage.getItem('tokenAppLoginAccount') &&
              localStorage.getItem('tokenAppToken') ? (
                <>
                  <thead className="t-head-trades">
                    <tr style={{ color: `#${appColor}`, fontSize: '10px' }}>
                      <th width="20%">Pair </th>
                      <th width="20%">Date</th>
                      <th width="20%">Credited</th>
                      <th width="20%">Debited</th>
                      <th width="20%">Trade Audit</th>
                    </tr>
                  </thead>
                  <tbody className="t-body-trades">
                    {myTrades.length === 0 ? (
                      <div
                        style={{
                          position: 'absolute',
                          top: '50%',
                          left: '50%',
                          transform: 'translate(-50%,-50%)',
                        }}
                      >
                        {errData}
                      </div>
                    ) : (
                      <>
                        {myTrades?.map((ele, i) => {
                          return (
                            <tr>
                              <td
                                style={{
                                  display: 'flex',
                                  color:
                                    ele.type === 'buy' ? `#${appColor}` : 'red',
                                  alignItems: 'center',
                                }}
                              >
                                <img
                                  src={ele?.ter_coin_pair?.image}
                                  alt=""
                                  style={{ marginRight: '5px', height: '14px' }}
                                />{' '}
                                {ele?.ter_coin_pair?.pair}
                              </td>
                              <td
                                style={{
                                  paddingTop: '20px',
                                  color:
                                    ele.type === 'buy' ? `#${appColor}` : 'red',
                                }}
                              >
                                {moment(ele?.created_at).format('MMM Do YYYY')}{' '}
                                <br />{' '}
                                <span style={{ fontSize: '11.5px' }}>
                                  {moment(ele?.created_at).format('LT')} EST
                                </span>
                              </td>
                              <td
                                style={{
                                  color:
                                    ele.type === 'buy' ? `#${appColor}` : 'red',
                                }}
                              >
                                {ele?.is_forex
                                  ? ele?.gross_p_l
                                    ? parseFloat(
                                        parseFloat(ele?.gross_p_l) +
                                          parseFloat(ele?.meta_user_debits),
                                      )?.toFixed(5) + '  USD'
                                    : '-'
                                  : `${ele?.meta_user_recieves}
                                ${ele?.meta_user_recieves_asset}`}
                              </td>
                              <td
                                style={{
                                  color:
                                    ele.type === 'buy' ? `#${appColor}` : 'red',
                                }}
                              >
                                {`${ele?.meta_user_debits}
                                ${ele?.meta_user_debits_asset}`}
                              </td>
                              <td style={{ paddingTop: '25px' }}>
                                {' '}
                                <motion.div whileHover={{ scale: 1.1 }}>
                                  <div
                                    style={{
                                      width: 'fit-content',
                                      border: '1px solid #e7e7e7',
                                      padding: ' 5px 16px',
                                      borderRadius: '4px',
                                    }}
                                    onClick={(e) => {
                                      e.preventDefault();
                                      setFeeDisplay(true);
                                      setDisplayData(ele);
                                    }}
                                  >
                                    <img
                                      src={sd_exchange_full_logo}
                                      alt=""
                                      height="14px"
                                    />
                                  </div>{' '}
                                </motion.div>
                              </td>
                            </tr>
                          );
                        })}
                      </>
                    )}
                    {meta?.currentPage < meta?.totalPages && (
                      <tr width="100%">
                        <td style={{ padding: '10px' }}></td>
                        <td style={{ padding: '10px' }}></td>
                        <td
                          style={{ padding: '15px 10px', textAlign: 'center' }}
                        >
                          <button
                            style={{
                              padding: '4px 10px',
                              border: `1px solid #${appColor}`,
                              borderRadius: '4px',
                            }}
                            onClick={(e) => {
                              e.preventDefault();
                              getMyTrades(
                                meta.currentPage + 1,
                                100,
                                true,
                                false,
                              );
                            }}
                          >
                            <span style={{ marginRight: load ? '5px' : '' }}>
                              Load More
                            </span>{' '}
                            {load && <LoadingOutlined />}
                          </button>
                        </td>{' '}
                        <td style={{ padding: '10px' }}></td>
                        <td style={{ padding: '10px' }}></td>
                      </tr>
                    )}
                  </tbody>
                </>
              ) : (
                <Gx_Login />
              )}
            </>
          ) : (
            ''
          )}

          <Modal
            show={feeDisplay}
            onHide={() => {
              setFeeDisplay(false);
            }}
            className={'gx-modal-fee'}
            centered
          >
            <div className="top-logo">
              <img src={sd_exchange_full_logo} alt="" />
            </div>

            <div className="m-box">
              <div className="inner-box">
                <div>
                  <div className="m-bold">Trade Summary</div>
                </div>
                <div>
                  <p>{displayData?.type}</p>
                  <p>
                    <img src={displayData?.ter_coin_pair?.image} alt="" />{' '}
                    {displayData?.ter_coin_pair?.pair}
                  </p>
                </div>
                <div>
                  <p>Credited</p>
                  <p>
                    {displayData?.is_forex
                      ? displayData?.gross_p_l
                        ? parseFloat(displayData?.gross_p_l) +
                          parseFloat(displayData?.meta_user_debits) +
                          '  USD'
                        : '-'
                      : `${displayData?.meta_user_recieves}
                                ${displayData?.meta_user_recieves_asset}`}
                    {/* {`${displayData?.meta_user_recieves} ${displayData?.meta_user_recieves_asset}`} */}
                  </p>
                </div>
                <div>
                  <p>Debited</p>
                  <p>
                    {`${displayData?.meta_user_debits} ${displayData?.meta_user_debits_asset}`}
                  </p>
                </div>
                <div>
                  <p>Status</p>
                  <p>{displayData?.status}</p>
                </div>
                <div className="m-time">
                  <p>
                    <span style={{ justifyContent: 'flex-start' }}>Time</span>{' '}
                    <span style={{ justifyContent: 'flex-end' }}>
                      {moment(displayData?.created_at).format('MMM Do YYYY')} at{' '}
                      {moment(displayData?.created_at).format('LT')}
                    </span>
                  </p>

                  <span> EST</span>
                </div>
              </div>
              <div className="inner-box">
                <div>
                  <div className="m-bold">Bankers Fees</div>
                </div>
                <div>
                  <p>Banker</p>
                  <p>
                    <img src={displayData?.banker?.cover_pic} />{' '}
                    {displayData?.banker?.banker_tag}
                  </p>
                </div>
                <div>
                  <p>CMC Rate Per {displayData?.ter_coin_pair?.base}</p>
                  <p>
                    {`${displayData?.meta_raw_price} ${displayData?.ter_coin_pair?.quote}`}
                  </p>
                </div>
                <div>
                  <p>Banker’s Spread</p>
                  <p>{displayData?.meta_banker_cut} %</p>
                </div>
                {displayData?.meta_banker_price && (
                  <div>
                    <p>Banker’s Rate</p>
                    <p>
                      {`${
                        displayData?.meta_banker_price
                          ? displayData?.meta_banker_price
                          : '-'
                      } ${
                        displayData?.meta_banker_price
                          ? displayData?.ter_coin_pair?.quote
                          : ''
                      }`}
                    </p>
                  </div>
                )}
                <div>
                  <p>Trade Volume</p>
                  <p>
                    {`${displayData?.meta_user_debits} ${displayData?.meta_user_debits_asset}`}
                  </p>
                </div>
                <div>
                  <p>Bankers Fees</p>
                  <p>
                    {displayData?.meta_user_debits *
                      (displayData?.meta_banker_cut / 100)}
                  </p>
                </div>
              </div>
              <div className="inner-box">
                <div>
                  <div className="m-bold">Terminal Fees</div>
                </div>
                <div>
                  <p>Terminal</p>
                  <p>
                    <img src={displayData?.ter_terminal?.full_logo_coloured} />
                    {displayData?.ter_terminal?.name}
                  </p>
                </div>
                {displayData?.meta_banker_price && (
                  <div>
                    <p>Banker’s Rate</p>
                    <p>
                      {' '}
                      {`${displayData?.meta_banker_price} ${displayData?.ter_coin_pair?.quote}`}
                    </p>
                  </div>
                )}
                <div>
                  <p>Terminal’s Spread</p>
                  <p> {displayData?.meta_terminal_cut}%</p>
                </div>
                {displayData?.meta_cut_price && (
                  <div>
                    <p>Terminal’s Rate</p>
                    <p>
                      {`${displayData?.meta_cut_price} ${displayData?.ter_coin_pair?.quote}`}
                    </p>
                  </div>
                )}
                <div>
                  <p>Trade Volume</p>
                  <p>{displayData?.meta_user_debits}</p>
                </div>
                <div>
                  <p>Terminal Fees</p>
                  <p>
                    {displayData?.meta_user_debits *
                      (displayData?.meta_terminal_cut / 100)}
                  </p>
                </div>
              </div>
              <div className="inner-box">
                <div>
                  <div className="m-bold">Broker Fees</div>
                </div>
                <div>
                  <p>Broker</p>
                  <p>Coming Soon</p>
                </div>
                <div>
                  <p>Broker's Rate</p>
                  <p>Coming Soon</p>
                </div>
                <div>
                  <p>Broker's Spread</p>
                  <p>Coming Soon</p>
                </div>

                <div>
                  <p>Trade Volume</p>
                  <p>Coming Soon</p>
                </div>
                <div>
                  <p>Broker Fees</p>
                  <p>Coming Soon</p>
                </div>
              </div>
            </div>

            <div className="bottom-logo">
              <div
                onClick={() => {
                  window.open('https://marketcharts.co', '_blank');
                }}
              >
                {' '}
                <img src={marketcharts} alt="" />{' '}
              </div>{' '}
              <div
                onClick={() => {
                  window.open('https://exchangefees.com', '_blank');
                }}
              >
                <img src={sd_exchange_full_logo} alt="" />
              </div>{' '}
            </div>
          </Modal>
        </Table>
      )}

      <div></div>
    </div>
  );
};

export default withRouter(Gx_trading_card);

const tabs = [
  'Buy Orders',
  'Sell Orders',
  'Order History',
  'My Open Orders',
  'My Trades',
];
