import React, { useState, useContext, useEffect } from 'react';
import { OptionsContext } from '../../../context/Context';
// import Gx_Login from '../..//gx-login/gx_login';
import axios from 'axios';
import { message, notification, Skeleton } from 'antd';
import Gx_order_apis from '../../../utils/services/gx-order.service';
import { motion } from 'framer-motion';
import { LoadingOutlined } from '@ant-design/icons';

const MarketTabView = (props) => {
  const {
    tradeCoin,
    setRandomValue,
    buysellorder,
    appColor,
    terminalID,
    setloginModal,
    setSelectedTab,
  } = useContext(OptionsContext);

  const amountInputTypes = tradeCoin ? tradeCoin?.pair?.split('/') : [];

  const [showLogin, setShowLogin] = useState(false);
  const [amount, setAmount] = useState('1');
  const [showAnimation, setShowAnimation] = useState(false);

  const [selectedCoin, setSelectedCoin] = useState(
    tradeCoin ? tradeCoin?.pair?.split('/')[0] : '',
  );
  const [metaData, setMetaData] = useState('');
  const [balance1, setbalance1] = useState('');

  const [balance2, setbalance2] = useState('');
  const [metaLoading, setMetaLoading] = useState(false);
  useEffect(() => {
    if (buysellorder) {
      setAmount(buysellorder.amount);
    }
  }, [buysellorder]);

  // useEffect(() => {
  //   setAmount(1);
  // }, [selectedCoin]);

  useEffect(() => {
    if (tradeCoin) {
      setSelectedCoin(tradeCoin?.pair?.split('/')[0]);
    }
  }, [tradeCoin, localStorage.getItem('tokenAppLoginAccount')]);

  useEffect(() => {
    setbalance1(props.balance1);
    setbalance2(props.balance2);
  }, [props.balance1, props.balance2]);

  const handleOtherCoinPay = async (type) => {
    axios
      .get(
        `https://teller2.apimachine.com/trade/marketPriceForAmount?basePair=${tradeCoin?.pair}&amount=${amount}&type=${type}`,
      )
      .then(async (res) => {
        if (res.data.status) {
          await setAmount(res.data.data.price);
        }
      })
      .catch((e) => {
        notification['error']({
          message: 'Error',
          description: e?.message,
        });
      });
  };
  useEffect(() => {
    if (tradeCoin?.is_forex) {
      setAmount(0.1);
      getMetaDataForForex(0.1);
    } else {
      setAmount('1');
    }
  }, [selectedCoin]);

  const handleBuy = async () => {
    setShowLogin(true);
    if (selectedCoin !== tradeCoin?.pair?.split('/')[0]) {
      await handleOtherCoinPay('buy');
    }

    if (
      localStorage.getItem('tokenAppLoginAccount') &&
      localStorage.getItem('tokenAppToken')
    ) {
      const headers = {
        'Content-Type': 'application/json',
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };

      let body = {
        terminalId: terminalID,
        type: 'buy',
        orderType: 'market',
        amount: amount,
        basePair: tradeCoin?.pair,
        amountIn:
          selectedCoin === tradeCoin?.pair?.split('/')[0] ? 'base' : 'quote',
      };

      if (props.selectedVaultSet.nick_name !== 'Master Vault') {
        body.vaultSet = props.selectedVaultSet.vault_set;
      }
      // console.log(body, headers);

      setShowAnimation(true);
      try {
        await Gx_order_apis.placeMarketOrder(body, headers);
        notification['success']({
          message: 'Success',
          description: `Buy Order Placed for ${amount} ${selectedCoin}`,
        });
        if (tradeCoin?.is_forex) {
          setSelectedTab('My Trades');
        }

        setShowAnimation(false);
        setRandomValue(Math.random() * 1000);
        await props.getBalances();
      } catch (error) {
        setShowAnimation(false);
        if (
          error?.message === 'User is not registered for terminals' ||
          error?.message === 'Session Expired'
        ) {
          notification?.error(error?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          console.log(error);
          notification.error({ message: error?.message });
        }
      }
    }
  };

  const getMetaDataForForex = async (value) => {
    setMetaLoading(true);
    setMetaData({ pipValue: '0', margin: '0' });
    const headers = {
      'Content-Type': 'application/json',
      email: localStorage.getItem('tokenAppLoginAccount'),
      token: localStorage.getItem('tokenAppToken'),
    };

    let body = {
      terminalId: terminalID,
      type: 'buy',
      orderType: 'market',
      amount: value ? value : amount,
      basePair: tradeCoin?.pair,
      stats: true,
      amountIn:
        selectedCoin === tradeCoin?.pair?.split('/')[0] ? 'base' : 'quote',
    };

    try {
      let res = await Gx_order_apis.placeMarketOrder(body, headers);
      setMetaData(res);
      setMetaLoading(false);
    } catch (error) {
      setMetaData({ pipValue: '0', margin: '0' });
      setMetaLoading(false);

      if (
        error?.message === 'User is not registered for terminals' ||
        error?.message === 'Session Expired'
      ) {
        notification?.error('Please Login');
        localStorage?.clear();
        setloginModal(true);
      } else {
        console.log(error);
        // notification.error({ message: error?.message });
      }
    }
  };

  const handleSell = async () => {
    setShowLogin(true);
    if (selectedCoin !== tradeCoin?.pair?.split('/')[0]) {
      await handleOtherCoinPay('sell');
    }

    if (
      localStorage.getItem('tokenAppLoginAccount') &&
      localStorage.getItem('tokenAppToken')
    ) {
      const headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };

      let body = {
        terminalId: terminalID,
        type: 'sell',
        orderType: 'market',
        amount: amount,
        basePair: tradeCoin?.pair,
        amountIn:
          selectedCoin == tradeCoin?.pair?.split('/')[0] ? 'base' : 'quote',
      };
      console.log(body);
      if (props.selectedVaultSet.nick_name !== 'Master Vault') {
        body.vaultSet = props.selectedVaultSet.vault_set;
      }
      setShowAnimation(true);

      try {
        await Gx_order_apis.placeMarketOrder(body, headers);
        notification['success']({
          message: 'Success',
          description: `Sell Order Placed for ${amount} ${selectedCoin}`,
        });

        setShowAnimation(false);
        setRandomValue(Math.random() * 1000);
        await props.getBalances();
      } catch (error) {
        setShowAnimation(false);
        if (
          error?.message === 'User is not registered for terminals' ||
          error?.message === 'Session Expired'
        ) {
          notification?.error(error?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          console.log(error);
          notification.error({ message: error?.message });
        }
      }
    }
  };
  return (
    <>
      {showAnimation ? (
        <Skeleton />
      ) : (
        <div className="market-tab-wrapper">
          {tradeCoin?.is_forex && (
            <>
              <div
                className=""
                style={{ display: 'flex', justifyContent: 'space-between' }}
              >
                <div style={{ color: `#${appColor}`, fontWeight: '600' }}>
                  Pip Value:{' '}
                  {metaLoading ? (
                    <LoadingOutlined />
                  ) : (
                    parseFloat(metaData?.pipValue)?.toFixed(2)
                  )}{' '}
                  USD{' '}
                </div>
                <div style={{ color: `#${appColor}`, fontWeight: '600' }}>
                  Margin :{' '}
                  {metaLoading ? (
                    <LoadingOutlined />
                  ) : (
                    parseFloat(metaData?.margin)?.toFixed(2)
                  )}{' '}
                  USD{' '}
                </div>
              </div>
              <br />{' '}
            </>
          )}

          <div className="input-wrapper">
            <div className="label-wrapper">
              <div
                className="label"
                style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
              >
                {tradeCoin?.is_forex ? 'Volume' : 'Amount'}
              </div>
              <div className="controls"></div>
            </div>
            <div
              className="input-container"
              style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
            >
              <div className="input-type-switcher">
                {amountInputTypes.map((item) => (
                  <div
                    key={item}
                    className={`input-type-item`}
                    onClick={() => {
                      setSelectedCoin(item);

                      if (!tradeCoin?.is_terminal_lite || tradeCoin?.is_forex) {
                        // console.log('in lite ');
                        return;
                      }
                    }}
                    style={{
                      backgroundColor:
                        selectedCoin === item ? `#${appColor}` : 'white',
                      color: selectedCoin === item ? 'white' : `#${appColor}`,
                      borderColor: `#${appColor}`,
                    }}
                  >
                    <motion.div whileHover={{ scale: 1.2 }}>{item}</motion.div>
                  </div>
                ))}
              </div>
              <input
                className="input-filed"
                placeholder="0.00"
                value={amount}
                onChange={async (e) => {
                  if (tradeCoin?.is_forex) {
                    setAmount(e.target.value);
                    getMetaDataForForex(e.target.value);
                  } else {
                    setAmount(e.target.value);
                  }
                }}
                style={{ color: `#${appColor}` }}
              />
              <div className="unit" style={{ color: `#${appColor}` }}>
                {tradeCoin?.is_forex ? 'Lot ' : selectedCoin}
              </div>
            </div>
          </div>
          <div className="input-wrapper"></div>

          <div className="actions-container">
            <motion.div
              whileHover={{ scale: 1.2 }}
              className="action-button sell mr-3"
              onClick={() => {
                if (localStorage?.getItem('tokenAppToken')) {
                  handleSell();
                } else {
                  setloginModal(true);
                }
              }}
            >
              SELL <i className="fas fa-arrow-down" />
            </motion.div>
            <motion.div
              whileHover={{ scale: 1.2 }}
              className="action-button buy"
              onClick={() => {
                if (localStorage?.getItem('tokenAppToken')) {
                  handleBuy();
                } else {
                  setloginModal(true);
                }
              }}
            >
              BUY <i className="fas fa-arrow-up" />
            </motion.div>
          </div>
        </div>
      )}

      {/* show login modal if required */}
      {/* {showLogin && !email && !token ? <Gx_Login vault={false} /> : ''} */}
    </>
  );
};

export default MarketTabView;
