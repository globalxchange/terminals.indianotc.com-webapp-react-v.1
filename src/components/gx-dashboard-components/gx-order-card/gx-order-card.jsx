import React, { useState, useContext, useEffect } from 'react';
import Gx_LimitTabView from './gx-limit';
import Gx_MarketTabView from './gx-market-view';
import { OptionsContext } from '../../../context/Context';
import { motion } from 'framer-motion';
import { LoadingOutlined } from '@ant-design/icons';
import Gx_VaultSelect from './gx-vault-select';
import '../../../assets/scss/os.scss';
import { message, notification } from 'antd';
import Gx_vault_apis from '../../../utils/services/gx-vaults.service';
import { gxToken } from '../../../assets/image/terminal-images';
import Gx_order_apis from '../../../utils/services/gx-order.service';
const Gx_order_card = () => {
  const [tab, setTab] = useState('Market');
  const {
    selectedTerminal,
    tradeCoin,
    randomValue,
    findFiat,
    appColor,
    coinModal,
    setcoinModal,
    setloginModal,
    balanceTrigger,
  } = useContext(OptionsContext);

  const [balance1, setbalance1] = useState('');
  const [balance2, setbalance2] = useState('');
  const [selectedVaultSet, setselectedVaultSet] = useState({
    nick_name: 'Master Vault',
  });
  const [showVaultSelect, setshowVaultSelect] = useState(false);
  const [vaultSets, setVaultSets] = useState('');
  useEffect(() => {
    if (localStorage.getItem('tokenAppLoginAccount')) {
      getMainVaultsets();
    }
  }, [
    localStorage?.getItem('tokenAppLoginAccount'),
    randomValue,
    selectedTerminal,
  ]);

  useEffect(() => {
    if (
      localStorage.getItem('tokenAppToken') &&
      localStorage?.getItem('tokenAppLoginAccount')
    ) {
      getBalances();
    }
  }, [selectedVaultSet, tradeCoin, randomValue, balanceTrigger]);

  const getBalances = async () => {
    if (!showVaultSelect) {
      let coins2 = tradeCoin?.pair?.split('/');
      // console.log(selectedVaultSet);
      try {
        if (selectedVaultSet.nick_name === 'Master Vault') {
          let res = await Gx_order_apis.getBalancesWallet(tradeCoin?.pair, {
            email: localStorage?.getItem('tokenAppLoginAccount'),
          });
          // console.log(res);
          res.map((item) => {
            if (item.asset === coins2[0]) {
              setbalance1(item.balance);
            }
            if (item.asset === coins2[1]) {
              setbalance2(item.balance);
            }
          });
        } else {
          let res = await Gx_order_apis.getBalancesVault(
            tradeCoin?.pair,
            selectedVaultSet?.vault_set,
            {
              email: localStorage?.getItem('tokenAppLoginAccount'),
              token: localStorage?.getItem('tokenAppToken'),
            },
          );
          // console.log(res);
          res.map((item) => {
            if (item.asset === coins2[0]) {
              setbalance1(item.balance);
            }
            if (item.asset === coins2[1]) {
              setbalance2(item.balance);
            }
          });
        }
      } catch (e) {
        if (
          e?.message === 'User is not registered for terminals' ||
          e?.message === 'Session Expired'
        ) {
          message?.info(e?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          message?.info(e?.message);
        }
      }
    }
  };

  const getMainVaultsets = async () => {
    if (localStorage.getItem('tokenAppLoginAccount')) {
      setbalance1('...');
      setbalance2('...');
      try {
        let headers = {
          email: localStorage.getItem('tokenAppLoginAccount'),
          token: localStorage.getItem('tokenAppToken'),
        };
        let res = await Gx_vault_apis.getVaultsetStats(headers);

        if (res?.length > 0) {
          setVaultSets(res);
          if (selectedVaultSet) {
            // console.log(selectedVaultSet);
            if (selectedVaultSet.nick_name === 'Master Vault') {
              setselectedVaultSet({ nick_name: 'Master Vault' });
            } else {
              let x1 = res.find((ele) => {
                return ele.nick_name == selectedVaultSet.nick_name;
              });
              setselectedVaultSet(x1);
            }
          } else {
            setselectedVaultSet({ nick_name: 'Master Vault' });
          }
        }
      } catch (error) {
        if (
          error?.message === 'User is not registered for terminals' ||
          error?.message === 'Session Expired'
        ) {
          notification?.error(error?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          console.log(error);
          notification.error({ message: error?.message });
        }
      }
    }
  };

  return (
    <div
      className="card card-terminal-trades card-dark flex-grow-1 h-100 d-flex flex-column"
      style={{ borderColor: `#${appColor}` }}
      onClick={() => {
        if (coinModal) {
          setcoinModal(false);
        }
      }}
    >
      {/* balances new ui */}

      {localStorage.getItem('tokenAppLoginAccount') ? (
        <>
          <div
            style={{
              borderBottom: `1px solid #${appColor}`,
              borderRadius: '0px',
              height: '60px',
            }}
            className="d-flex"
          >
            <div
              onClick={() => setshowVaultSelect(true)}
              style={{
                borderRight: `1px solid #${appColor}`,
              }}
            >
              <motion.div whileHover={{ scale: 1.2 }}>
                {selectedVaultSet?.vault_set_icon ? (
                  <img
                    src={selectedVaultSet?.vault_set_icon}
                    style={{
                      width: '44px',
                      height: '100%',
                      padding: '8px',
                      paddingTop: '16px',
                    }}
                  />
                ) : (
                  <img
                    src={gxToken}
                    alt="..."
                    style={{
                      width: '44px',
                      height: '100%',
                      padding: '8px',
                      paddingTop: '16px',
                    }}
                  />
                )}
              </motion.div>
            </div>
            <div
              className="d-flex"
              style={{
                borderRight: `1px solid #${appColor}`,
                width: '45%',
                textAlign: 'center',
              }}
            >
              {balance1 === '...' ? (
                <LoadingOutlined style={{ display: 'block', margin: 'auto' }} />
              ) : (
                // "..."
                <div
                  className="d-flex"
                  style={{ display: 'block', margin: 'auto' }}
                >
                  <span style={{ fontSize: '1.2vw', fontWeight: 'bold' }}>
                    {' '}
                    {findFiat(tradeCoin?.pair?.split('/')[0])
                      ? balance1
                        ? parseFloat(balance1).toFixed(2)
                        : '0.00'
                      : balance1
                      ? parseFloat(balance1).toFixed(5)
                      : '0.0000'}{' '}
                    <small
                      style={{
                        fontSize: '12px',
                        // marginLeft: '4px',
                        fontWeight: 'bold',
                      }}
                    >
                      {tradeCoin?.pair?.split('/')[0]}
                    </small>
                  </span>
                </div>
              )}
            </div>
            <div
              className="d-flex"
              style={{ width: '45%', textAlign: 'center' }}
            >
              {balance2 == '...' ? (
                <LoadingOutlined style={{ display: 'block', margin: 'auto' }} />
              ) : (
                // "..."
                <div
                  className="d-flex"
                  style={{ display: 'block', margin: 'auto' }}
                >
                  {/* <img
                          src={icon2}
                          style={{
                            height: '16px',
                            width: '16px',
                            marginTop: '4px',
                            marginRight: '4px',
                          }}
                        /> */}
                  <span style={{ fontSize: '1.2vw', fontWeight: 'bold' }}>
                    {' '}
                    {findFiat(tradeCoin?.pair?.split('/')[1])
                      ? balance2
                        ? parseFloat(balance2).toFixed(2)
                        : '0.00'
                      : balance2
                      ? parseFloat(balance2).toFixed(5)
                      : '0.0000'}{' '}
                    <small
                      style={{
                        fontSize: '12px',
                        // marginLeft: '4px',
                        fontWeight: 'bold',
                      }}
                    >
                      {tradeCoin?.pair?.split('/')[1]}
                    </small>
                  </span>
                </div>
              )}
            </div>
          </div>
          <div className="custmgn7"></div>
          <br></br>
        </>
      ) : (
        ''
      )}

      <div className="opt-tab d-flex" style={{ borderColor: `#${appColor}` }}>
        <motion.div
          whileHover={{ scale: 1.4 }}
          className={`tab-itm w-50 p-3 ${tab === 'Limit' ? 'active' : ''}`}
          onClick={() => {
            if (!tradeCoin?.is_terminal_lite) setTab('Limit');
          }}
          style={{ color: `#${appColor}` }}
        >
          Limit
        </motion.div>
        <motion.div
          whileHover={{ scale: 1.4 }}
          className={`tab-itm w-50 p-3 ${tab === 'Market' ? 'active' : ''}`}
          onClick={() => setTab('Market')}
          style={{ color: `#${appColor}` }}
        >
          Market
        </motion.div>
      </div>
      {tab === 'Market' ? (
        <Gx_MarketTabView
          selectedVaultSet={selectedVaultSet}
          balance1={balance1}
          balance2={balance2}
          getBalances={getBalances}
        />
      ) : (
        <Gx_LimitTabView
          balance1={balance1}
          balance2={balance2}
          selectedVaultSet={selectedVaultSet}
          getBalances={getBalances}
        />
      )}

      <Gx_VaultSelect
        showVaultSelect={showVaultSelect}
        setshowVaultSelect={setshowVaultSelect}
        setselectedVaultSet={setselectedVaultSet}
        selectedVaultSet={selectedVaultSet}
        vaultSets={vaultSets}
      />
    </div>
  );
};

export default Gx_order_card;
