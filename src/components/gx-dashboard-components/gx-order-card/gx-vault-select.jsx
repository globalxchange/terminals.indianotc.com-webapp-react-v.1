import React, { useState, useEffect } from 'react';
import { Modal } from 'react-bootstrap';
// import { OptionsContext } from '../../context/Context';
// import Gx_Login from '../../gx-login/gx_login';
// import axios from 'axios';

import { motion } from 'framer-motion';
import { gxToken } from '../../../assets/image/terminal-images';

const Gx_VaultSelect = (props) => {
  //   const { email, token, loginModal } = useContext(OptionsContext);
  const [show, setShow] = useState(false);
  useEffect(() => {
    setShow(props.showVaultSelect);
  }, [props.showVaultSelect]);
  const handleClose = () => {
    setShow(false);
    props.setshowVaultSelect(false);
  };

  return (
    <>
      <div className="Vaults">
        <Modal
          show={show}
          onHide={handleClose}
          className="modal-container-ham"
          centered
        >
          <>
            <div
              style={{
                backgroundColor: '#3485C0',
                textAlign: 'center',
                height: '80px',
              }}
            >
              <h2
                style={{ color: 'white', fontWeight: '800', marginTop: '20px' }}
              >
                Select Trading VaultSet
              </h2>
            </div>

            <div style={{ padding: '8%' }}>
              <div className="row">
                <>
                  {props.vaultSets.length == 0 ? (
                    'No Vault Sets'
                  ) : (
                    <>
                      <div
                        className="col-md-4"
                        style={{
                          padding: '4%',
                          border:
                            props.selectedVaultSet.nick_name === 'Master Vault'
                              ? '2px solid grey !important'
                              : '',
                        }}
                        onClick={() => {
                          props.setselectedVaultSet({
                            nick_name: 'Master Vault',
                          });
                          setShow(false);
                          props.setshowVaultSelect(false);
                        }}
                      >
                        <motion.div whileHover={{ scale: 1.2 }}>
                          <div
                            style={{
                              border: '0.5px solid #E7E7E7 ',
                              textAlign: 'center',
                              padding: '24px',
                            }}
                          >
                            <img
                              src={gxToken}
                              style={{ height: '64px', width: '64px' }}
                              className="mb-2"
                            />
                            <br></br>
                            <span
                              style={{
                                fontWeight: '600',
                                whiteSpace: 'nowrap',
                              }}
                            >
                              Master Vault
                            </span>
                          </div>
                        </motion.div>
                      </div>
                      {props.vaultSets?.map((ele, i) => {
                        return (
                          <div
                            key={i}
                            className="col-md-4"
                            style={{
                              padding: '4%',
                            }}
                            onClick={() => {
                              props.setselectedVaultSet(ele);
                              setShow(false);
                              props.setshowVaultSelect(false);
                            }}
                          >
                            <motion.div whileHover={{ scale: 1.2 }}>
                              <div
                                style={{
                                  border: '0.5px solid #E7E7E7',
                                  textAlign: 'center',
                                  padding: '24px',
                                }}
                              >
                                <img
                                  src={ele.vault_set_icon}
                                  style={{ height: '64px', width: '64px' }}
                                  className="mb-2"
                                />
                                <br></br>
                                <span
                                  style={{
                                    fontWeight: '600',
                                  }}
                                >
                                  {ele.nick_name}
                                </span>
                              </div>
                            </motion.div>
                          </div>
                        );
                      })}
                    </>
                  )}
                </>
              </div>
            </div>
          </>
        </Modal>
      </div>
    </>
  );
};

export default Gx_VaultSelect;
