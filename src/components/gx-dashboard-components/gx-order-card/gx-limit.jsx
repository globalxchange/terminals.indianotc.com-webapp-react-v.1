import React, { useState, useContext, useEffect } from 'react';
import { OptionsContext } from '../../../context/Context';
import axios from 'axios';
import { message, notification, Skeleton } from 'antd';
import { motion } from 'framer-motion';
import Gx_order_apis from '../../../utils/services/gx-order.service';

const Gx_LimitTabView = (props) => {
  const {
    email,
    token,
    tradeCoin,
    setRandomValue,
    terminalID,
    appColor,
    setloginModal,
  } = useContext(OptionsContext);

  const amountInputTypes = tradeCoin?.pair?.split('/');
  const priceInputTypes = tradeCoin?.pair?.split('/');

  const [showLogin, setShowLogin] = useState(false);
  const [amount, setAmount] = useState('');
  const [price, setPrice] = useState('');
  const [showAnimation, setShowAnimation] = useState(false);

  const [selectedCoin, setSelectedCoin] = useState(
    tradeCoin?.pair?.split('/')[0],
  );
  const [selectedPriceType, setSelectedPriceType] = useState(
    priceInputTypes[0],
  );

  const [balance1, setbalance1] = useState('');
  const [balance2, setbalance2] = useState('');

  useEffect(() => {
    // getBalance();
    setAmount(0);
  }, [selectedCoin]);

  useEffect(() => {
    if (tradeCoin) {
      setSelectedCoin(tradeCoin?.pair?.split('/')[0]);
      setSelectedPriceType(tradeCoin?.pair?.split('/')[1]);
    }
  }, [tradeCoin, localStorage.getItem('tokenAppLoginAccount')]);

  useEffect(() => {
    setbalance1(props.balance1);
    setbalance2(props.balance2);
  }, [props.balance1, props.balance2]);

  const handleBuy = async () => {
    setShowLogin(true);
    // if (selectedCoin !== tradeCoin.slice(0, 3)) {
    //   await handleOtherCoinPay('buy');
    // }

    if (
      localStorage.getItem('tokenAppLoginAccount') &&
      localStorage.getItem('tokenAppToken')
    ) {
      const headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };

      let body = {
        terminalId: terminalID,
        type: 'buy',
        orderType: 'limit',
        amount: amount,
        basePair: tradeCoin?.pair,
        amountIn:
          selectedCoin == tradeCoin?.pair?.split('/')[0] ? 'base' : 'quote',

        price,
      };

      if (props.selectedVaultSet.nick_name !== 'Master Vault') {
        body.vaultSet = props.selectedVaultSet.vault_set;
      }
      // console.log('kjjfjfjf', body, props.selectedVaultSet);
      setShowAnimation(true);

      try {
        let res = await Gx_order_apis.placeMarketOrder(body, headers);

        notification['success']({
          message: 'Success',
          description: `Buy Order Placed for ${amount} ${selectedCoin}`,
        });
        // message.info(res.message ? res.message : res.statusText);
        setShowAnimation(false);
        setRandomValue(Math.random() * 1000);
        await props.getBalances();
      } catch (error) {
        setShowAnimation(false);
        if (
          error?.message === 'User is not registered for terminals' ||
          error?.message === 'Session Expired'
        ) {
          notification?.error(error?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          console.log(error);
          notification.error({ message: error?.message });
        }
      }
    }
  };

  const handleSell = async () => {
    setShowLogin(true);

    // if (selectedCoin !== tradeCoin.slice(0, 3)) {
    //   await handleOtherCoinPay('sell');
    // }

    if (
      localStorage.getItem('tokenAppLoginAccount') &&
      localStorage.getItem('tokenAppToken')
    ) {
      const headers = {
        email: localStorage.getItem('tokenAppLoginAccount'),
        token: localStorage.getItem('tokenAppToken'),
      };
      let body = {
        terminalId: terminalID,
        type: 'sell',
        orderType: 'limit',
        amount: amount,
        basePair: tradeCoin?.pair,
        amountIn: tradeCoin?.pair?.split('/')[0] ? 'base' : 'quote',

        price,
      };

      if (props.selectedVaultSet.nick_name !== 'Master Vault') {
        body.vaultSet = props.selectedVaultSet.vault_set;
      }
      setShowAnimation(true);
      // console.log('kjjfjfjf', body, props.selectedVaultSet);

      try {
        let res = await Gx_order_apis.placeMarketOrder(body, headers);
        notification['success']({
          message: 'Success',
          description: `Sell Order Placed for ${amount} ${selectedCoin}`,
        });

        // message.info(res.message ? res.message : res.statusText);
        setShowAnimation(false);
        setRandomValue(Math.random() * 1000);
        await props.getBalances();
      } catch (error) {
        setShowAnimation(false);
        if (
          error?.message === 'User is not registered for terminals' ||
          error?.message === 'Session Expired'
        ) {
          notification?.error(error?.message);
          localStorage?.clear();
          setloginModal(true);
        } else {
          console.log(error);
          notification.error({ message: error?.message });
        }
      }

      // console.log(res);

      // if (res.data.status) {
      //   message.success('Success');
      // } else {
      //
      // }
    }
  };

  return (
    <>
      {showAnimation ? (
        <Skeleton />
      ) : (
        <div
          className="market-tab-wrapper"
          style={{ paddingTop: '4px', paddingBottom: '4px' }}
        >
          <div className="input-wrapper">
            <div className="label-wrapper" style={{ color: `#${appColor}` }}>
              <div className="label" style={{ color: `#${appColor}` }}>
                Amount
              </div>
              <div className="controls"></div>
            </div>
            <div
              className="input-container"
              style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
            >
              <div className="input-type-switcher">
                {amountInputTypes.map((item) => (
                  <div
                    key={item}
                    className={`input-type-item`}
                    style={{
                      backgroundColor:
                        selectedCoin === item ? `#${appColor}` : 'white',
                      color: selectedCoin === item ? 'white' : `#${appColor}`,
                      borderColor: `#${appColor}`,
                    }}
                  >
                    <motion.div whileHover={{ scale: 1.2 }}>{item}</motion.div>
                  </div>
                ))}
              </div>
              <input
                className="input-filed"
                placeholder="0.00"
                value={amount}
                onChange={(e) => {
                  if (tradeCoin?.is_terminal_lite) {
                    return;
                  }
                  setAmount(e.target.value);
                }}
                style={{ color: `#${appColor}` }}
              />
              <div className="unit" style={{ color: `#${appColor}` }}>
                {selectedCoin}
              </div>
            </div>
          </div>
          <div className="input-wrapper">
            <div className="label-wrapper">
              <div className="label" style={{ color: `#${appColor}` }}>
                Price
              </div>
            </div>
            <div
              className="input-container"
              style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
            >
              <div className="input-type-switcher">
                {priceInputTypes.map((item) => (
                  <div
                    key={item}
                    // onClick={() => setSelectedPriceType(item)}
                    className={`input-type-item`}
                    style={{
                      backgroundColor:
                        selectedPriceType === item ? `#${appColor}` : 'white',
                      color:
                        selectedPriceType === item ? 'white' : `#${appColor}`,
                      borderColor: `#${appColor}`,
                    }}
                  >
                    <motion.div whileHover={{ scale: 1.2 }}>{item}</motion.div>
                  </div>
                ))}
              </div>
              <input
                className="input-filed"
                onChange={(e) => setPrice(e.target.value)}
                style={{ color: `#${appColor}` }}
              />
              <div className="unit" style={{ color: `#${appColor}` }}>
                {selectedPriceType}
              </div>
            </div>
          </div>

          <div className="actions-container">
            <motion.div
              whileHover={{ scale: 1.2 }}
              className="action-button sell mr-3"
              onClick={() => {
                if (localStorage?.getItem('tokenAppToken')) {
                  handleSell();
                } else {
                  setloginModal(true);
                }
              }}
            >
              SELL <i className="fas fa-arrow-down" />
            </motion.div>
            <motion.div
              whileHover={{ scale: 1.2 }}
              className="action-button buy"
              onClick={() => {
                if (localStorage?.getItem('tokenAppToken')) {
                  handleBuy();
                } else {
                  setloginModal(true);
                }
              }}
            >
              BUY <i className="fas fa-arrow-up" />
            </motion.div>
          </div>
        </div>
      )}

      {/* show login modal if required */}
      {/* {showLogin && !email && !token ? <Login vault={false} /> : ''} */}
    </>
  );
};

export default Gx_LimitTabView;
