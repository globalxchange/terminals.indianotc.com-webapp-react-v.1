import React, { useEffect, useRef, useContext } from 'react';
import { createChart } from 'lightweight-charts';
import { OptionsContext } from '../../../context/Context';

let areaSeries;
let lineSeries;
function Gx_Chart_new(props) {
  const { coin, appColor, chartType } = useContext(OptionsContext);
  const parent = useRef();
  const resizeObserver = useRef();
  const chartContainerRef = useRef();
  // const [chartDimension, setChartDimension] = useState({ width: 0, height: 0 });
  const chartTemp = useRef();
  // const [chartType,setChartType]=useState('')

  //-----------new-----------------//

  useEffect(() => {
    if (window.innerWidth < 600) {
      return;
    } else {
      chartTemp.current = createChart(chartContainerRef.current, {
        height: parent?.current?.offsetHeight,
        width: parent?.current?.offsetWidth,
        layout: {
          backgroundColor: 'white',
          textColor: `#${appColor}`,
        },
        grid: {
          vertLines: {
            visible: false,
            color: `#${appColor}`,
          },
          horzLines: {
            visible: false,
            color: `#${appColor}`,
          },
        },
        priceScale: {
          borderVisible: true,
          borderColor: `#${appColor}`,
          position: 'right',
          autoScale: true,
        },
        timeScale: {
          borderVisible: true,
          borderColor: `#${appColor}`,
          visible: true,
          timeVisible: true,
          secondsVisible: true,
        },
        crosshair: {
          vertLine: {
            visible: false,
            labelVisible: false,
          },
          horzLine: {
            visible: false,
            labelVisible: false,
          },
          mode: 1,
        },
      });

      if (chartType === 'area') {
        if (lineSeries) {
          chartTemp.current.removeSeries(lineSeries);
        }

        prepareAreaChart();
      } else if (chartType === 'line') {
        prepareLineChart();
      }
    }
  }, []);

  function prepareAreaChart() {
    console.log('preping area chart');
    // console.log(areaSeries?.series());
    if (lineSeries?._series) {
      // console.log('removing line series');
      chartTemp.current.removeSeries(lineSeries);
      lineSeries = undefined;
    }
    if (!areaSeries?._series) {
      // console.log('adding area series');
      areaSeries = chartTemp.current.addAreaSeries({
        topColor: `#${appColor}`,
        bottomColor: `#${appColor}`,
        lineColor: `#${appColor}`,
      });
      areaSeries.applyOptions({
        priceFormat: {
          type: 'price',
          precision: 6,
          minMove: 0.00001,
        },
      });
      // return;
    }
    try {
      if (props.chartData?.length > 0) {
        console.log('enter area');
        let chartDatas = [];
        props.chartData.map((ele) => {
          let data = getTimeSplit(ele.timestamp);
          if (props.timeInterval === '1s') {
            console.log('1s update');
            areaSeries.update({
              time: data,
              value: ele.value,
            });
          } else {
            chartDatas.push({
              time: data,
              value: ele.value,
            });
          }
        });
        if (props.timeInterval !== '1s') {
          areaSeries.setData(chartDatas);
        }
        chartTemp?.current?.timeScale?.()?.scrollToRealTime();
      }
    } catch (e) {
      console.log(e);
    }
  }
  function prepareLineChart() {
    console.log('preping line chart');
    console.log(lineSeries);
    if (areaSeries?._series) {
      console.log('removing area series');

      chartTemp.current.removeSeries(areaSeries);
      areaSeries = undefined;
    }
    if (!lineSeries?._series) {
      lineSeries = chartTemp.current.addLineSeries({
        color: `#${appColor}`,
      });
      lineSeries.applyOptions({
        priceFormat: {
          type: 'price',
          precision: 6,
          minMove: 0.00001,
        },
      });
    }
    let chartDatas = [];
    try {
      if (props.chartData?.length !== 0) {
        console.log('inside line');
        props.chartData.map((ele) => {
          let data = getTimeSplit(ele.timestamp);
          if (props.timeInterval === '1s') {
            lineSeries.update({
              time: data,
              value: ele.value,
            });
          } else {
            chartDatas.push({
              time: data,
              value: ele.value,
            });
          }
        });
        if (props.timeInterval !== '1s') {
          lineSeries.setData(chartDatas);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
  const getTimeSplit = (dateTime) => {
    let dateTimeSec = new Date(Number(dateTime));
    let date;
    switch (props.timeInterval) {
      case '1s': {
        date =
          Date.UTC(
            dateTimeSec.getFullYear(),
            dateTimeSec.getMonth(),
            dateTimeSec.getDate(),
            dateTimeSec.getHours(),
            dateTimeSec.getMinutes(),
            dateTimeSec.getSeconds(),
            0,
          ) / 1000;
        // console.log(dateTimeSec);
        break;
      }
      case 'min': {
        date =
          Date.UTC(
            dateTimeSec.getFullYear(),
            dateTimeSec.getMonth(),
            dateTimeSec.getDate(),
            dateTimeSec.getHours(),
            dateTimeSec.getMinutes(),
            // parseInt(dateTimeSec.getSeconds() / 30) * 30,
            0,
            0,
          ) / 1000;

        break;
      }
      case 'month': {
        date =
          Date.UTC(
            dateTimeSec.getFullYear(),
            dateTimeSec.getMonth(),
            // dateTimeSec.getDate(),
            // dateTimeSec.getHours(),
            // dateTimeSec.getMinutes(),
            0,
            0,
          ) / 1000;
        break;
      }
      case 'day': {
        date =
          Date.UTC(
            dateTimeSec.getFullYear(),
            dateTimeSec.getMonth(),
            dateTimeSec.getDate(),
            // dateTimeSec.getHours(),
            // parseInt(dateTimeSec.getMinutes() / 5) * 5,
            0,
            0,
            0,
            0,
          ) / 1000;
        break;
      }
      default: {
        date =
          Date.UTC(
            dateTimeSec.getFullYear(),
            dateTimeSec.getMonth(),
            dateTimeSec.getDate(),
            dateTimeSec.getHours(),
            // dateTimeSec.getMinutes(),
            // dateTimeSec.getSeconds(),
            0,
            0,
            0,
          ) / 1000;
      }
    }
    // console.log(date);
    return date;
  };

  const getCharts = () => {
    if (chartTemp?.current && chartType === 'area') {
      prepareAreaChart();
    } else if (chartTemp?.current && chartType === 'line') {
      prepareLineChart();
    }
  };

  useEffect(() => {
    getCharts();
  }, [coin, props.timeInterval, props.chartData, chartType]);

  const resizeListener = () => {
    chartTemp?.current?.applyOptions({
      width: parent?.current?.offsetWidth - 1,
      height: parent?.current?.offsetHeight - 1,
    });
    setTimeout(() => {
      chartTemp?.current?.timeScale?.()?.scrollToRealTime();
    }, 0);
  };

  useEffect(() => {
    window.addEventListener('resize', resizeListener);
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  }, []);

  useEffect(() => {
    resizeObserver.current = new ResizeObserver(() => {
      resizeListener();
    });

    resizeObserver.current.observe(chartContainerRef.current);

    return () => resizeObserver.current.disconnect();
  }, []);

  useEffect(() => {
    resizeListener();
  }, []);

  return (
    <div
      ref={parent}
      className="flex-grow-1 d-flex he0grow"
      style={{ marginRight: '-1px', marginBottom: '-1px', overflow: 'hidden' }}
    >
      <div ref={chartContainerRef} className="chart-container flex-grow-1" />
    </div>
  );
}

export default Gx_Chart_new;
