import React, { useState, useContext, useEffect } from 'react';
import { OptionsContext } from '../../../context/Context';
import { Select, Input, message } from 'antd';
import { LineChartOutlined, LoadingOutlined } from '@ant-design/icons';
import { motion } from 'framer-motion';
import { Table } from 'react-bootstrap';
import '../../../assets/scss/charts.scss';
import { withRouter } from 'react-router';
import GX_apis from '../../../utils/services/gx-terminal-apis.service';
import Gx_ApexChart from './gx-ApexChart';
import TradingViewWidget, { Themes } from 'react-tradingview-widget';

const { Option } = Select;

const Gx_chart_card = () => {
  const {
    tradeCoin,

    appColor,
    setTradeCoin,
    handleLeaveBasePair,
    terminalID,
    coinModal,
    setcoinModal,
    chartType,
    setChartType,
    terminalLiteList_pairs,
  } = useContext(OptionsContext);

  const [timeZ, setTimeZ] = useState('');
  const [dropValue, setdropValue] = useState('Asset Price');
  const [selectedInterval, setSelectedInterval] = useState(chartIntervals[2]);
  const [search, setSearch] = useState('');
  const [chartData, setchartData] = useState([]);
  const [volumeLoading, setvolumeLoading] = useState(false);
  const [timeInterval, settimeInterval] = useState('1Hr');
  const [subCoinList, setsubCoinList] = useState([]);
  const [subCoinLoading, setsubCoinLoading] = useState(false);
  const [subCoinList_copy, setsubCoinList_copy] = useState([]);

  async function onChange(value) {
    setdropValue(value);
    if (value == 'Volume') {
      settimeInterval('min');
      setSelectedInterval('1m');
      fetchData();
    }
  }

  useEffect(() => {
    setInterval(() => {
      const moment = require('moment-timezone');
      const timeInEST = moment().tz('America/New_York').format('HH:mm:ss A');
      setTimeZ(timeInEST);
    }, 1000);
  }, []);

  useEffect(() => {
    var root = document.querySelector(':root');
    var rootStyles = getComputedStyle(root);
    var deg = rootStyles.getPropertyValue('--deg');
    // console.log(deg);
    root.style.setProperty('--dColor', `#${appColor}`);
  }, [appColor]);

  useEffect(() => {
    // console.log('comnig');
    fetchData();
    getCoinList_lite();
  }, [tradeCoin, coinModal]);

  const getCoinList_lite = async () => {
    setvolumeLoading(true);
    setsubCoinLoading(true);
    // console.log(terminalLiteList_pairs);
    try {
      if (coinModal) {
        setsubCoinLoading(true);
        setsubCoinList_copy(terminalLiteList_pairs);
        let res = await GX_apis.getBaseFormattedPrices(
          tradeCoin?.pair?.split('/')[1],
          terminalID,
        );
        let res1 = await GX_apis.getBaseFormattedPricesInUSD(
          tradeCoin?.pair?.split('/')[1],
          terminalID,
        );

        let x1;
        let a1 = [];

        for (x1 in res) {
          let o1 = {
            basePair: x1,
            price: res[x1],
            value: res1[x1],
          };

          a1.push(o1);
        }
        setsubCoinList(a1);

        if (search !== '') {
          let n2 = a1?.filter((x1) => {
            return x1?.basePair?.includes(search?.toUpperCase());
          });
          console.log(n2);
          setsubCoinList_copy(n2);
        } else {
          setsubCoinList_copy(a1);
        }

        setsubCoinLoading(false);
      }
    } catch (e) {
      message.error(e?.message);
    }
  };

  const fetchData = async () => {
    let n1 = 'min';
    if (selectedInterval == '1m') {
      n1 = 'min';
      settimeInterval('min');
    } else if (selectedInterval == '1Hr') {
      n1 = 'hour';
      settimeInterval('hour');
    } else if (selectedInterval == '1D') {
      n1 = 'day';
      settimeInterval('day');
    } else if (selectedInterval == '1M') {
      n1 = 'month';
      settimeInterval('month');
    } else if (selectedInterval == '1s') {
      settimeInterval('1s');
      n1 = '1s';
    }

    if (dropValue == 'Volume') {
      try {
        let res = await GX_apis.getStatsChartsAndVolume(tradeCoin, n1);
        setvolumeLoading(false);
        if (res.data.status) {
          setchartData(res.data.data.prices);
        }
      } catch (E) {
        console.log(E?.message);
      }
    }
  };

  return (
    <>
      {coinModal ? (
        <>
          <div
            className="coinModalCls"
            style={{ border: `1px solid #${appColor}` }}
          >
            <div onClick={() => setcoinModal(true)}>
              <Input
                placeholder={`Search ${
                  tradeCoin?.pair?.split('/')[1]
                } Pairs ..|`}
                style={{
                  borderBottom: `1px solid #${appColor}`,
                  borderRadius: '0px',
                  height: '40px',
                }}
                value={search}
                onChange={(e) => {
                  let n1 = e.target.value;
                  setSearch(n1);
                  if (n1.length > 0) {
                    let n2 = subCoinList_copy?.filter((x1) => {
                      if (subCoinLoading) {
                        return x1?.pair?.includes(n1?.toUpperCase());
                      } else {
                        return x1?.basePair?.includes(n1?.toUpperCase());
                      }
                    });
                    setsubCoinList_copy(n2);
                  } else {
                    setsubCoinList_copy(
                      subCoinLoading ? terminalLiteList_pairs : subCoinList,
                    );
                  }
                }}
              />
            </div>

            <Table
              responsive
              className="tradeCard"
              style={{ color: `#${appColor}`, borderColor: `#${appColor}` }}
            >
              <thead>
                <tr>
                  <th>Pair</th>
                  <th>Price</th>
                  <th>Value (USD)</th>
                </tr>
              </thead>
              <tbody>
                <>
                  {subCoinList_copy.map((ele, i) => {
                    return (
                      <tr
                        key={i}
                        onClick={async () => {
                          if (tradeCoin) {
                            await handleLeaveBasePair();
                          }
                          let elem = terminalLiteList_pairs.find(
                            (e) => e.pair === ele.basePair,
                          );
                          if (elem && !subCoinLoading) {
                            // console.log('setting in coinsider');
                            setTradeCoin(elem);
                          }
                          {
                            !subCoinLoading && setcoinModal(false);
                          }
                        }}
                      >
                        <td>
                          <small>
                            {!subCoinLoading ? ele.basePair : ele.pair}
                          </small>
                        </td>
                        <td>
                          {subCoinLoading ? (
                            <LoadingOutlined />
                          ) : (
                            <small>
                              {ele.price ? ele.price.toFixed(4) : ''}
                            </small>
                          )}
                        </td>
                        <td>
                          {subCoinLoading ? (
                            <LoadingOutlined />
                          ) : (
                            <small>
                              $ {ele.value ? ele.value.toFixed(2) : ''}
                            </small>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </>
              </tbody>
            </Table>
          </div>
        </>
      ) : (
        ''
      )}

      <div
        className="card chart-card card-dark flex-grow-1 h-100 d-flex flex-column"
        style={{ borderColor: `#${appColor}` }}
        onClick={() => {
          if (coinModal) {
            setcoinModal(false);
          }
        }}
      >
        <div
          className="d-flex chart-head"
          style={{ borderColor: `#${appColor}` }}
        >
          <motion.div
            whileHover={{ scale: 1.2 }}
            className="current-chart-name"
            style={{ backgroundColor: `#${appColor}` }}
            onClick={() => setcoinModal(!coinModal)}
          >
            {tradeCoin.pair}
          </motion.div>

          {!tradeCoin?.is_terminal_lite ? (
            <>
              {' '}
              {dropValue == 'Asset Price' ? (
                <div
                  className="chart-time-interval-selector"
                  style={{ color: `#${appColor}` }}
                >
                  {chartIntervals.map((item) => (
                    <div
                      key={item}
                      className={`chart-interval-item ${
                        selectedInterval === item && 'active'
                      }`}
                      onClick={async () => setSelectedInterval(item)}
                      style={{
                        backgroundColor:
                          selectedInterval === item ? `#${appColor}` : 'white',
                        color:
                          selectedInterval === item ? 'white' : `#${appColor}`,
                        borderColor: `#${appColor}`,
                      }}
                    >
                      <motion.div whileHover={{ scale: 1.2 }}>
                        {item}
                      </motion.div>
                    </div>
                  ))}
                </div>
              ) : (
                <div className="chart-time-interval-selector">
                  {chartIntervals2.map((item) => (
                    <div
                      key={item}
                      className={`chart-interval-item ${
                        selectedInterval === item && 'active'
                      }`}
                      onClick={async () => setSelectedInterval(item)}
                      style={{
                        backgroundColor:
                          selectedInterval === item ? `#${appColor}` : 'white',
                        color:
                          selectedInterval === item ? 'white' : `#${appColor}`,
                        borderColor: `#${appColor}`,
                      }}
                    >
                      <motion.div whileHover={{ scale: 1.2 }}>
                        {item}
                      </motion.div>
                    </div>
                  ))}
                </div>
              )}{' '}
              <LineChartOutlined
                style={{
                  fontSize: '24px',
                  color: chartType !== 'area' ? `#${appColor}` : 'white',
                  backgroundColor:
                    chartType !== 'area' ? 'white' : `#${appColor}`,
                  marginRight: '16px',
                }}
                onClick={() => {
                  if (chartType === 'area') {
                    setChartType('line');
                  } else {
                    setChartType('area');
                  }
                }}
              />{' '}
              <Select
                showSearch
                defaultValue={dropValue}
                placeholder="Select"
                optionFilterProp="children"
                onChange={onChange}
                className="chartTop"
                style={{
                  width: 140,
                  marginRight: '16px',
                  border: `1px solid #${appColor}`,
                  color: `#${appColor}`,
                }}
              >
                <Option value="Asset Price" style={{ color: `#${appColor}` }}>
                  Asset Price
                </Option>
                <Option value="Volume" style={{ color: `#${appColor}` }}>
                  Volume
                </Option>
              </Select>{' '}
            </>
          ) : (
            ''
          )}

          {/* <small style={{color:"#3485C0",fontWeight:"600"}}>TimeZone</small> */}
          <div
            className="add-btn ml-2"
            style={{ backgroundColor: `#${appColor}`, color: 'white' }}
          >
            {/* <i className="fas fa-plus" /> Add */}
            {timeZ} EST
          </div>
        </div>
        <div
          className="d-flex flex-grow-1 p-1 mb-2 custchartdiv"
          style={{ overflow: 'hidden', height: '0' }}
        >
          {/* <BtcUsdtChart selectedInterval={selectedInterval} /> */}
          {dropValue == 'Asset Price' ? (
            tradeCoin?.is_terminal_lite ? (
              <TradingViewWidget
                symbol={tradeCoin?.pair.replace('/', '')}
                locale="en"
                autosize={true}
                width="100%"
                height="100%"
                interval={1}
              />
            ) : (
              <Gx_ApexChart
                selectedInterval={selectedInterval}
                timeInterval={timeInterval}
                setTimeInterVal={settimeInterval}
              />
            )
          ) : (
            <Gx_ApexChart
              selectedInterval={selectedInterval}
              timeInterval={timeInterval}
              setTimeInterVal={settimeInterval}
            />
            // <>
            //   {timeInterval == '1s' ? (
            //     <>
            //       <Gx_volumeChart
            //         selectedInterval={timeInterval}
            //         chartData={chartData}
            //       />
            //     </>
            //   ) : (
            //     ''
            //   )}
            //   {timeInterval == 'min' ? (
            //     <Gx_volumeChart
            //       selectedInterval={timeInterval}
            //       chartData={chartData}
            //     />
            //   ) : (
            //     ''
            //   )}
            //   {timeInterval == 'day' ? (
            //     <Gx_volumeChart
            //       selectedInterval={timeInterval}
            //       chartData={chartData}
            //     />
            //   ) : (
            //     ''
            //   )}
            //   {timeInterval == 'hour' ? (
            //     <Gx_volumeChart
            //       selectedInterval={timeInterval}
            //       chartData={chartData}
            //     />
            //   ) : (
            //     ''
            //   )}
            //   {timeInterval == 'month' ? (
            //     <Gx_volumeChart
            //       selectedInterval={timeInterval}
            //       chartData={chartData}
            //     />
            //   ) : (
            //     ''
            //   )}
            // </>
          )}
        </div>
      </div>
    </>
  );
};
const chartIntervals = ['1s', '1m', '1Hr', '1D', '1M'];
// const chartIntervals = ['1m', '1Hr', '1D', '1M'];

const chartIntervals2 = ['1s', '1m', '1Hr', '1D', '1M'];
export default withRouter(Gx_chart_card);
