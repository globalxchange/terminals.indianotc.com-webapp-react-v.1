import React, { useState, useEffect, useContext } from 'react';
import { OptionsContext } from '../../../context/Context';
// import Gx_CandleChart from './gx-candlechart';
// import Gx_BtcUsdChart from './gx-btcUsdCharts';
// import Gx_Chart2 from './gx-chart2';
// import Gx_terminal_user_api from '../../../utils/services/gx-terminal-user.service';
import Gx_Chart_new from './gx-new-charts';
import Terminal_ChartService from '../../../utils/services/gx-chart.service';
// const moment = require('moment-timezone');

// import socketIOClient from 'socket.io-client';
// import { Skeleton } from 'antd';
// import Chart from 'react-apexcharts';

// import Volumechart from './Volumechart';
// import moment from 'moment';
// import { motion } from 'framer-motion';

// const timeInEST = moment().tz('America/New_York').format('HH:mm');

// const ENDPOINT = 'https://teller2.apimachine.com';

// const socket = socketIOClient(ENDPOINT);
// socket.on('error', function (err) {
//   console.log('received socket error:');
//   console.log(err);
// });
// socket.on('connect', () => {
//   // console.log(socket.id, 'socket');
// });

export default function Gx_ApexChart(props) {
  const { randomValue, tradeCoin, socketPriceData } =
    useContext(OptionsContext);

  const [chartData, setChartData] = useState([]);
  const [timeInterval, setTimeInterVal] = useState('min');
  const [loading, setloading] = useState(false);

  const [type, settype] = useState('candlestick');
  // const [realTimeChart, setrealTimeChart] = useState(realTime);
  useEffect(() => {
    if (timeInterval === '1s') {
      console.log('setting chart data');
      setChartData(socketPriceData);
    }
  }, [socketPriceData]);

  useEffect(async () => {
    if (timeInterval === '1s') {
      if (!tradeCoin?.is_forex) {
        if (tradeCoin?.is_terminal_lite) {
          let res = await Terminal_ChartService.getTerminalLiteCharts(
            tradeCoin?.pair,
            'min',
          );

          let new_chartData = [];
          res.forEach((element) => {
            new_chartData.push({
              timestamp: element.timestamp,
              value: element.highest,
            });
          });
        } else if (!tradeCoin?.is_terminal_lite) {
          let res = await Terminal_ChartService.getNonTerminalCharts(
            tradeCoin?.pair,
            'min',
          );
          let new_chart_data = [];
          // setChartData(res);
          res?.map((element) => {
            new_chart_data.push({
              timestamp: element?.timestamp,
              value: element?.highest,
            });
          });
        }
      } else {
        let res = await Terminal_ChartService.getForexChart(
          tradeCoin?.pair,
          'min',
        );

        let new_chartData = [];
        try {
          res?.forEach((element) => {
            new_chartData.push({
              timestamp: element.t,
              value: element.h,
            });
          });
        } catch (E) {
          //
        }
      }
      setChartData([...chartData, ...socketPriceData]);
      console.log([...socketPriceData, ...chartData]);
      setloading(false);

      // console.log(timeInterval);
    } else {
      if (!tradeCoin?.is_forex) {
        if (tradeCoin?.is_terminal_lite) {
          let res = await Terminal_ChartService.getTerminalLiteCharts(
            tradeCoin?.pair,
            timeInterval,
          );

          console.log('lite');

          let new_chartData = [];
          res.forEach((element) => {
            new_chartData.push({
              timestamp: element.timestamp,
              value: element.highest,
            });
          });
          setChartData(new_chartData);
          setloading(false);
        } else if (!tradeCoin?.is_terminal_lite) {
          let res = await Terminal_ChartService.getNonTerminalCharts(
            tradeCoin?.pair,
            timeInterval,
          );
          let new_chart_data = [];
          // setChartData(res);
          res?.map((element) => {
            new_chart_data.push({
              timestamp: element?.timestamp,
              value: element?.highest,
            });
          });

          setChartData(new_chart_data);
          setloading(false);
        }
      } else {
        let res = await Terminal_ChartService.getForexChart(
          tradeCoin?.pair,
          timeInterval,
        );

        let new_chartData = [];
        try {
          res?.forEach((element) => {
            new_chartData.push({
              timestamp: element.t,
              value: element.h,
            });
          });
        } catch (E) {
          //
        }
        setChartData(new_chartData);
        setloading(false);
      }
    }
  }, [tradeCoin, timeInterval, randomValue]);

  useEffect(() => {
    if (timeInterval == 'min') {
      settype('candlestick');
    } else if (timeInterval == '1s') {
      settype('area');
    } else {
      settype('area');
    }
  }, [timeInterval]);

  useEffect(() => {
    setloading(true);
    if (props.selectedInterval == '1m') {
      setTimeInterVal('min');
    } else if (props.selectedInterval == '1Hr') {
      setTimeInterVal('hour');
    } else if (props.selectedInterval == '1D') {
      setTimeInterVal('day');
    } else if (props.selectedInterval == '1M') {
      setTimeInterVal('month');
    } else if (props.selectedInterval == '1s') {
      setTimeInterVal('1s');
    }
    setTimeout(() => {
      setloading(true);
    }, 400);
  }, [props.selectedInterval]);

  return (
    <>
      {chartData?.length > 0 ? (
        <>
          <Gx_Chart_new timeInterval={timeInterval} chartData={chartData} />
        </>
      ) : (
        ''
      )}
    </>
  );
}
