import React, { useContext } from 'react';
import { OptionsContext } from '../../../context/Context';

const Gx_orderListItem = ({ value, color, isBid }) => {
  // console.log("value :", value);

  const { appColor } = useContext(OptionsContext);
  // const splitCoin = findFiat(tradeCoin?.pair?.split('/')[0]);
  return (
    <div
      className="flex-grow-1 d-flex justify-content-between order-list-itm text-left text-white px-4"
      style={{
        background: `linear-gradient(90deg, rgba(0,0,0,0) 0%, rgba(0,0,0,0) ${
          100 - value?.percentage
        }%, ${color} ${100 - value?.percentage}%, ${color} 100%)`,
        height: '25%',
        fontWeight: '600',
      }}
    >
      <div
        className="tab-itm  px-0 text-center d-flex flex-column align-content-center justify-content-center"
        style={{
          color: isBid ? '#EF5350' : `#${appColor}`,
          marginLeft: '-6px',
          fontWeight: '600',
        }}
      >
        {value?.amount
          ? parseInt(value?.amount)?.toString()?.length > 5
            ? parseFloat(value?.amount)?.toFixed(2)
            : parseFloat(value?.amount)?.toFixed(5)
          : '--'}
      </div>

      <div
        className="tab-itm  px-0 text-center d-flex flex-column align-content-center justify-content-center"
        style={{ color: isBid ? '#EF5350' : `#${appColor}`, fontWeight: '600' }}
      >
        {value.price
          ? parseInt(value?.price)?.toString()?.length > 5
            ? parseFloat(value?.price)
              ? parseFloat(value?.price)?.toFixed(2)
              : '-'
            : parseFloat(value?.price)
            ? parseFloat(value?.price)?.toFixed(5)
            : '-'
          : '--'}
      </div>

      <div
        className="tab-itm col-4 px-0 text-center d-flex flex-column align-content-center justify-content-center"
        style={{
          color: isBid ? '#EF5350' : `#${appColor}`,
          marginRight: '-12px',
          fontWeight: '600',
        }}
      >
        {isBid
          ? value.totalAsk
            ? parseInt(value?.totalAsk)?.toString()?.length > 5
              ? parseFloat(value.totalAsk)?.toFixed(2)
              : parseFloat(value.totalAsk)?.toFixed(5)
            : '--'
          : value.totalBid
          ? parseInt(value?.totalBid)?.toString()?.length > 5
            ? parseFloat(value.totalBid)?.toFixed(2)
            : parseFloat(value.totalBid)?.toFixed(5)
          : '--'}
      </div>
    </div>
  );
};

export default Gx_orderListItem;
