import React, { useState, useEffect, useContext } from 'react';
import Gx_OrderListItem from './gx-order-item';
import { OptionsContext } from '../../../context/Context';
import {
  init as orderbookInit,
  orderbookWSHelpers,
} from '../../../utils/orderbook-ws';

function Gx_order_book() {
  const {
    tradeCoin,
    socketOrderBook,
    appColor,
    coinModal,
    setcoinModal,
    prevTradeCoin,
    // findFiat,
    setsocketOrderBook,
    setPreviousTradeCoin,
    setSocketPriceData,
    socketPriceData,
  } = useContext(OptionsContext);
  const [orderBuyData, setOrderBuyData] = useState([]);
  const [ordereSellData, setOrderSellData] = useState([]);
  const [largest, setLargest] = useState(1);

  useEffect(() => {
    tradeFunction();
  }, [tradeCoin]);

  useEffect(() => {
    if (socketOrderBook) {
      if (socketOrderBook.buy || socketOrderBook.sell) {
        updateOrderBook(socketOrderBook);
      }
    }
  }, [socketOrderBook]);

  const tradeFunction = () => {
    // console.log(`${tradeCoin?.pair} ${prevTradeCoin?.pair}`);
    setsocketOrderBook({ buy: [], sell: [] });
    setSocketPriceData([]);
    setOrderBuyData([]);
    setOrderSellData([]);

    if (prevTradeCoin?.pair) {
      // console.log(`pT--${prevTradeCoin?.pair} T--${tradeCoin?.pair}`);
      if (tradeCoin?.is_forex) {
        if (tradeCoin?.is_forex === prevTradeCoin?.is_forex) {
          // console.log('same forex pair');
          joinBasePairFunction(tradeCoin, prevTradeCoin, setPreviousTradeCoin);
        } else {
          // console.log('different forex pair');
          orderbookWSHelpers.leaveBasePair(prevTradeCoin);
          setPreviousTradeCoin(undefined);

          setTimeout(() => {
            orderbookInit(tradeCoin);
          }, 500);
        }
      } else {
        if (prevTradeCoin?.is_terminal_lite === tradeCoin?.is_terminal_lite) {
          // console.log('same pair terminal lite');
          joinBasePairFunction(tradeCoin, prevTradeCoin, setPreviousTradeCoin);
        } else {
          // console.log('leave');
          orderbookWSHelpers.leaveBasePair(prevTradeCoin);
          setPreviousTradeCoin(undefined);
          setTimeout(() => {
            orderbookInit(tradeCoin);
          }, 500);
        }
      }
    } else {
      // console.log('no previous coin');
      // console.log(tradeCoin);

      orderbookInit(tradeCoin);
    }
  };
  const joinBasePairFunction = () => {
    console.log(`T- ${tradeCoin?.pair} PT- ${prevTradeCoin?.pair}-------`);
    orderbookWSHelpers.joinBasePair(
      tradeCoin,
      prevTradeCoin,
      setPreviousTradeCoin,
    );
  };

  const updateOrderBook = (data) => {
    // console.log(data);
    if (data?.length === 0) {
      return;
    }
    if (tradeCoin?.is_forex) {
      // console.log(data);
      setOrderBuyData([
        { price: data.buy },
        { price: '' },
        { price: '' },
        { price: '' },
      ]);
      setOrderSellData([
        { price: data.sell },
        { price: '' },
        { price: '' },
        { price: '' },
      ]);
      return;
    }
    let fake = {
      amount: '',
      price: '',
      totalAsk: '',
      totalBid: '',
      percentage: 0,
    };
    if (data?.buy?.length === 0 && data?.sell?.length === 0) {
      setOrderBuyData([fake, fake, fake, fake]);
      setOrderSellData([fake, fake, fake, fake]);
      return;
    } else {
      try {
        let n1 = data?.buy?.length;
        let n2 = data?.sell?.length;
        data.sell = data?.sell?.reverse?.();
        const haha = (x1, x2) => {
          if (x1 == 0) {
            data[x2] = [fake, fake, fake, fake];
          } else if (x1 == 1) {
            data[x2] = [...data[x2], fake, fake, fake];
          } else if (x1 == 2) {
            data[x2] = [...data[x2], fake, fake];
          } else if (x1 == 3) {
            data[x2] = [...data[x2], fake];
          }
        };
        haha(n1, 'buy');
        haha(n2, 'sell');

        setOrderBuyData(data?.buy?.slice?.(0, 4));
        setOrderSellData(data?.sell?.slice?.(0, 4));
      } catch (e) {
        console.log(e);
      }
    }
  };

  return (
    <div
      className="card order-book-card card-dark flex-grow-1 h-100"
      style={{ borderColor: `#${appColor}` }}
      onClick={() => {
        if (coinModal) {
          setcoinModal(false);
        }
      }}
    >
      <div
        className="opt-tab d-flex justify-content-between text-left"
        style={{
          borderColor: `#${appColor}`,
          height: '48px',
          padding: '0px 0px',
        }}
      >
        <div
          className="tab-itm  font-weight-bold text-center"
          style={{ color: `#${appColor}`, width: '33.33%' }}
        >
          Amount
        </div>
        <div
          className="tab-itm  font-weight-bold text-center"
          style={{ color: `#${appColor}`, width: '33.33%' }}
        >
          {/* Ask ({tradeCoin ? tradeCoin.slice(4) : ''}) */}
          Price
        </div>
        <div
          className="tab-itm  font-weight-bold text-center"
          style={{ color: `#${appColor}`, width: '33.33%' }}
        >
          Total
          {/* ({tradeCoin ? tradeCoin.slice(4) : ''}) */}
        </div>
      </div>
      <div className="order-book-list d-flex flex-column flex-grow-1">
        <div
          style={{ height: '45%', overflowY: 'scroll' }}
          className="inner-ob"
        >
          {ordereSellData
            ? ordereSellData.map((bid, i) => {
                // console.log(bid);
                if (bid[0] * bid[1] > largest) {
                  setLargest(bid[0] * bid[1]);
                }
                return (
                  <Gx_OrderListItem
                    isBid={true}
                    key={i}
                    value={bid}
                    percentage={62}
                    color="rgba(239, 83, 80, 0.17)"
                    largest={largest}
                    setLargest={setLargest}
                  />
                );
              })
            : ''}
        </div>

        <h4
          className="d-flex justify-content-between m-0 px-4 py-2"
          style={{ backgroundColor: `#${appColor}`, height: '48px' }}
        >
          <small
            className="col tab-itm"
            style={{
              paddingLeft: '0px',
              alignSelf: 'center',
              fontSize: '14px',
            }}
          >
            {`1  ${tradeCoin.pair.split('/')[0]}`}
          </small>
          <small
            className="col text-right tab-itm"
            style={{
              paddingRight: '0px',
              alignSelf: 'center',
              fontSize: '14px',
            }}
          >
            {(() => {
              if (socketPriceData && socketPriceData?.length !== 0) {
                if (socketPriceData?.length === 0) {
                  return;
                }
                return (
                  <>
                    {parseInt(socketPriceData?.[0]?.value)?.toString()?.length >
                    5
                      ? parseFloat(socketPriceData?.[0]?.value).toFixed(2)
                      : parseFloat(socketPriceData?.[0]?.value).toFixed(5)}{' '}
                    {tradeCoin.pair.split('/')[1]}
                  </>
                );
              } else {
                return (
                  <>
                    {socketOrderBook?.sell
                      ? parseInt(socketOrderBook?.sell[0]?.price)?.toString()
                          ?.length > 5
                        ? parseFloat(socketOrderBook?.sell[0]?.price).toFixed(2)
                        : parseFloat(socketOrderBook?.sell[0]?.price).toFixed(5)
                      : '---'}{' '}
                    {tradeCoin.pair.split('/')[1]}
                  </>
                );
              }
            })()}
          </small>{' '}
        </h4>

        <div
          style={{ height: '45%', overflowY: 'scroll' }}
          className="inner-ob"
        >
          {orderBuyData
            ? orderBuyData.map((bid, i) => {
                // console.log(bid);
                if (bid[0] * bid[1] > largest) {
                  setLargest(bid[0] * bid[1]);
                }
                return (
                  <Gx_OrderListItem
                    isBid={false}
                    key={i}
                    value={bid}
                    percentage={62}
                    color={'rgba(52, 133, 192, 0.17)'}
                    largest={largest}
                    setLargest={setLargest}
                  />
                );
              })
            : ''}
        </div>
      </div>
    </div>
  );
}

export default Gx_order_book;
