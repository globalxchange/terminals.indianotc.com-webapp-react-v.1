import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router';
import Gx_TopNav from '../gx-navbar/gx-navbar';
import { OptionsContext } from '../../context/Context';
import Gx_coinsider from '../gx-coinsider/gx-coinsider';
const Gx_layout = ({ history, children, className, loading, setLoading }) => {
  const { coinModal, setcoinModal } = useContext(OptionsContext);
  const [isToken, setIsToken] = useState(false);
  const [tab, setTab] = useState('default');
  const tabItem = {
    default: children,
    token1: <h1>Token1</h1>,
    token2: <h1>Token2</h1>,
    token3: <h1>Token3</h1>,
  };
  return (
    <div className={`d-flex ${!isToken ? 'terminal ' : ''}${className}`}>
      <div className="opt-layout d-flex flex-column">
        <Gx_TopNav
          isToken={isToken}
          setIsToken={setIsToken}
          tab={tab}
          setTab={setTab}
          onClick={() => {
            if (coinModal) {
              setcoinModal(false);
            }
          }}
        />

        <div className="flex-grow-1 d-flex flex-column">{tabItem[tab]}</div>
      </div>
      <Gx_coinsider isToken={isToken} />
    </div>
  );
};
export default withRouter(Gx_layout);
