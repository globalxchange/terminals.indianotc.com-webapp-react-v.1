import React, { useState, useContext, useEffect } from 'react';
import Axios from 'axios';
import { Modal } from 'react-bootstrap';
import {
  Input,
  Card,
  Badge,
  Modal as Modal2,
  Form,
  message,
  notification,
} from 'antd';
import Lottie from 'react-lottie';
import { OptionsContext } from '../../context/Context';
import { withRouter } from 'react-router-dom';
import CryptoJS from 'crypto-js';
import * as animationData from '../../assets/animation/new_slow_chart.json';
import { LoadingOutlined } from '@ant-design/icons';
import OtpInput from 'react-otp-input';

import '../../assets/scss/login.scss';
import Gx_commsService from '../../utils/services/gx-comms.service';
import Gx_terminal_user_api from '../../utils/services/gx-terminal-user.service';
import Gx_Loading from '../gx-misc/gx-loading';
// import GX_apis from '../../utils/services/gx-terminal-apis.service';

const secretkey = 'gmBuuQ6Er8XqYBd';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData.default,
  rendererSettings: {
    preserveAspectRatio: 'xMidYMid slice',
  },
};

const Gx_login = ({ history }) => {
  const {
    setUserRegisteredTerminals,
    loginModal,
    setloginModal,
    appColor,
    terminalID,
    setEmailId,
    setshowVaults,
    setRandomValue,
    setAppLoginData,
    appName,
    setTempLoginDetails,
    returnToVault,
  } = useContext(OptionsContext);

  //-------------------states--------------------------//

  const [userEmail, setUserEmail] = useState();
  const [userPassword, setUsrPassword] = useState();

  const [username, setusername] = useState('');
  const [regPassword, setRegpassword] = useState('');
  const [temp, setTemp] = useState({});
  //--------------------------------------------------//
  const [useremail, setuseremail] = useState('');
  const [confirmRegPassword, setconfirmRegPassword] = useState('');
  const [otp, setotp] = useState('');
  const [page, setpage] = useState('1');
  const [pass1, setpass1] = useState(false);
  const [pass2, setpass2] = useState(false);
  const [pass3, setpass3] = useState(false);
  const [pass4, setpass4] = useState(false);
  const [usernameValid, setusernameValid] = useState(false);
  const [emailValid, setemailValid] = useState(false);

  const [signupLoading, setsignupLoading] = useState(false);
  const [loginEmailError, setloginEmailError] = useState(false);
  const [loginPasswordError, setloginPasswordError] = useState(false);
  const [showRegister, setshowRegister] = useState(true);
  const [mfaEnabled, setMfaEnabled] = useState(false);
  const [googlePin, setGooglePin] = useState('');
  const [loading, setLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');

  const handleClose = async () => {
    setloginModal(false);
  };

  const checkUserName = async (e) => {
    let n1 = e.target.value;
    setusername(n1);
    let res = await Gx_commsService.checkUsernameAndEmail({ username: n1 });
    if (res.status) {
      setusernameValid(true);
    } else {
      setusernameValid(false);
    }
  };

  const checkEmail = async (e) => {
    let n1 = e.target.value;
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    setuseremail(n1);

    if (!re.test(String(n1).toLowerCase())) {
      setemailValid(false);

      return;
    }
    let res = await Gx_commsService.checkUsernameAndEmail({ email: n1 });
    if (res.status) {
      setemailValid(true);
    } else {
      setemailValid(false);
    }
  };

  const handleSignup = async () => {
    setsignupLoading(true);
    setpage('loading');
    let body = {
      username: username,
      email: useremail,
      password: confirmRegPassword,
      ref_affiliate: '1', // reference/upline affiliate-id
      account_type: 'Personal',
      signedup_app: 'GX', // The user signing up from app
    };
    let res = await Gx_commsService.regsiterUser(body);

    setsignupLoading(false);
    if (res?.status) {
      setpage('4');
    } else {
      setpage('3');
      Modal2.error({
        title: 'Error',
        content: res?.message,
      });
    }
  };

  const handleRegistration = async () => {
    setpage('loading');
    try {
      let body = {
        email: useremail,
        code: otp,
      };
      let res = await Gx_commsService.regsiterUserConfirm(body);

      //console.log(res.data);

      if (res?.status) {
        setpage('1');
        setuseremail('');
        setusername('');
        setemailValid(false);
        setusernameValid(false);
        Modal2.success({
          title: 'Success',
          content: `Your GX registartion is succesfull. Please log into terminals.`,
        });
      } else {
        setpage('4');
        setotp('');
        Modal2.error({
          title: 'Error',
          content: res?.message,
        });
      }
    } catch (e) {
      notification.error({ message: e?.message });
    }
  };

  const handlePassword = (e) => {
    let n1 = e.target.value;
    // console.log(n1);
    setRegpassword(n1);
    setpass1(n1.length > 7 ? true : false);
    setpass2(/[A-Z]/.test(n1) ? true : false);
    setpass3(/[0-9]/.test(n1) ? true : false);
    setpass4(/[*@!#%&()^~{}]+/.test(n1) ? true : false);
  };

  const handleLogin = async () => {
    if (userEmail && userPassword) {
      setLoading(true);
      if (/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(userEmail)) {
        if (mfaEnabled) {
          try {
            let data = await Gx_commsService.login(
              userEmail,
              userPassword,
              true,
              googlePin,
            );

            if (data.status) {
              try {
                const headers = {
                  email: userEmail,
                  token: data.idToken,
                };
                let res =
                  await Gx_terminal_user_api.checkUserIsRegisteredToTerminals(
                    userEmail,
                  );
                if (res) {
                  try {
                    let res = await Gx_terminal_user_api.checkUserIsRegistered(
                      terminalID,
                      headers,
                    );
                    if (res?.ter_terminal_id) {
                      setEmailId(userEmail);
                      setRandomValue(Math.random() * 1000);
                      setAppLoginData(
                        data.idToken,
                        data.accessToken,
                        userEmail,
                        data.device_key,
                      );

                      if (returnToVault === 'Vault') {
                        setTimeout(() => {
                          setshowVaults(true);
                        }, 100);
                      }
                      setLoading(false);
                      setloginModal(false);
                    } else {
                      await Gx_terminal_user_api.addUserToTerminal(
                        headers,
                        terminalID,
                      );
                      let registeredTerminals =
                        await Gx_terminal_user_api.getUserRegisteredTerminals(
                          headers,
                        );
                      setUserRegisteredTerminals(registeredTerminals);
                      setEmailId(userEmail);
                      setAppLoginData(
                        data.idToken,
                        data.accessToken,
                        userEmail,
                        data.device_key,
                      );
                      setRandomValue(Math.random() * 1000);
                      setLoading(false);

                      if (returnToVault === 'Vault') {
                        setTimeout(() => {
                          setshowVaults(true);
                        }, 100);
                      }
                      setloginModal(false);
                    }
                  } catch (e) {
                    if (e?.message === 'Invalid Terminal ID') {
                      Modal2.error({
                        title: 'Error',
                        content: e?.message,
                      });
                    } else {
                      Modal2.error({
                        title: 'Error',
                        content: e?.message,
                      });
                    }

                    setloginModal(false);
                    // setEmailId(userEmail);
                    setLoading(false);
                  }
                } else {
                  setTempLoginDetails({
                    token: data.idToken,
                    accessToken: data.accessToken,
                    email: userEmail,
                    device_key: data.device_key,
                  });
                  setloginModal(false);
                  setLoading(false);
                  history?.push(`/${terminalID}/terminalSignup`);
                }
              } catch (err) {
                message.error(err?.message);
              }
            } else {
              setErrorMsg(data.message);
              Modal2.error({
                title: 'Error',
                content: data.message,
              });
              setLoading(false);
            }
          } catch (e) {
            message.error(e?.message);
            setLoading(false);
          }
        } else {
          try {
            let data = await Gx_commsService.login(
              userEmail,
              userPassword,
              false,
            );

            if (data.status) {
              try {
                const headers = {
                  email: userEmail,
                  token: data.idToken,
                };
                let res =
                  await Gx_terminal_user_api.checkUserIsRegisteredToTerminals(
                    userEmail,
                  );
                if (res) {
                  try {
                    let res = await Gx_terminal_user_api.checkUserIsRegistered(
                      terminalID,
                      headers,
                    );
                    if (res?.ter_terminal_id) {
                      setEmailId(userEmail);
                      setRandomValue(Math.random() * 1000);
                      setAppLoginData(
                        data.idToken,
                        data.accessToken,
                        userEmail,
                        data.device_key,
                      );

                      if (returnToVault === 'Vault') {
                        setTimeout(() => {
                          setshowVaults(true);
                        }, 100);
                      }
                      setLoading(false);
                      setloginModal(false);
                    } else {
                      await Gx_terminal_user_api.addUserToTerminal(
                        headers,
                        terminalID,
                      );
                      let registeredTerminals =
                        await Gx_terminal_user_api.getUserRegisteredTerminals(
                          headers,
                        );
                      setUserRegisteredTerminals(registeredTerminals);
                      setEmailId(userEmail);
                      setAppLoginData(
                        data.idToken,
                        data.accessToken,
                        userEmail,
                        data.device_key,
                      );
                      setRandomValue(Math.random() * 1000);
                      setLoading(false);

                      if (returnToVault === 'Vault') {
                        setTimeout(() => {
                          setshowVaults(true);
                        }, 100);
                      }
                      setloginModal(false);
                    }
                  } catch (e) {
                    if (e?.message === 'Invalid Terminal ID') {
                      Modal2.error({
                        title: 'Error',
                        content: e?.message,
                      });
                    } else {
                      Modal2.error({
                        title: 'Error',
                        content: e?.message,
                      });
                    }

                    setloginModal(false);
                    // setEmailId(userEmail);
                    setLoading(false);
                  }
                } else {
                  setTempLoginDetails({
                    token: data.idToken,
                    accessToken: data.accessToken,
                    email: userEmail,
                    device_key: data.device_key,
                  });
                  setloginModal(false);
                  setLoading(false);
                  history?.push(`/${terminalID}/terminalSignup`);
                }
              } catch (err) {
                message.error(err?.message);
              }
            } else if (data.mfa) {
              setTemp(data);
              setMfaEnabled(true);
              setLoading(false);
            } else {
              setErrorMsg(data.message);
              Modal2.error({
                title: 'Error',
                content: data.message,
              });
              setLoading(false);
            }
          } catch (error) {
            console.log('Login Error', error);
            setErrorMsg('Some Thing Went Wrong!');
            setLoading(false);
          }
        }
      } else {
        Modal2.error({
          title: 'Error',
          content: 'Enter Valid Email',
        });
        setLoading(false);
      }
    }

    if (userEmail.length == 0) {
      setloginEmailError(true);
    } else {
      setloginEmailError(false);
    }

    if (userPassword.length == 0) {
      setloginPasswordError(true);
    } else {
      setloginPasswordError(false);
    }
  };
  return (
    <>
      <div className="newlogin">
        <Modal
          show={loginModal}
          onHide={handleClose}
          style={{ color: `#${appColor}` }}
          className={'newlogin1'}
          centered
        >
          <div className="row">
            <div className="col-md-6" style={{ paddingRight: '10%' }}>
              {' '}
              <Form>
                <div>
                  <div
                    className="gx-login-one"
                    style={{
                      color: `#${appColor}`,
                    }}
                  >
                    Login
                  </div>
                  <span style={{ color: `#${appColor}` }}>
                    With Your {appName} Account
                  </span>
                  {loading ? (
                    ''
                  ) : (
                    <>
                      <br></br>
                      {loginEmailError ? (
                        <div className="mt-4 gx-email-box">
                          Your Need To Enter An Email/Username
                        </div>
                      ) : (
                        <div
                          style={{
                            color: `#${appColor}`,
                          }}
                          className="mt-4 gx-l-username"
                        >
                          USERNAME
                        </div>
                      )}
                      <Input
                        placeholder="Username"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        className="mt-2"
                        value={userEmail}
                        onChange={(e) => {
                          setUserEmail(e.target.value);
                          // setEmail(e.target.value);
                        }}
                      />
                      <br></br>
                      {loginPasswordError ? (
                        <div className="mt-4 gx-email-box">
                          Your Need To Enter A Password
                        </div>
                      ) : (
                        <div
                          className="mt-4 gx-l-username"
                          style={{
                            color: `#${appColor}`,
                          }}
                        >
                          Password
                        </div>
                      )}
                      <Input.Password
                        placeholder="Password"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        value={userPassword}
                        className="mt-2"
                        onChange={(e) => setUsrPassword(e.target.value)}
                      />
                      {mfaEnabled ? (
                        <>
                          <div
                            style={{
                              color: `#${appColor}`,
                            }}
                            className="mt-4 gx-l-username"
                          >
                            MFA Code
                          </div>
                          <Input
                            placeholder="MFA"
                            style={{
                              border: 'none',
                              borderBottom: `1px solid #${appColor}`,
                            }}
                            className="mt-2"
                            value={googlePin}
                            onChange={(e) => {
                              setGooglePin(e.target.value);
                              // setEmail(e.target.value);
                            }}
                          />
                        </>
                      ) : (
                        ''
                      )}
                      <span style={{ float: 'right', color: `#${appColor}` }}>
                        Forgot Password ?
                      </span>{' '}
                    </>
                  )}
                </div>
              </Form>
              <br></br>
              <br></br>
              {loading ? (
                <Lottie options={defaultOptions} height={140} width={120} />
              ) : (
                <button
                  className="mt-4 p-2"
                  style={{
                    width: '100%',
                    backgroundColor: `#${appColor}`,
                    color: 'white',
                    border: `1px solid #${appColor}`,
                  }}
                  onClick={() => {
                    if (loading) {
                      return;
                    }
                    handleLogin();
                  }}
                >
                  LOGIN
                </button>
              )}
            </div>

            {showRegister ? (
              <div
                className="col-md-6"
                style={{
                  borderLeft: `1px dashed  #${appColor}`,
                  paddingLeft: '10%',
                }}
              >
                <div>
                  <div style={{ fontSize: '40px', fontWeight: 'bold' }}>
                    Register
                  </div>
                  <span>If You Don’t Have A {appName} Account</span>
                  <br></br>
                  {page == '1' && (
                    <>
                      <div className="mt-4 gx-l-username">SELECT USERNAME</div>
                      <Input
                        placeholder="Username"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        className="mt-2"
                        value={username}
                        onChange={(e) => checkUserName(e)}
                      />
                      <Badge
                        status={usernameValid ? 'success' : 'error'}
                        className="gx-l-pos"
                      />
                      <br></br>
                      <div className="mt-4 gx-l-username">ENTER EMAIL</div>
                      <Input
                        placeholder="Email"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        className="mt-2"
                        value={useremail}
                        onChange={(e) => checkEmail(e)}
                      />
                      <Badge
                        status={emailValid ? 'success' : 'error'}
                        className="gx-l-pos"
                      />
                      <br></br>
                      <br></br>
                      <button
                        disabled={usernameValid && emailValid ? false : true}
                        className="mt-4 p-2"
                        style={{
                          width: '100%',
                          backgroundColor: `#${appColor}`,
                          color: 'white',
                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => setpage('2')}
                      >
                        NEXT STEP
                      </button>
                    </>
                  )}

                  {page == '2' && (
                    <>
                      <div
                        style={{
                          color: `#${appColor}`,
                        }}
                        className="mt-4 gx-l-username"
                      >
                        Enter Password
                      </div>
                      <Input.Password
                        placeholder="Enter Password"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        className="mt-2"
                        value={regPassword}
                        onChange={(e) => handlePassword(e)}
                      />

                      <Card style={{ width: '100%' }}>
                        <span>The Password Checklist</span>
                        <br></br>
                        <div className="d-flex">
                          <small>Should be 8 charectors</small>
                          <Badge
                            status={pass1 ? 'success' : 'error'}
                            className="gx-l-pos"
                          />
                        </div>
                        <div className="d-flex">
                          <small>Contains Atleast One Capital Letter</small>
                          <Badge
                            status={pass2 ? 'success' : 'error'}
                            className="gx-l-pos"
                          />
                        </div>
                        <div className="d-flex">
                          <small>Contains Atleast One Number</small>
                          <Badge
                            status={pass3 ? 'success' : 'error'}
                            className="gx-l-pos"
                          />
                        </div>
                        <div className="d-flex">
                          <small>Contains Atleast One Special Charector</small>
                          <Badge
                            status={pass4 ? 'success' : 'error'}
                            className="gx-l-pos"
                          />
                        </div>
                      </Card>

                      <br></br>
                      <button
                        disabled={
                          pass1 && pass2 && pass3 && pass4 ? false : true
                        }
                        className="mt-4 p-2"
                        style={{
                          width: '100%',
                          backgroundColor: `#${appColor}`,
                          color: 'white',
                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => setpage('3')}
                      >
                        Confirm Password
                      </button>
                      <br />
                      <br />
                      <span
                        onClick={() => {
                          setpage('1');
                        }}
                      >
                        Back
                      </span>
                    </>
                  )}
                  {page == '3' && (
                    <>
                      <div className="mt-4 gx-l-username">Confirm Password</div>
                      <Input.Password
                        placeholder="Confirm Password"
                        style={{
                          border: 'none',
                          borderBottom: `1px solid #${appColor}`,
                        }}
                        className="mt-2"
                        value={confirmRegPassword}
                        onChange={(e) => setconfirmRegPassword(e.target.value)}
                      />
                      <Badge
                        status={
                          regPassword === confirmRegPassword
                            ? 'success'
                            : 'error'
                        }
                        className="gx-l-pos"
                      />
                      <br></br>
                      <button
                        disabled={
                          regPassword === confirmRegPassword ? false : true
                        }
                        className="mt-4 p-2"
                        style={{
                          width: '100%',
                          backgroundColor: `#${appColor}`,
                          color: 'white',
                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => {
                          handleSignup();
                          // setpage("4")
                        }}
                      >
                        {signupLoading ? <LoadingOutlined /> : 'Next'}
                      </button>
                      <br />
                      <br />
                      <span
                        onClick={() => {
                          setpage('2');
                        }}
                      >
                        Back
                      </span>
                    </>
                  )}

                  {page == '4' && (
                    <>
                      <br></br>
                      <br></br>
                      <span>
                        Enter The Six Digit Confirmation Code Was Just Sent To{' '}
                        {useremail}
                      </span>
                      <OtpInput
                        value={otp}
                        onChange={setotp}
                        numInputs={6}
                        separator={<span>-</span>}
                        inputStyle={{
                          width: '2rem',
                          height: '2rem',
                          margin: '0.5rem 0',
                          fontSize: '1.2rem',
                          borderRadius: 4,
                          color: `#${appColor}`,
                          border: `1px solid #${appColor}`,
                        }}
                      />
                      <br></br>
                      <button
                        className="mt-4 p-2"
                        style={{
                          width: '100%',
                          backgroundColor: `#${appColor}`,
                          color: 'white',
                          border: `1px solid #${appColor}`,
                        }}
                        onClick={() => {
                          handleRegistration();
                          // setpage("4")
                        }}
                      >
                        Complete Registration
                      </button>
                      <br />
                      <br />

                      <span
                        onClick={() => {
                          setpage('3');
                        }}
                      >
                        Back
                      </span>
                    </>
                  )}
                  {page == 'loading' ? (
                    <>
                      {' '}
                      <Lottie
                        options={defaultOptions}
                        height={140}
                        width={120}
                      />
                    </>
                  ) : (
                    ''
                  )}
                </div>
              </div>
            ) : (
              ''
            )}
          </div>
        </Modal>
      </div>
    </>
  );
};

export default withRouter(Gx_login);
