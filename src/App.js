import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import OptionsContextProvider from './context/Context';
import Gx_Terminal_Dashboard from './pages/Main-Dashboard/Main-Dashboard';
import Gx_TerminalSignUp from './pages/gx-login-register/gx-terminal-register';
import Gx_terminal_landing from './pages/Landing/gx-landing';
import Gx_errorboundary from './components/gx-error-boundary/gx-error-boundary';
import Gx_terminal_user_api from './utils/services/gx-terminal-user.service';
import Gx_Loading from './components/gx-misc/gx-loading';

const App = () => {
  const [loading, setLoading] = React.useState(true);
  React.useEffect(async () => {
    if (window.innerWidth < 600) {
      setLoading(false);
      return;
    }
    try {
      setLoading(true);
      await Gx_terminal_user_api.checkToken();
      setLoading(false);
    } catch (e) {
      if (e?.message === 'Session Expired') {
        localStorage?.clear();
        setLoading(false);
      } else {
        console.log(e?.message);
        setLoading(false);
      }
    }
  }, []);
  return (
    <Router basename="/">
      <Gx_errorboundary>
        {loading ? (
          <Gx_Loading />
        ) : (
          <OptionsContextProvider>
            <Switch>
              <Route path="/:id/terminalSignup" component={Gx_TerminalSignUp} />
              <Route path="/:id" exact component={Gx_Terminal_Dashboard} />
              <Route path="/" exact component={Gx_terminal_landing} />
            </Switch>
          </OptionsContextProvider>
        )}
      </Gx_errorboundary>
    </Router>
  );
};

export default App;
